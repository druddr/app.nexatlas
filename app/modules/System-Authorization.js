﻿angular.module('System-Authorization',[]);

angular.module("System-Authorization").factory("Session", ['$rootScope', function ($rootScope) {
	var $current_user_store_key = '#current$user@';
	var $current_company_store_key = '#current$company@';
	var $current_subsidiary_store_key = '#current$subsidiary@';

	var $companies_store_key = '#list$companies@';
	var $subsidiaries_store_key = '#list$subsidiaries@';
	var $routes_store_key = '#list$routes@';
	
	var session = {
		clear: function () {
			Storage.local.set($current_user_store_key, {});
			Storage.local.set($current_subsidiary_store_key, {});
			Storage.local.set($current_company_store_key, {});

			Storage.local.set($routes_store_key, null);
			Storage.local.set($subsidiaries_store_key, null);
			Storage.local.set($companies_store_key, null);

			$rootScope.logged = false;
		},
		setCurrent:{
			user: function (user) {
				session.current.user = user;
				return Storage.local.set($current_user_store_key, user);
			},
			company: function (company) {
				for (var i = 0; i < session.list.subsidiaries; i++) {
					if (company.id == session.list.subsidiaries[i].id_empresa) {
						session.setCurrent.subsidiary(session.list.subsidiaries[i]);
						break;
					}
				}
				session.current.company = company;
				return Storage.local.set($current_company_store_key, company);;
			},
			subsidiary: function (subsidiary) {
				session.current.subsidiary = subsidiary;
				return Storage.local.set($current_subsidiary_store_key, subsidiary);
			}
		},
		setList:{
			companies: function (companies) {
				session.list.companies = companies;
				return Storage.local.set($companies_store_key, companies);
			},
			subsidiaries: function (subsidiaries) {
				session.list.subsidiaries = subsidiaries;
				return Storage.local.set($subsidiaries_store_key, subsidiaries);
			},
			routes: function (routes) {
				session.list.routes = routes;
				return Storage.local.set($routes_store_key, routes);
			}
		},
		current: {
			user: Storage.local.get($current_user_store_key, {}),
			company: Storage.local.get($current_company_store_key, {}),
			subsidiary: Storage.local.get($current_subsidiary_store_key, {})
		},
		list: {
			companies: Storage.local.get($companies_store_key, null),
			subsidiaries: Storage.local.get($subsidiaries_store_key, null),
			routes: Storage.local.get($routes_store_key, null)
		},
		start: function (info) {
			session.setList.routes(info.routes);
			session.setList.companies(info.companies);
			session.setList.subsidiaries(info.subsidiaries);

			session.setCurrent.user(info.user);
			session.setCurrent.company(info.companies[0]);
			session.setCurrent.subsidiary(info.subsidiaries[0]);
		},
		checkRoutePermission: function (route) {
			var routeAllowed = false;
			for (var i = 0; i < session.list.routes.length; i++) {
				//TODO check if parameters are allowed
				if (session.current.subsidiary.id == session.list.routes[i].id_empresa_filial 
					&& session.list.routes[i].rota.toLowerCase() == route.toLowerCase()) {
					routeAllowed = true;
				}
			}
			return routeAllowed;
		}
	}
	
	return session;
}]);

angular.module("System-Authorization").run(['$rootScope', '$location', '$http', 'Session', function ($rootScope, $location, $http, Session) {
	$rootScope.$on('$locationChangeStart', function (event, next, current) {
		//Set last route
		if ($location.$$url != settings.router.forbidden
				&& $location.$$url != settings.router.signupform
				&& $location.$$url != settings.router.authentication
				&& $location.$$url != settings.router.signupgoogle
				&& $location.$$url != settings.router.signupfacebook) {
			$rootScope.lastRoute = $location.$$url;
		}
		//Check Session
		if (!Session.current.user.id) {
			$rootScope.logged = false;
			if ($location.$$url != settings.router.forbidden
				&& $location.$$url != settings.router.signupform
				&& $location.$$url != settings.router.authentication
				&& $location.$$url != settings.router.signupgoogle
				&& $location.$$url != settings.router.signupfacebook) {
				$location.path(settings.router.authentication);
			}
		}
		else {
			$rootScope.logged = true;
		}
		if (settings.devmode) return;

		var route = angular.module('Axis-Router').Router.getRoute($location.$$url);
		
		//Check Route permission
		var $routeAllowed = Session.checkRoutePermission(route.path);

		if (!$routeAllowed
			&& $location.$$url != settings.router.forbidden
			&& $location.$$url != settings.router.signupform
			&& $location.$$url != settings.router.authentication
			&& $location.$$url != settings.router.signupgoogle
			&& $location.$$url != settings.router.signupfacebook) {
			$location.path(settings.router.forbidden);
		}
	});
}]);

angular.module("System-Authorization").factory('HeadersDefault-httpInterceptor', ['$q', '$rootScope', '$log', 'Session', function ($q, $rootScope, $log, Session) {
	return {
		request: function (config) {
		    
		    config.headers['X-Requested-With'] = 'XMLHttpRequest';
			config.headers['Rest'] = 'REST';
			config.headers['CCID'] = Session.current.company.id;
			config.headers['CSID'] = Session.current.subsidiary.id;

			return config || $q.when(config);
		},
		response: function (response) {
			return response || $q.when(response);
		},
		responseError: function (response) {
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('HeadersDefault-httpInterceptor');
}])