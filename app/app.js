﻿settings.devmode = true;
settings.debug = false;

settings.app = {
	title: 'NexAtlas',
	baseurl: 'http://app.nexatlas.com/',
	sendsAuthorizationToken: true,
	theme: 'ng-theme-light',
	language: 'pt-BR',
	services: {
		api: 'https://api.nexatlas.com/',
		facebook: 'http://facebook.com/',
		google: 'http://google.ccom/'
	},
	is_extension: window.location.protocol == 'chrome-extension:'
};
settings.router.signupform = '/app/authentication/form';
settings.router.signupgoogle = '/app/authentication/form';
settings.router.signupfacebook = '/app/authentication/form';
settings.router.forbidden = '/app/static/errors/forbidden';
settings.router.login = '/app/authentication/login';
//Default route redefining
settings.router.default_route = '/app/dashboard/i';
settings.router.authentication = '/app/authentication/login';
//Routes table
settings.router.custom.param = function (route, paramName, paramValue) {
	return "";
}
settings.router.custom.user = {
	register: '/app/user/register',
	profile: '/app/user/profile',
	logout: '/app/user/logout',
	confirmEmail: '/app/user/confirmEmail',
	lostPassword: '/app/user/lostPassword',
	checkPasswordRequest: '/app/password-request/check',
	resetPassword: '/app/password-request/resetPassword'
};
//Default components to be loaded
settings.components.default_components = [
	'login/form',
	'na/svg',
	'na/select',
	'na/general-modal',
	'na/contact-form',
	'na/terms-modal',
	'user/lost-password/form',
	'user/redefine-password/form',
	'user/profile/form'
];

//BEGIN
//TODO: Remove this functions from here
function getBase64Image(url, callback) {
	var canvas = document.createElement("canvas"),
		img = document.createElement("img");
	img.onload = function () {
		var canvas = document.createElement("canvas");
		canvas.id = toString((new Date()).getTime()) + '__canvas_id';
		canvas.width = img.width;
		canvas.height = img.height;
		var ctx = canvas.getContext("2d");
		ctx.drawImage(img, 0, 0);
		var dataURL = canvas.toDataURL("image/png");
		callback(dataURL);
	};
	img.setAttribute('crossOrigin', 'anonymous');
	img.src = url;
}


function arrayFindString(arr, s) {
	//console.log(arr);
	for (var i = 0; i < arr.length; i++) {
		if (s.indexOf(arr[i]) !== -1)
			return i;
	}
	return -1;
}
//END: TODO

console.log("host: " + window.location.hostname)

switch (window.location.hostname) {
	case 'nexatlas.com':
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://api.nexatlas.com/';
		settings.app.sendsAuthorizationToken = true;
		break;
	case 'app.nexatlas':
		settings.app.baseurl = 'http://app.nexatlas/';
		settings.app.services.api = 'http://api.nexatlas/';
		settings.app.sendsAuthorizationToken = true;
		break;
	case 'localhost':
		settings.app.baseurl = 'http://localhost:3000/';
		settings.app.services.api = 'http://localhost:8080/';
		settings.app.sendsAuthorizationToken = true;
		break;
	case 'alfa.nexatlas.com':
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://alfa.nexatlas.com/api/';
		settings.app.sendsAuthorizationToken = false;
		break;
	default:
		settings.app.baseurl = 'https://' + window.location.hostname + '/';
		settings.app.services.api = 'https://api.nexatlas.com/';
		settings.app.sendsAuthorizationToken = true;
		break;
}

// Added angular sortable view for drag-n-drop sort
angular.module('System-Authorization', ['ngSanitize', 'ng-showdown', 'ui.sortable',]);

angular.module("System-Authorization").factory("Session", ['$rootScope', function ($rootScope) {
	var $current_user_store_key = '#current$user@';
	var $current_userProfile_store_key = '#current$userProfile@';
	var $remember_current_user_store_key = '#remember$user@';
	var $current_token_store_key = '#session$token@';

	var session = {
		clear: function (callback) {
			//Storage.local.set($current_user_store_key, {});
			//Storage.local.set($current_userProfile_store_key, {});
			//Storage.local.set($remember_current_user_store_key, {});
			//Storage.local.set($current_token_store_key, null);
			//Storage.local.clear()
			localStorage.clear()
			$rootScope.logged = false;
			console.log(Storage.local.getAll());
			console.log("Cleared local storage.");

			//If a callback was specified
			if (angular.isFunction(callback)) callback(session);
		},
		define: function (objects, callback) {
			//First clears all previous objects
			session.clear();
			//Write new ones
			if (objects.token) session.token(objects.token);
			if (objects.remember) session.rememberUser(objects.remember);
			if (objects.user) session.setCurrent.user(objects.user);
			if (objects.userProfile) session.setCurrent.userProfile(objects.userProfile);

			return callback && angular.isFunction(callback) ? callback(session) : session;
		},
		rememberUser: function (remember) {
			session.current.remember = remember;
			//If session should not be recorded to local storage
			if (!session.current.remember) return remember;
			return Storage.local.set($remember_current_user_store_key, remember);
		},
		token: function (token) {
			session.current.token = token;
			//if (!session.current.remember) return token;
			return Storage.local.set($current_token_store_key, token);
		},
		setCurrent: {
			user: function (user) {
				session.current.user = user;
				//If session should not be recorded to local storage
				if (!session.current.remember) return user;
				return Storage.local.set($current_user_store_key, user);
			},
			userProfile: function (userProfile) {
				session.current.userProfile = userProfile;
				//If session should not be recorded to local storage
				if (!session.current.remember) return userProfile;
				return Storage.local.set($current_userProfile_store_key, userProfile);
			}
		},
		current: {
			//Localstorage access
			user: Storage.local.get($current_user_store_key, {}),
			userProfile: Storage.local.get($current_userProfile_store_key, {}),
			remember: Storage.local.get($remember_current_user_store_key, false),
			token: Storage.local.get($current_token_store_key)
		}
	}

	return session;
}]);

angular.module("System-Authorization").run(['$rootScope', '$location', '$http', 'Session', function ($rootScope, $location, $http, Session) {
	$rootScope.$on('$locationChangeStart', function (event, next, current) {

		//These pages do not required a logged user
		var noAuthRequiredPages = [
			settings.router.forbidden
			, settings.router.signupform
			, settings.router.authentication
			, settings.router.signupgoogle
			, settings.router.signupfacebook
			, settings.router.custom.user.register
			, settings.router.custom.user.profile
			, settings.router.custom.user.lostPassword
			, settings.router.custom.user.confirmEmail
			, settings.router.custom.user.checkPasswordRequest
			, settings.router.custom.user.resetPassword
		]

		//Set last route
		if ($location.$$path != settings.router.forbidden
				&& $location.$$path != settings.router.signupform
				&& $location.$$path != settings.router.authentication
				&& $location.$$path != settings.router.signupgoogle
				&& $location.$$path != settings.router.signupfacebook) {
			$rootScope.lastRoute = $location.$$url;
		}
		//Check Session
		if (!Session.current.user || !Session.current.user.id || !Session.current.user.active) {
			$rootScope.logged = false;
			//No authorization required pages
			//if ($location.$$url == settings.router.custom.user.register
			//	|| $location.$$url == settings.router.custom.user.profile
			//	|| $location.$$url == settings.router.custom.user.lostPassword
			//	|| $location.$$url == settings.router.custom.user.checkPasswordRequest
			//	|| $location.$$url == settings.router.custom.user.resetPassword) {
			//	return;
			//}
			if (arrayFindString(noAuthRequiredPages, $location.$$path) === -1) {
				//if (noAuthRequiredPages.indexOf($location.$$path) === -1) {
				//!= settings.router.forbidden
				//!= settings.router.signupform
				//!= settings.router.authentication
				//!= settings.router.signupgoogle
				//!= settings.router.signupfacebook
				//!= settings.router.custom.user.register
				//!= settings.router.custom.user.profile
				//!= settings.router.custom.user.lostPassword
				//!= settings.router.custom.user.checkPasswordRequest
				//!= settings.router.custom.user.resetPassword) {
				$location.path(settings.router.authentication);
			}
		}
		else {
			$rootScope.logged = true;

			//This is the case when the use tries to access a page which is only useful
			//When he is not authenticated or registered, except for forbidden page which
			//Might yet be accessed even though he is authenticated (and else pages)
			if (arrayFindString(noAuthRequiredPages, $location.$$path) > -1
				&& !($location.$$path.indexOf(settings.router.forbidden) > -1)
				&& !($location.$$path.indexOf(settings.router.custom.user.confirmEmail) > -1)
				&& !($location.$$path.indexOf(settings.router.custom.user.profile) > -1)
				&& !($location.$$path.indexOf(settings.router.custom.user.logout) > -1)) {
				//If the user is already logged in, and the condition described has a match
				//Drives it to the default route
				$location.path(settings.router.default_route);
			}
		}
	});
}]);

angular.module("System-Authorization").factory('HeadersDefault-httpInterceptor', ['$q', '$rootScope', '$log', 'Session', function ($q, $rootScope, $log, Session) {
	return {
		request: function (config) {

			config.headers['X-Requested-With'] = 'XMLHttpRequest';
			config.headers['Rest'] = 'REST';

			if (settings.app.sendsAuthorizationToken)
				config.headers['Authorization'] = 'Token ' + Session.current.token;
			//config.headers['Uid'] = Session.current.user.id;

			return config || $q.when(config);
		},
		response: function (response) {
			return response || $q.when(response);
		},
		responseError: function (response) {
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('HeadersDefault-httpInterceptor');
}]);

var System = angular.module("System", ["Sugar", "Axis", "Axis-Router", "System-Authorization"]);
System.factory("i18n", [(function () {
	var service = {
		language: {},
		changeLanguage: function (lang) {
			$.ajax({
				url: "app/i18n/" + lang + ".json",
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				scriptCharset: "utf-8",
				async: false,
				success: function (data) {
					service.language = data;
				},
				error: function (e) {
					//Error loading the language file
					console.log("Language file couldn't load.");
					console.log(e);
				}
			});
		},
		getTranslated: function (key) {
			var value = '';
			try {
				value = eval("service.language." + key);
			}
			catch (e) { }
			return value;
		}
	}

	service.changeLanguage("pt-BR");
	return function () {
		return service;
	}
})()]);

System.filter("translate", ['i18n', function (i18n) {
	return function (input) {
		return i18n.getTranslated(input);
	}
}]);

//System.factory("base64Image", [function (url, callback) {
//	var canvas = document.createElement("canvas"),
//		img = document.createElement("img");
//	img.src = url;
//	img.setAttribute('crossOrigin', 'anonymous');
//	img.onload = function () {
//		var canvas = document.createElement("canvas");
//		canvas.id = toString((new Date()).getTime()) + '__canvas_id';
//		canvas.width = img.width;
//		canvas.height = img.height;
//		var ctx = canvas.getContext("2d");
//		ctx.drawImage(img, 0, 0);
//		var dataURL = canvas.toDataURL("image/png");
//		callback(dataURL);
//	};
//}]);

System.controller("ApplicationController", ['$rootScope', '$location', '$scope', 'i18n', 'Session', function ($rootScope, $location, $scope, i18n, Session) {
	$rootScope.language = {
		instance: i18n.language
	}
	$rootScope.navigateToDefault = function () {
		$location.path(settings.router.default_route);
	}
	$rootScope.navigate = function (sPath) {
		$location.path(sPath);
	}
	$scope.navigate = function (sPath) {
		$location.path(sPath);
	}
	$scope.header = {
		title: 'NexAtlas',
		hidden: true, //false, when the app has a top bar by default
		customText: null,
		setBackgrond: function (background, color) {
			if (typeof StatusBar !== "undefined") {
				StatusBar.hide();
				StatusBar.backgroundColorByHexString(hexc(background));
			}
			$(".app-bar").css('background', background).css('color', color);
		},
		icon: {
			showBack: function (action) {
				$scope.header.setMainIcon('arrow_back', action ? action : window.history.back);
			}
		},
		setCustomText: function (text) {
			$scope.header.customText = text;
		},
		setMainIcon: function (icon, fnAction) {
			$scope.header.mainIcon = icon;
			$scope.header.mainIconAction = fnAction;
		},
		setTitle: function (title) {
			var translated;
			if (translated = i18n.getTranslated(title)) {
				$scope.header.title = translated;
				return;
			}
			$scope.header.title = title;
		},
		show: function () {
			$scope.header.hidden = false;
		},
		hide: function () {
			$scope.header.hidden = true;
		},
		actions: {
			open: false,
			list: [],
			clear: function (actions) {
				$scope.header.actions.list = [];
			},
			set: function (actions) {
				$scope.header.actions.clear();

				for (i in actions) {
					$scope.header.actions.add(actions[i].name, actions[i].icon, actions[i].click, actions[i].items, actions[i].attributes)
				}
			},
			add: function (name, icon, click, items, attributes) {
				if (typeof icon == 'function') {
					attributes = items;
					items = click;
					click = icon;
					icon = undefined;
				}
				$scope.header.actions.list.push($scope.header.actions.create(name, icon, click, items, attributes));
			},
			remove: function (id) {
				for (i in $scope.header.actions.list) {
					if ($scope.header.actions.list[i].id == id) {
						$scope.header.actions.list.splice(i, 1);
						break;
					}
				}
			},
			create: function (name, icon, click, items, attributes) {
				return { name: name, icon: icon, click: click, items: items, attributes: attributes };
			}
		},
		tabs: {
			list: [],
			visible: true,
			centered: false,
			center: function () {
				$scope.header.tabs.centered = true;
			},
			uncenter: function () {
				$scope.header.tabs.centered = false;
			},
			add: function (label, icon, active, hideLabel) {
				this.list.push({
					label: hideLabel ? '' : i18n.getTranslated(label),
					icon: icon,
					contentId: label.split('.').slice(-1)[0],
					active: active
				});
			},
			remove: function (label) {
				for (var i = 0; i < this.list.length; i++) {
					if (this.list[i].contentId == label) {
						this.list.shift(i, 1);
						return true;
					}
				}
				return false;
			},
			clear: function () {
				this.list = [];
				this.centered = false;
			},
			hide: function () {
				this.visible = false;
			},
			show: function () {
				this.visible = true;
			}
		}
	}

	//Sets the router and routes table to the $scope
	$scope.router = $rootScope.router = settings.router;
	$scope.routes = $rootScope.routes = settings.router.custom;

	//Sets the app title
	$scope.header.setTitle(settings.app.title);

	//Navigates to the default route
	$scope.navigateToDefault = function () {
		$scope.navigate($scope.router.default_route);
	}

	$rootScope.$on("axis:resolve:controller_loading:start", function () {
		$scope.header.setCustomText('');
		$scope.header.actions.clear();
		//TODO: Not to be show by default
		//$scope.header.show();
		$scope.header.setMainIcon(null, null);
		$scope.header.setBackgrond($(".app-bar").css("backgroundColor"), $(".app-bar").css("color"));
		$scope.header.tabs.clear();
		document.body.scrollTop = 0;

		//TODO: Remove this workaround from here
		$('#modal-date-picker').hide();
	});
}]);
