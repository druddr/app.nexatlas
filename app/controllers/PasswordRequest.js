﻿var PasswordRequest = (function () {
	var oPasswordRequest = {
		components: [
		],
		services: [
			'PasswordRequestService'
		],
		/// Route usage:
		/// #/app/password-request/check
		check: ['$scope', 'PasswordRequestService', function ($scope, PasswordRequestService) {
			$scope.token = { token: PasswordRequest.params.t };
			$scope.language = $scope.language.instance;

			//Does the password request
			PasswordRequestService.checkPasswordRequest({}, $scope.token).$promise.then(
				function (oChecked) {
					if (oChecked.error) {
						switch (oChecked.message) {
							case "invalidPasswordRequest":
								alert($scope.language.exceptions.invalidPasswordRequest);
								return $scope.navigate($scope.router.authentication);
								break;
							default:
								console.log(oChecked);
								return $scope.navigate($scope.router.authentication);
								break;
						}
					}

					//Checks the http status received
					if (oChecked.status != 302) return false;

					//Drives the user to the reset password screen
					$scope.navigate(oChecked.url);
				});
		}],
		/// Route usage:
		/// #/app/password-request/resetPassword
		resetPassword: ['$scope', 'PasswordRequestService', function ($scope, PasswordRequestService) {
			$scope.passwordRequest = {
				token: PasswordRequest.params.t
			};
		}],
	}

	return oPasswordRequest;
})();