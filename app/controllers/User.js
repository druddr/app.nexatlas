﻿var User = (function () {
	var oUser = {
		components: ['user/register/form'],
		services: [
			'UserService',
			'UserProfileService'
		],
		/// Route usage:
		/// #/app/user/register
		register: ['$scope', 'UserService', 'UserProfileService', 'Session',
			function ($scope, UserService, UserProfileService, Session) {
				$scope.user = {};
			}],
		/// Route usage:
		/// #/app/user/profile
		profile: ['$scope', 'UserService', 'UserProfileService', 'Session', function ($scope, UserService, UserProfileService, Session) {
			$scope.userProfile = { user_id: Session.current.user.id };
		}],
		/// Route usage:
		/// #/app/user/lostPassword
		lostPassword: ['$scope', 'UserService', function ($scope, UserService) {
			$scope.user = {};
		}],
		/// Route usage:
		/// #/app/user/confirmEmail/t=[]
		confirmEmail: ['$scope', '$rootScope', '$timeout', 'Session', 'UserService',
			function ($scope, $rootScope, $timeout, Session, UserService) {
				$scope.language = $rootScope.language.instance;
				$scope.confirmation = { token: User.params.t, userId: Session.current.user ? Session.current.user.id : null };

				//Confirms the user email
				UserService.confirmEmail({}, $scope.confirmation).$promise.then(function (oResult) {
					if (oResult.error) {
						switch (oResult.message) {
							case "invalidEmailConfirmationToken":
								alert($scope.language.exceptions.invalidEmailConfirmationToken);
								return $scope.navigate($scope.router.authentication);
							case "emailAlreadyConfirmed":
								alert($scope.language.exceptions.emailAlreadyConfirmed);
								return $scope.navigate($scope.router.authentication);
							default:
								console.log(oResult.message);
								return false
								break;
						}
					}

					//After 1 second, redirects the user to the authentication screen
					$timeout(function () {
						$scope.navigate($scope.router.authentication);
					}, 2000)
				});
			}],
		/// Route usage:
		/// #/app/user/logout
		logout: ['$scope', '$timeout', 'UserService', 'Session', function ($scope, $timeout, UserService, Session) {
			//No session user loaded
			if (!Session.current.user || !Session.current.user.id) $scope.navigate($scope.router.authentication);

			//Does the logout query
			UserService.logout({}, { token: Session.current.token }).$promise.then(
				function (response) {
					if (response.error) {
						console.log(response.message);
					}

					//Clears the user session and redirects it to authentication afterwards
					Session.clear(function (Session) {
						//After 200 miliseconds,
						$timeout(function (Session) {
							//Drives it to the default route
							//$scope.navigate($scope.router.authentication)
							console.log("Bye bye authentication. :)");
							window.location.href = "/";
							//console.log($scope);
							//$scope.$apply();
						}, 200)
					});
				});
		}]
	}

	return oUser;
})();
