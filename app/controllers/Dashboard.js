var Dashboard = (function () {
	'use strict';

	var oDashboard = {
		//Used components
		components: [
		],
		//Used services
		services: [
			'NavigationRouteService',
			'WaypointService',
			'UserService',
			'EventsService',
		],
		/// Route usage:
		/// #/app/dashboard/i
		i: ['$scope',
			'$rootScope',
			'$http',
			'$filter',
			'$timeout',
			'Session',
			'UserService',
			'NavigationRouteService',
			'WaypointService',
			'EventsService',
			function ($scope, $rootScope, $http, $filter, $timeout, Session, UserService, NavigationRouteService, WaypointService, EventsService) {
				var map;
				var defaultPanel = 'map';
				var defaultTool = 'home';
				var mapInitialCentre = [-54.5, -13.5]; //Centre of Brazil country
				$scope.language = $rootScope.language.instance;

				$scope.getScreenWidth = function () {
					var g = document.getElementsByTagName('body')[0];
					return window.innerWidth || document.documentElement.clientWidth || g.clientWidth;
				}

				// scope change of a complete route planning (CRP)
				$scope.completeRoutePlanning = {
					threeWaypoints: false,
					mapChange: false,
					meteorologyCheck: false,
					speedInput: false,
				};

				/**
				 * Maitains the last activity time and validity
				 */
				$scope.userActivity = {
					time: null,
					valid: false
				};

				// variable to check if the amount of added waypoints if the necessary
				// to send some events
				$scope.amountAddedWaypoints = 0;

				/**
				 * Sends an event to the service
				 */
				$scope.sendEvent = function ($eventType) {
					// console.log("SENDING: " + $eventType);
					EventsService.generic({}, { type: $eventType })
					.$promise.then(
						function (response) {

						},
						function (error) {
							console.error(error);
						}
					);
				}

				/**
				 * Begins user interaction time monitoring
				 */
				$scope.initUserActivity = function () {
					//Initializes user activity
					$scope.userActivity.time = new Date().getTime();
					$scope.userActivity.valid = true;
				}

				/**
				 * Resets the user active
				 */
				$scope.setActiveUser = function (renew) {
					//Checks the user activity timestamp
					$scope.checkUserActivity();

					//Gets the latest date for new
					$scope.userActivity.time = new Date().getTime();

					//If it's not to renew and it is still valid
					if (renew && $scope.userActivity.valid) {
						return;
					}

					var renewObject = {
						user_id: Session.current.user.id, //User related to this auth
						t: Session.current.token, //Used as a reference
						time: Math.floor(new Date().getTime() / 1000) //Gets the auth death time from mili to seconds
					}

					//Requests a new user token
					UserService.renewActivity({}, renewObject).$promise.then(function (oNewAuth) {
						if (oNewAuth.error) return console.log(oNewAuth.message);

						//Renews the user token
						Session.token(oNewAuth.token);

						//Sets the user activity valid
						$scope.initUserActivity();
					});
				}

				/**
				 * Checks the user activity
				 */
				$scope.checkUserActivity = function () {
					//Gets the difference in time (seconds) and casts to minutes
					var currentTimeDifference = (new Date().getTime()) - $scope.userActivity.time;
					var timeDiff = currentTimeDifference / (1000 * 60);

					//If the difference is minor than 30 minutes,
					//User activity is still valid
					if (timeDiff < 30)
						return $scope.userActivity.valid = true

					//Makes the user activity invalid already
					return $scope.userActivity.valid = false;
				}

				$scope.clearCompleteRoutePlanningScope = function () {
					$scope.completeRoutePlanning = {
						threeWaypoints: false,
						mapChange: false,
						meteorologyCheck: false,
						speedInput: false,
					};
				}

				// send a complete route planning event and reset the CRP scope
				$scope.sendCompleteRoutePlanningEvent = function () {
					var completeRoutePlanningScope = $scope.completeRoutePlanning;
					// console.log(completeRoutePlanningScope);
					if (completeRoutePlanningScope.threeWaypoints &&
						completeRoutePlanningScope.mapChange &&
						completeRoutePlanningScope.meteorologyCheck &&
						completeRoutePlanningScope.speedInput) {
						$scope.sendEvent('complete_route_planning');
						$scope.clearCompleteRoutePlanningScope();
					}
				};

				// send a simple route planning event
				$scope.sendSimpleRoutePlanningEvent = function () {
					$scope.sendEvent('simple_route_planning');
				};

				// if (!mapboxgl.supported({ failIfMajorPerformanceCaveat: true })) {
				//   return;
				// }

				// Default left panel
				$scope.panel = defaultPanel;

				// Default right panel tool
				$scope.tool = defaultTool;

				$scope.goToPanel = function (panel, callback) {

					//Renews the activity monitorer
					$scope.setActiveUser(true);

					// Closes the layer tool if open
					if ($scope.tool === 'layer') {
						$scope.tool = defaultTool;
					}

					// clears the search if changing panel from search
					if ($scope.panel === 'search') {
						$scope.clearSearch();
					}

					panel = panel || defaultPanel;
					$scope.panel = (panel != $scope.panel)
						? panel
						: defaultPanel;
					if ($scope.panel === 'contact') {
						$scope.contactFormRef.clearMessage();
					}
					$scope.closeDescriptionMenu();
				};

				/**
				 * Defines a specific tool
				 */
				$scope.goToTool = function (tool, callback) {
					// checks if the layers listener closed the tool
					if ($scope.layersToolJustClosed) {
						$scope.layersToolJustClosed = false;
						return;
					}

					//Renews the activity monitorer
					$scope.setActiveUser(true);

					tool = tool || defaultTool;
					$scope.tool = (tool != $scope.tool)
						? tool
						: defaultTool;
					$scope.closeDescriptionMenu();

					if (tool === 'layer') {
						$(document).on('mousedown.closeLayersTool', $scope.closeLayersToolListener);
					}
				};

				// NOTE: Direcionar para pesquisa quando estiver no painel de rotas
				$scope.goToSearch = function () {

					//Renews the activity monitorer
					$scope.setActiveUser(true);

					$scope.panel = 'search';
					var elem = document.querySelector('#pesquisa');
					if (elem)
						elem.focus();
					$scope.closeDescriptionMenu();
				};

				/*
				 * Hides search (shows default panel) or displays search panel
				 */
				$scope.toggleSearch = function () {

					if ($scope.panel != 'search')
						$scope.goToSearch();
					else {
						//Renews the activity monitorer
						$scope.setActiveUser(true);

						$scope.panel = defaultPanel;
						$scope.clearSearch();
					}
					$scope.closeDescriptionMenu();

					// Closes the layers tool if open
					if ($scope.tool === 'layer') {
						$scope.tool = defaultTool;
					}
				};

				/*
				 * Hides the panel when it's not routes or settings
				 * Panel
				 */
				$scope.hidePanelAndTool = function () {

					if ($scope.panel != 'routes' && $scope.panel != 'settings')
						$scope.panel = defaultPanel;
					$scope.tool = defaultTool;
					$scope.closeDescriptionMenu();
					$scope.clearSearch();
				};

				//--------------------------------
				// MAP CONFIGURATION
				//--------------------------------

				var fitBounds = [
					[-74.5, -34.1],
					[-34, 5.7]
				];

				var satSource = {
					type: "raster",
					minzoom: 3,
					maxzoom: 7,
					tileSize: 256,
					scheme: "tms",
					tiles: ["https://a.tiles.nexatlas.com/sat/1/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/sat/1/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/sat/1/{z}/{x}/{y}.png"]
				}

				mapboxgl.accessToken = 'pk.eyJ1IjoiZmFicmljaW9kb3Nhbmpvc3NpbHZhIiwiYSI6ImNpbjFsaXZmNDBiMXZ2N2trNmF4YXB6czUifQ.uSpSadMQQK36UuduDQJYIw';
				map = new mapboxgl.Map({
					container: "mapaDiv",
					style: "mapbox://styles/mapbox/streets-v9",
					attributionControl: false,
					failIfMajorPerformanceCaveat: true,
					minZoom: 2,
					center: mapInitialCentre,
					zoom: 3
				});

				map.once("load", function () { // avoids falling back to default size
					map.resize();
					// sets a listner to resize events
					window.addEventListener('resize', function (e) {
						map.resize();
					});
					window.addEventListener('orientationchange', function (e) {
						map.resize();
					});

					map.fitBounds(fitBounds);
					//    map.showTileBoundaries = true;

					map.addSource("mapbox-satellite", {
						"type": "raster",
						"url": "mapbox://mapbox.satellite",
						"tileSize": 256
					});
					map.addLayer({
						"id": "mapbox-satellite",
						"type": "raster",
						"source": "mapbox-satellite",
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-wac", {
						"type": "raster",
						"minzoom": 3,
						"maxzoom": 10,
						"tileSize": 256,
						"scheme": "tms",
						"tiles": ["https://a.tiles.nexatlas.com/wac/4/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/wac/4/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/wac/4/{z}/{x}/{y}.png"]
					});
					map.addLayer({
						"id": "nexatlas-wac",
						"type": "raster",
						"source": "nexatlas-wac",
						"minzoom": 2,
						"maxzoom": 10,
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-erch", {
						"type": "raster",
						"minzoom": 3,
						"maxzoom": 10,
						"tileSize": 256,
						"scheme": "tms",
						"tiles": ["https://a.tiles.nexatlas.com/erch/3/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/erch/3/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/erch/3/{z}/{x}/{y}.png"]
					});
					map.addLayer({
						"id": "nexatlas-erch",
						"type": "raster",
						"source": "nexatlas-erch",
						"minzoom": 2,
						"maxzoom": 10,
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-ercl", {
						"type": "raster",
						"minzoom": 3,
						"maxzoom": 10,
						"tileSize": 256,
						"scheme": "tms",
						"tiles": ["https://a.tiles.nexatlas.com/ercl/3/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/ercl/3/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/ercl/3/{z}/{x}/{y}.png"]
					});
					map.addLayer({
						"id": "nexatlas-ercl",
						"type": "raster",
						"source": "nexatlas-ercl",
						"minzoom": 2,
						"maxzoom": 10,
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-rea", {
						"type": "raster",
						"minzoom": 3,
						"maxzoom": 12,
						"tileSize": 256,
						"scheme": "tms",
						"tiles": ["https://a.tiles.nexatlas.com/rea/2/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/rea/2/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/rea/2/{z}/{x}/{y}.png"]
					});
					map.addLayer({
						"id": "nexatlas-rea",
						"source": "nexatlas-rea",
						"type": "raster",
						"minzoom": 2,
						"maxzoom": 12,
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-reh", {
						"type": "raster",
						"minzoom": 3,
						"maxzoom": 13,
						"tileSize": 256,
						"scheme": "tms",
						"tiles": ["https://a.tiles.nexatlas.com/reh/1/{z}/{x}/{y}.png", "https://b.tiles.nexatlas.com/reh/1/{z}/{x}/{y}.png", "https://c.tiles.nexatlas.com/reh/1/{z}/{x}/{y}.png"]
					});
					map.addLayer({
						"id": "nexatlas-reh",
						"source": "nexatlas-reh",
						"type": "raster",
						"minzoom": 2,
						"maxzoom": 13,
						layout: {
							"visibility": "none"
						}
					});

					map.addSource("nexatlas-sat", satSource);
					map.addLayer({
						"id": "nexatlas-sat",
						"source": "nexatlas-sat",
						"type": "raster",
						"minzoom": 2,
						"maxzoom": 7,
						layout: {
							"visibility": "none"
						},
						paint: {
							"raster-opacity": 0.8
						}
					});

					// Pin
					map.addSource("nexatlas-pin", {
						"type": "geojson",
						"data": $scope.pinFeatures
					});
					// Style Pin POINT
					map.addLayer({
						id: 'nexatlas-pin-point',
						source: 'nexatlas-pin',
						type: 'circle',
						paint: {
							'circle-radius': 6.7,
							'circle-color': "#1E262A",
							"circle-opacity": 1,
							"circle-stroke-width": 3.3,
							"circle-stroke-color": "#1E262A",
							"circle-stroke-opacity": .3,
						},
						filter: [
							'all',
							['==', 'point_type', 'pin'],
							['in', '$type', 'Point'],
						]
					});

					// location pin
					map.addSource("nexatlas-location-pin", {
						"type": "geojson",
						"data": $scope.locationPinFeatures
					});
					// Style location Pin POINT
					// TODO: change style and opacity!!!
					map.addLayer({
						id: 'nexatlas-location-pin-point',
						source: 'nexatlas-location-pin',
						type: 'circle',
						paint: {
							'circle-radius': 6.7,
							'circle-color': "#F00",
							"circle-opacity": 0,
							"circle-stroke-width": 3.3,
							"circle-stroke-color": "#F00",
							"circle-stroke-opacity": 0,
						},
						filter: [
							'all',
							['==', 'point_type', 'location'],
							['in', '$type', 'Point'],
						]
					});

					map.addSource("nexatlas-rota", {
						"type": "geojson",
						"data": {
							"type": "FeatureCollection",
							"features": []
						}
					});
					// Style route destination / departure waypoint
					map.addLayer({
						id: 'nexatlas-rota-main-pontos-1',
						source: 'nexatlas-rota',
						type: 'circle',
						paint: {
							'circle-radius': 2,
							'circle-color': "#fff",
							"circle-opacity": 1,
							"circle-stroke-width": 6,
							"circle-stroke-color": "#00d102",
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'main'],
							['in', '$type', 'Point']
						]
					});
					// Style route simple waypoint
					map.addLayer({
						id: 'nexatlas-rota-simple-pontos-1',
						source: 'nexatlas-rota',
						type: 'circle',
						paint: {
							'circle-radius': 4,
							'circle-color': "#fff",
							"circle-opacity": 1,
							"circle-stroke-width": 3,
							"circle-stroke-color": "#00d102",
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'simple'],
							['in', '$type', 'Point']
						]
					});
					// Style route line
					map.addLayer({
						id: "nexatlas-rota-linha",
						type: "line",
						source: "nexatlas-rota",
						layout: {
							"line-cap": "round",
							"line-join": "round"
						},
						paint: {
							"line-color": "#04b400",
							"line-width": 6,
							"line-opacity": 0.8
						},
						filter: ["in", "$type", "LineString"]
					});
					map.addLayer({
						id: 'nexatlas-rota-main-pontos-2',
						source: 'nexatlas-rota',
						type: 'circle',
						paint: {
							'circle-radius': 2,
							'circle-color': "#fff",
							"circle-opacity": 1,
							"circle-stroke-width": 6,
							"circle-stroke-color": "#00d102",
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'main'],
							['in', '$type', 'Point']
						]
					});
					// Style route simple waypoint
					map.addLayer({
						id: 'nexatlas-rota-simple-pontos-2',
						source: 'nexatlas-rota',
						type: 'circle',
						paint: {
							'circle-radius': 4,
							'circle-color': "#fff",
							"circle-opacity": 1,
							"circle-stroke-width": 3,
							"circle-stroke-color": "#00d102",
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'simple'],
							['in', '$type', 'Point']
						]
					});

					// Style text for destination / departure waypoints
					map.addLayer({
						id: "nexatlas-rota-texto-main",
						type: "symbol",
						source: "nexatlas-rota",
						layout: {
							"text-field": "{title1}",
							"text-offset": [
								0, 0.6
							],
							"text-anchor": "top",
							"text-size": 13,
						},
						paint: {
							"text-color": "#000",
							"text-halo-color": "#fff",
							"text-halo-width": 2.5,
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'main'],
							['in', '$type', 'Point']
						]
					});

					// Style text for simple waypoints
					map.addLayer({
						id: "nexatlas-rota-texto-simple",
						type: "symbol",
						source: "nexatlas-rota",
						layout: {
							"text-field": "{title1}",
							"text-offset": [
								0, 0.6
							],
							"text-anchor": "top",
							"text-size": 11
						},
						paint: {
							"text-color": "#000",
							"text-halo-color": "#fff",
							"text-halo-width": 2.5
						},
						filter: [
							"all",
							['==', 'waypoint_type', 'simple'],
							['in', '$type', 'Point']
						]
					});


					// Create the ruler styles to be drawn on map
					map.addSource("nexatlas-regua", {
						"type": "geojson",
						"data": $scope.reguaFeatures
					});
					// Style POINT 1
					map.addLayer({
						id: 'nexatlas-regua-pontos1',
						source: 'nexatlas-regua',
						type: 'circle',
						paint: {
							'circle-radius': 4,
							'circle-color': "#ff9d00",
							"circle-opacity": 1,
							"circle-stroke-width": 3,
							"circle-stroke-color": "#fff"
						},
						filter: ['in', '$type', 'Point']
					});
					// Style RULER LINE
					map.addLayer({
						id: "nexatlas-regua-linha",
						type: "line",
						source: "nexatlas-regua",
						layout: {
							"line-join": "round",
							"visibility": "none"
						},
						paint: {
							"line-color": "#ff9d00",
							"line-width": 6,
							"line-opacity": 0.8
						},
						filter: ["in", "$type", "LineString"]
					});
					// Style POINT 2
					map.addLayer({
						id: 'nexatlas-regua-pontos2',
						source: 'nexatlas-regua',
						type: 'circle',
						layout: {
							"visibility": "none"
						},
						paint: {
							'circle-radius': 4,
							'circle-color': "#ff9d00",
							"circle-opacity": 1,
							"circle-stroke-width": 3,
							"circle-stroke-color": "#fff"
						},
						filter: ['in', '$type', 'Point']
					});
					// Style ruler TEXT
					map.addLayer({
						id: "nexatlas-regua-texto",
						type: "symbol",
						source: "nexatlas-regua",
						layout: {
							"text-field": "{title}",
							// "text-font": ["Montserrat light", ],
							"text-size": 11,
							"text-offset": [
								0, 0.6
							],
							"text-anchor": "top",
							"visibility": "none"
						},
						paint: {
							"text-color": "#000",
							"text-halo-color": "#fff",
							"text-halo-width": 2.5
						},
						filter: ["in", "$type", "Point"]
					});
				});

				//--------------------------------
				// MAP EVENTS
				//--------------------------------

				var longPressTimeout = null;
				var longPressStartEvents = ["touchstart", ];
				var longPressCancelEvents = [
					"touchstop",
					"mouseup",
					"dragstart",
					"rotatestart",
					"zoomstart",
					"boxzoomstart",
					"movestart",
					"pitch"
				];

				// Cancel map rotation
				// Click + drag
				map.dragRotate.disable();
				// Pinch + drag
				map.touchZoomRotate.disableRotation();

				/*
				 * Cancels context menu event
				 */
				document.addEventListener('contextmenu', function (event) {
					event.preventDefault();
				});

				/**
				 * Programs each longpress event
				 * based on the same timout
				 */
				longPressStartEvents.forEach(function (eventName) {
					map.on(eventName, function (event) {
						if (event.type === 'touchstart') {
							if (event.originalEvent.touches.length === 1) {
								longPressTimeout && clearTimeout(longPressTimeout);
								longPressTimeout = setTimeout(function () {
									longPress(event);
								}, 1200);
							}
						}
						//else {
						//	//After 1 second, closes side menu
						//	if (event.originalEvent.button == 0) {
						//		longPressTimeout && clearTimeout(longPressTimeout);
						//		longPressTimeout = setTimeout(function () {
						//			//longPress(event);

						//		}, 1000);
						//	}
						//}
					});
				});

				longPressCancelEvents.forEach(function (eventName) {
					map.on(eventName, function (event) {
						//    	console.log("cancel");
						//      if (event.type=="dragstart" && $scope.modoRegua && $scope.reguaNumPontos==2) {
						//        map.once("mouseup", function() {
						//          $scope.reguaFeatures.features[1]=null;
						//          $scope.reguaNumPontos=1;
						//          $scope.ligaSegueMouse();
						//        });
						//      }
						if (longPressTimeout) {
							clearTimeout(longPressTimeout);
							longPressTimeout = null;
						}
					});
				});

				function longPress(event) {
					$scope.searchCoords(event.lngLat);
					$scope.setPin(event.lngLat);
				}

				/**
				 * Sets canvas cursor style to a crosshair
				 */
				map.getCanvas().style.cursor = "crosshair";

				// a method to handle all single click events
				$scope.singleClickMethod = function (event) {
					// checks if the ruler tool is active
					if ($scope.modoRegua) {
						$scope.clickRegua(event.lngLat);
					} else {
						/*
						 * On left mouse button click,
						 * A search should be triggered
						 */
						if (!event.originalEvent.button == 0) {
							//Renews the activity monitorer
							$scope.setActiveUser(true);
						}

						// if the device is capable of firing a touch signal. the search by click is not triggered
						if (event.originalEvent.sourceCapabilities) {
							if (!event.originalEvent.sourceCapabilities.firesTouchEvents) {
								//Executes a search on click of left mouse button
								$scope.searchCoords(event.lngLat);
								$scope.setPin(event.lngLat);
							}
						} else {
							$scope.searchCoords(event.lngLat);
							$scope.setPin(event.lngLat);
						}
					}
					singleClickTimer = null;
				};

				// the single click timer that is canceled on double click events
				var singleClickTimer = null;

				// handle all click events on the map
				map.on("click", function (event) {
					// if a timer wasn't set for a single click
					if (singleClickTimer === null) {
						// sets the timer to fire a single click action
						singleClickTimer = setTimeout(function () {
							$scope.singleClickMethod(event);
						}, 350);
					} else {
						// if not, it should be considered a double click
						// double clicks just zoom in
						clearTimeout(singleClickTimer);
						singleClickTimer = null;
					}
				});

				/*
				 * On right mouse button down,
				 * A search should be triggered;
				 * On left mouse button down,
				 * With ruler mode - A ruler click should be triggered
				 *
				 */
				map.on("mousedown", function (event) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					// if (event.originalEvent.button == 0) {
					// if ($scope.modoRegua) {
					// $scope.clickRegua(event.lngLat);
					// }
					// else {
					// 	$scope.searchCoords(event.lngLat);
					// }
					// else {
					//   if (menuControl["Esquerda"].ativo && ["menuRota", "menuMsg"].indexOf(menuControl["Esquerda"].idAtivo) < 0) {
					//     // menuHide("Esquerda");
					//   }
					//   if (menuControl["Direita"].ativo) {
					//     // menuHide("Direita");
					//   }
					//   if (menuDescAtivo) {
					//     hideMenuDesc();
					//   }
					// }
					// }
					//TODO: Disabled by request - We never know, it can be reactivated whatsoever
					//else if (event.originalEvent.button == 2) {
					//	$scope.searchCoords(event.lngLat);
					//}
				});

				/*
				 *
				 * TODO: REMOVED LEFT CLICK FROM MOUSEDOWN/MOUSEUP EVENT
				 */
				//map.on("mouseup", function (event) {
				//	if (event.originalEvent.button == 0) {
				//		if (!$scope.modoRegua) {
				//			$scope.searchCoords(event.lngLat);
				//		}
				//	}
				//});

				//--------------------------------------------------------------
				// Pin Config
				//--------------------------------------------------------------
				$scope.pinFeatures = turf.featureCollection([]);

				$scope.setPin = function (lngLat) {
					var p1 = turf.point([
						lngLat.lng, lngLat.lat
					], { "title": "", "point_type": 'pin' });
					$scope.pinFeatures.features = [p1, ];
					map.getSource("nexatlas-pin").setData($scope.pinFeatures);
					map.setLayoutProperty("nexatlas-pin-point", "visibility", "visible");
				};
				$scope.clearPin = function () {
					$scope.pinFeatures.features = [];
					map.getSource("nexatlas-pin").setData($scope.pinFeatures);
					$scope.clearLocationPin();
				}

				//--------------------------------------------------------------
				// Location Pin Config
				//--------------------------------------------------------------
				$scope.locationPinFeatures = turf.featureCollection([]);

				$scope.setLocationPin = function (lngLat) {
					var p1 = turf.point([
						lngLat.lng, lngLat.lat
					], { "title": "", "point_type": "location" });
					$scope.locationPinFeatures.features = [p1, ];
					map.getSource("nexatlas-location-pin").setData($scope.locationPinFeatures);
					map.setLayoutProperty("nexatlas-location-pin-point", "visibility", "visible");
				};
				$scope.clearLocationPin = function () {
					$scope.locationPinFeatures.features = [];
					map.getSource("nexatlas-location-pin").setData($scope.locationPinFeatures);
				}

				//--------------------------------
				// RULER CONFIG
				//--------------------------------

				$scope.modoRegua = false;

				$scope.reguaFeatures = turf.featureCollection([]);

				var btnRegua = document.querySelector("#botaoRegua");

				$scope.startModoRegua = function () {
					if (!$scope.modoRegua) {
						map.setLayoutProperty("nexatlas-regua-pontos1", "visibility", "visible");
						map.setLayoutProperty("nexatlas-regua-pontos2", "visibility", "visible");
						map.setLayoutProperty("nexatlas-regua-texto", "visibility", "visible");
						map.setLayoutProperty("nexatlas-regua-linha", "visibility", "visible");
						// menuHide("Direita");
						// $scope.hidePanelAndTool();

						btnRegua.style.backgroundColor = '#0066ff';
						btnRegua.style.opacity = '1';

						$scope.modoRegua = true;
					}
				};

				$scope.stopModoRegua = function () {
					if ($scope.modoRegua) {
						map.off("mousemove");
						map.setLayoutProperty("nexatlas-regua-pontos1", "visibility", "none");
						map.setLayoutProperty("nexatlas-regua-pontos2", "visibility", "none");
						map.setLayoutProperty("nexatlas-regua-texto", "visibility", "none");
						map.setLayoutProperty("nexatlas-regua-linha", "visibility", "none");
						$scope.reguaFeatures.features = [];
						map.getSource("nexatlas-regua").setData($scope.reguaFeatures);

						btnRegua.style.backgroundColor = '';
						btnRegua.style.opacity = '';

						$scope.modoRegua = false;
					}
				};

				$scope.startStopModoRegua = function () {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					if ($scope.modoRegua) {
						$scope.stopModoRegua();
					} else {
						$scope.startModoRegua();
					}
				};

				var updateReguaControl = {
					intervalId: null,
					lngLat: null,
					LastLngLat: null
				};

				$scope.ligaSegueMouse = function () {
					updateReguaControl.intervalId = setInterval(function () {
						$scope.updateRegua();
					}, 100);

					map.on("mousemove", function (event) {
						updateReguaControl.lngLat = [event.lngLat.lng, event.lngLat.lat];
					});
				};

				$scope.reguaNumPontos = 0;

				$scope.clickRegua = function (lngLat) {
					if ($scope.reguaNumPontos != 1) {
						var p1 = turf.point([
							lngLat.lng, lngLat.lat
						], { "title": "" });
						var p2 = turf.point([
							lngLat.lng, lngLat.lat
						], { "title": "" });
						$scope.reguaFeatures.features = [
							p1,
							p2,
							turf.lineString([p1.geometry.coordinates, p2.geometry.coordinates])
						];
						map.getSource("nexatlas-regua").setData($scope.reguaFeatures);

						$scope.reguaNumPontos = 1;
						$scope.ligaSegueMouse();
					} else {
						map.off("mousemove");
						clearInterval(updateReguaControl.intervalId);
						updateReguaControl.intervalId = null;
						$scope.reguaNumPontos = 2;
						$scope.updateRegua();
					}
				};

				$scope.updateRegua = function () {
					if (!updateReguaControl.lngLat || (updateReguaControl.LastLngLat && updateReguaControl.lngLat[0] == updateReguaControl.LastLngLat[0] && updateReguaControl.lngLat[1] == updateReguaControl.LastLngLat[1])) {
						return;
					}
					//    console.log("draw");
					var features = $scope.reguaFeatures.features;

					features[1] = turf.point(updateReguaControl.lngLat);

					var distancia = $scope.nauticalMileDistance(features[0], features[1]);
					var bearing = $scope.bearing(features[0], features[1]);
					features[1].properties.title = distancia.toFixed(1) + " nm   " + bearing;

					//    features[2] = $scope.generateLinestringArch(features[0], features[1]);
					features[2] = turf.lineString([features[0].geometry.coordinates, features[1].geometry.coordinates]);

					map.getSource("nexatlas-regua").setData($scope.reguaFeatures);

					updateReguaControl.lngLat = updateReguaControl.LastLngLat;
				};

				//--------------------------------
				// SEARCH AND CONVERTS
				//--------------------------------

				$scope.degreeToDMS = function (degree, isLat) {
					var deg = Math.abs(degree);
					var min = (deg - Math.floor(deg)) * 60;
					var sec = (min - Math.floor(min)) * 60;
					deg = Math.floor(deg);
					min = Math.floor(min);
					sec = Math.round(sec);

					var dms = '';
					if (sec >= 60) {
						sec = "59";
					}
					if (isLat) {
						if (deg < 10) {
							dms += '0';
						}
					} else {
						if (deg < 100 && deg >= 10) {
							dms += '0';
						} else if (deg < 10) {
							dms += '00';
						}
					}
					dms += deg + ' ' + (Math.abs(min) >= 10
						? min
						: '0' + min) + ' ' + (Math.abs(sec) >= 10
						? sec
						: '0' + sec) + ' ';
					if (isLat) {
						dms += degree >= 0
							? 'N'
							: 'S';
					} else {
						dms += degree >= 0
							? 'E'
							: 'W';
					}
					return dms;
				};

				/**
				 * Defines search radius
				 */
				$scope.getSearchRadius = function (lngLat) {
					var pxPerMm = 1 / 0.26;
					var errorPixels = pxPerMm * 10;
					var point = map.project(lngLat);
					var pointRef = point.add(new mapboxgl.Point(errorPixels, 0));
					var lngLatRef = map.unproject(pointRef);
					var nauticalMilesRadius = $scope.nauticalMileDistance(turf.point([lngLat.lng, lngLat.lat]), turf.point([lngLatRef.lng, lngLatRef.lat]));
					if (nauticalMilesRadius > 2) {
						nauticalMilesRadius = Math.ceil(nauticalMilesRadius);
					} else {
						nauticalMilesRadius = nauticalMilesRadius.toFixed(1);
						if (nauticalMilesRadius < 0.3) {
							nauticalMilesRadius = 0.3;
						}
					}
					return nauticalMilesRadius;
				};

				/*
				 * A research of waypoints is triggered
				 * Based on coordinates of an specific point
				 * Plus a search radius
				 */
				$scope.searchCoords = function (lngLat) {
					$scope.searchText = $scope.degreeToDMS(lngLat.lat, true) + " / " + $scope.degreeToDMS(lngLat.lng, false);
					$scope.search($scope.getSearchRadius(lngLat));
					// menuShow("Esquerda", "menuPesquisa");
					$scope.panel = 'search';
					$scope.closeDescriptionMenu();
					// $scope.goToSearch();

					// Closes the layers tool if open
					if ($scope.tool === 'layer') {
						$scope.tool = defaultTool;
					}
				};

				/**
				 * Generates a linestring between two points (origin and destination
				 */
				$scope.generateLinestringArch = function (from, to) {
					var linestring = turf.lineString([from.geometry.coordinates, to.geometry.coordinates]);
					var displacedPoint = turf.point([to.geometry.coordinates[0], from.geometry.coordinates[1]]);
					var distLong = $scope.nauticalMileDistance(from, displacedPoint);
					if (distLong > 500) {
						var distance = $scope.nauticalMileDistance(from, to);
						var arch = [];
						arch.push(from.geometry.coordinates);
						var segmentSize = 50;
						var segmentNumber = 1;
						while (segmentSize * segmentNumber < distance) {
							var segmento = turf.along(linestring, segmentSize * segmentNumber * 1.852, 'kilometers');
							arch.push(segmento.geometry.coordinates);
							segmentNumber++;
						}
						arch.push(to.geometry.coordinates);
						linestring.geometry.coordinates = arch;
					}
					return linestring;
				};

				//-------------------------------------------------------------------

				//  $scope.mapInfo = map.getZoom().toFixed(1);
				//
				//  map.on("zoomend", function (event) {
				//    $scope.mapInfo = map.getZoom().toFixed(1);
				//  });

				//--------------------------------

				$scope.camadasAtivas = {
					"base": "mapa",
					"baseAer": null,
					"re": null,
					"sat": null,
					"sigwx": null,
					"ventos": null
				};
				//  $scope.metAltitude = { "sigwx": "sfc", "ventos": "fl050" };
				var mapaStyles = {
					"nexatlas-wac": {
						// "water": {
						// 	"fill-color": "#c7eafc"
						// },
						// "landcover_crop": {
						// 	"fill-color": "#b8bc7c"
						// },
						// "landcover_grass": {
						// 	"fill-color": "#d0d691"
						// },
						// "landcover_scrub": {
						// 	"fill-color": "#cad37f"
						// }
					},
					"nexatlas-erc": {
						// "water": {
						// 	"fill-color": "#e5ffff"
						// },
						// "landcover_crop": {
						// 	"fill-color": "#e5ebd3"
						// },
						// "landcover_grass": {
						// 	"fill-color": "#dee3d1"
						// },
						// "landcover_scrub": {
						// 	"fill-color": "#e5ebd3"
						// }
					}
				};
				var reloadMet = {
					"nexatlas-sat": null
				};

				$scope.ativaCamada = function (grupo, camada) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					// fires complete route planning event
					// a meteorologyCheck
					if (camada === 'nexatlas-sat') {
						$scope.completeRoutePlanning.meteorologyCheck = true;
						$scope.sendCompleteRoutePlanningEvent();
					} else {
						// any map change
						$scope.completeRoutePlanning.mapChange = true;
						$scope.sendCompleteRoutePlanningEvent();
					}

					if (!$scope.camadasAtivas[grupo] || $scope.camadasAtivas[grupo] != camada) {
						if ($scope.camadasAtivas[grupo] && $scope.camadasAtivas[grupo] != "mapa") {
							for (var camadaMet in reloadMet) {
								if (camada == camadaMet) {
									clearInterval(reloadMet[camada]);
								}
							}
							map.setLayoutProperty($scope.camadasAtivas[grupo], "visibility", "none");
						}

						if (camada != "mapa") {
							for (var camadaMet in reloadMet) {
								if (camada == camadaMet) {
									//map.getSource(camada)._pyramid.reload();
									//map.styles.sources[camadaMet].reload();
									//console.log(map.styles.sources[camada]);
									//console.log(camada);
									//console.log(map.style);
									reloadMet[camada] = window.setInterval(function () {
										//console.log(camada);
										//console.log(map.style);
										//map.getSource(camada)._pyramid.reload();
										//map.getSource(camada).reload();
									}, 61000);
								}
							}
							map.setLayoutProperty(camada, "visibility", "visible");
						}

						for (var camadaStyle in mapaStyles) {
							if (camada.indexOf(camadaStyle) == 0) {
								for (var layer in mapaStyles[camadaStyle]) {
									for (var prop in mapaStyles[camadaStyle][layer]) {
										if (!$scope.camadasAtivas[grupo]) {
											if (!mapaStyles["default"]) {
												mapaStyles["default"] = {};
											}
											if (!mapaStyles["default"][layer]) {
												mapaStyles["default"][layer] = {};
											}
											mapaStyles["default"][layer][prop] = map.getPaintProperty(layer, prop);
										}
										map.setPaintProperty(layer, prop, mapaStyles[camadaStyle][layer][prop]);
									}
								}
							}
						}

						$scope.camadasAtivas[grupo] = camada;
						// updates the zoom level
						$scope.updateLayerZoom(camada);
					}
				};

				// method to update the zoom level when a layer is selected
				$scope.updateLayerZoom = function (layer) {
					var layerSource = map.getSource(layer);
					var maxZoom = layerSource.maxzoom,
						currentZoom = map.getZoom();
					if (currentZoom > maxZoom) {
						// sets the zoom (smoothly) based on the layer max
						// zoom and based on the user's current zoom level
						map.flyTo({
							zoom: maxZoom - 1,
							speed: 1,
							curve: 1,
						});
					}
				}

				$scope.desativaCamada = function (grupo, camada) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					if ($scope.camadasAtivas[grupo] && $scope.camadasAtivas[grupo] == camada) {
						for (var camadaMet in reloadMet) {
							if (camada == camadaMet) {
								window.clearInterval(reloadMet[camada]);
							}
						}
						map.setLayoutProperty(camada, "visibility", "none");
						$scope.camadasAtivas[grupo] = null;
						if (grupo == "baseAer") {
							for (var layer in mapaStyles["default"]) {
								for (var prop in mapaStyles["default"][layer]) {
									map.setPaintProperty(layer, prop, mapaStyles["default"][layer][prop]);
								}
							}
						}
					}
				};

				$scope.isCamadaAtiva = function (grupo, camada) {
					return $scope.camadasAtivas[grupo] && $scope.camadasAtivas[grupo] == camada;
				};

				$scope.ativaDesativaCamada = function (grupo, camada) {
					if ($scope.isCamadaAtiva(grupo, camada)) {
						$scope.desativaCamada(grupo, camada);
					} else {
						$scope.ativaCamada(grupo, camada);
					}
				};

				function binarySearch(ar, el, compare_fn) {
					var m = 0;
					var n = ar.length - 1;
					while (m <= n) {
						var k = (n + m) >> 1;
						var cmp = compare_fn(el, ar[k]);
						if (cmp > 0) {
							m = k + 1;
						} else if (cmp < 0) {
							n = k - 1;
						} else {
							return k;
						}
					}
					return -m - 1;
				}

				$scope.carregaVectorTile = function (e) {
					if ($scope.camadasAtivas["base"] && $scope.camadasAtivas["base"] == "mapbox-satellite") {
						return false;
					}
					else if ($scope.camadasAtivas["baseAer"] && $scope.camadasAtivas["baseAer"] == "nexatlas-wac") {
						var z = camadasTiles["nexatlas-wac"][e.coord.z];
						for (var i = 0; i < z.length; i++) {
							if (z[0] > e.coord.y) {
								return false;
							}
							if (z[0] == e.coord.y && z[1] <= e.coord.x && z[2] >= e.coord.x) {
								return true;
							}
						}
						return false;
					} else if ($scope.camadasAtivas["baseAer"] && e.coord.z >= 6 && e.coord.z < 10) {
						return false;
					}
					return true;
				}
				//};

				//-------------------------------------------------------------------
				// ROUTE TOOLING
				//-------------------------------------------------------------------

				$scope.route = {
					id: null,
					user_id: Session.current.user.id,
					name: $scope.language.dashboard.route.unnamedRoute,
					favourite: false,
					creation_date: new Date().getTime(),
					ground_speed: null,
					ts: null,
					clone: false,
					points: turf.featureCollection([])
				};

				//Empty origin/destination objects
				$scope.originId = null;
				$scope.destinationId = null;
				$scope.routeOriginOrDestinationChanged = false;
				//$scope.routeOriginOrDestinationChanged = function () {
				//	return $scope.route.points.features[0].properties.id === $scope.originId
				//			&& $scope.route.points.features[$scope.route.points.features.length - 1].properties.id === $scope.destinationId;
				//}

				//  $scope.$watch("route.points.features", function() {
				//    $scope.updateRouteTotal();
				//    $scope.updateRotaGrafica();
				//    if ($scope.route.id || $scope.route.points.features.length>0) {
				//      $scope.updateServerRoute();
				//    }
				//  }, true);

				$scope.icons = {
					fixo: "ion-arrow-up-b",
					aeroporto: "ion-plane",
					auxilio: "ion-android-checkbox-outline-blank",
					municipio: "ion-ios-circle-filled",
					coords: "ion-ios-location"
				};

				$scope.nauticalMileDistance = function (p1, p2) {
					return turf.distance(p1, p2) / 1.852;
				};

				$scope.distanceFromPrevious = function (posRota) {
					return $scope.nauticalMileDistance($scope.route.points.features[posRota], $scope.route.points.features[posRota - 1]).toFixed(1) + " nm";
				};

				$scope.ground_speed = "";
				$scope.distanciaTotal = 0;
				$scope.tempoSoloTotal = "--";

				/**
				 * Updates route totalizers
				 *
				 */
				$scope.updateRouteTotal = function () {
					var soma = 0;
					for (var i = 1; i < $scope.route.points.features.length; i++) {
						soma += $scope.nauticalMileDistance($scope.route.points.features[i], $scope.route.points.features[i - 1]);
					}
					$scope.distanciaTotal = soma.toFixed(1);


					var vel = ("" + $scope.ground_speed).replace(/,/g, ".");

					//console.log("-- Ground speed");
					//console.log(vel);

					if (!isNaN(vel) && parseFloat(Number(vel)) == vel && !isNaN(parseFloat(vel, 10)) && vel > 9) {
						//Updates the ground speed by first
						$scope.updateGroundSpeed(vel);
						//Calculates time
						var horas = soma / vel;
						$scope.tempoSoloTotal = $scope.formatDuration(horas);
					} else {
						$scope.tempoSoloTotal = "--";
					}

					//Updating origin/destination
					//If origin is present
					if ($scope.route.points.features.length > 0
						&& $scope.route.points.features[0].properties.id != $scope.originId) {

						//Route origin/destination can only change when a route already exists
						$scope.routeOriginOrDestinationChanged = ($scope.route.id !== null); // ($scope.originId == null);

						//Updates origin object id
						$scope.originId = $scope.route.points.features[0].properties.id;

						//console.log("-- Route origin set to: ");
						//console.log($scope.originId);
					}

					//If destination is present
					if ($scope.route.points.features.length > 1
						&& $scope.route.points.features[$scope.route.points.features.length - 1].properties.id != $scope.destinationId) {
						//Route origin/destination can only change when a route already exists
						$scope.routeOriginOrDestinationChanged = ($scope.route.id !== null);

						//Updates destination object id
						$scope.destinationId = $scope.route.points.features[$scope.route.points.features.length - 1].properties.id;

						//console.log("-- Route destination set to: ");
						//console.log($scope.destinationId);
					}
				};

				$scope.formatDuration = function (horas) {
					var minTemp = (horas - Math.floor(horas)) * 60;
					var hor = Math.floor(horas);
					var min = Math.round(minTemp);

					if (min >= 60) {
						min = "59";
					} else if (min < 10) {
						min = "0" + min;
					}

					if (hor === 0) {
						return min + " min";
					}
					return hor + " h " + min + " min";
				};

				/**
				 * Estimates time from - to points
				 */
				$scope.estimateFromPrevious = function (routePosition) {
					var vel = ("" + $scope.route.ground_speed).replace(/,/g, ".");
					if (!isNaN(vel) && parseFloat(Number(vel)) == vel && !isNaN(parseFloat(vel, 10)) && vel > 0) {
						var hours = $scope.nauticalMileDistance($scope.route.points.features[routePosition], $scope.route.points.features[routePosition - 1]) / vel;
						return $scope.formatDuration(hours);
					} else {
						return "--";
					}
				};

				/**
				 * Calculates bearing of a route position (waypoint)
				 */
				$scope.bearingFromPrevious = function (routePosition) {
					if (routePosition == 0)
						return "";

					var from = $scope.route.points.features[routePosition - 1];
					var to = $scope.route.points.features[routePosition];
					return $scope.bearing(from, to);
				};

				/**
				 * Calculates bearing from - to points
				 */
				$scope.bearing = function (origem, destino) {
					var bearing = turf.bearing(origem, destino);

					var wmm = new WorldMagneticModel();
					var now = new Date();
					var julian_day = (now - new Date(now.getFullYear(), 0, 1)) / (1000 * 60 * 60 * 24);
					var anoParcial = now.getFullYear() + (julian_day / 365);
					var decComponents = wmm.declination(0.0, origem.geometry.coordinates[1], origem.geometry.coordinates[0], anoParcial);
					var dec = decComponents[5];

					var bearingMag = Math.round(bearing - dec);
					if (bearingMag == 0) {
						bearingMag = 360;
					} else if (bearingMag < 0) {
						bearingMag = 360 + bearingMag;
					}

					if (bearingMag >= 10 && bearingMag < 100) {
						bearingMag = "0" + bearingMag;
					} else if (bearingMag < 10) {
						bearingMag = "00" + bearingMag;
					}

					var bearing = Math.round(bearing);
					if (bearing == 0) {
						bearing = 360;
					} else if (bearing < 0) {
						bearing = 360 + bearing;
					}

					if (bearing >= 10 && bearing < 100) {
						bearing = "0" + bearing;
					} else if (bearing < 10) {
						bearing = "00" + bearing;
					}

					return bearingMag + "° (M)";
				};

				$scope.updateRotaWaypointsType = function (points) {
					var features = points.features;
					for (var pointIndex = 0; pointIndex < features.length; pointIndex++) {
						var point = features[pointIndex];
						if (pointIndex === 0 || pointIndex === features.length - 1) {
							point.properties["waypoint_type"] = 'main';
						} else {
							point.properties["waypoint_type"] = 'simple';
						}
					}
					points.features = features;
					return points;
				}

				$scope.updateRotaGrafica = function () {
					$scope.clearPin();
					var source = map.getSource("nexatlas-rota");
					if (!source)
						return;

					var rotaGrafica = angular.copy($scope.route.points);

					rotaGrafica = $scope.updateRotaWaypointsType(rotaGrafica);

					if (rotaGrafica.features.length >= 2) {
						var linestring = turf.lineString([]);
						linestring.geometry.coordinates.push(rotaGrafica.features[0].geometry.coordinates);
						for (var p = 1; p < rotaGrafica.features.length; p++) {
							var linestringSegmento = $scope.generateLinestringArch(rotaGrafica.features[p - 1], rotaGrafica.features[p]);
							linestring.geometry.coordinates =
								linestring.geometry.coordinates.concat(linestringSegmento.geometry.coordinates);
						}
						rotaGrafica.features.push(linestring);
					}

					source.setData(rotaGrafica);
				};

				//----------------------------------------------------

				$scope.getHost = function () {
					if (window.location.href.indexOf("/ui/") < 0) {
						return "https://api.nexatlas.com";
					} else {
						return "/api";
					}
				};

				$scope.searchText = "";
				$scope.searchRunning = false;
				$scope.searchResult = turf.featureCollection([]);

				$scope.clearSearch = function () {
					$scope.searchText = "";
					$scope.searchMessage = null;
					$scope.searchRunning = false;
					$scope.searchResult.features = [];
					$scope.clearPin();
					// menuHide("Esquerda");
					// $scope.hidePanelAndTool();
				};

				/*
				 * Asks the server for results based on a textual query
				 */
				$scope.search = function (nauticalMilesRadius) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					// clears search on new search
					$scope.searchResult.features = [];

					$scope.searchMessage = null;

					var coordPattentGeneral = /^([NSWEOL])?([-+])?(\d{2,})\.?(\d{1,})?\s?(\d{2,})?\.?(\d{1,})?\s?(\d{2,})?\s?([NSWEOL])?/gi;
					var pattern1 = /(\d{2})\s?(\d{2})\s?(\d{2})\s?([NS]{1})\s?\/\s?(\d{2,3})\s?(\d{2})\s?(\d{2})\s?([EWLO]{1})/gi;
					var pattern1Format = "$1 $2 $3 $4 / $5 $6 $7 $8";
					var pattern2 = /([-+]?\d{2})\s?([0-5]{1}[0-9]{1})\.(\d{0,3})\s?\/\s?([-+]?\d{2,3})\s?([0-5]{1}[0-9]{1})\.(\d{0,3})/gi;
					var pattern2Format = "$1 $2.$3 / $4 $5.$6";
					var pattern3 = /([NS])\s?(\d{2})\.?(\d{1,4})?\s?\/\s?([EWLO])\s?(\d{2,3})\.?(\d{1,4})?/gi;
					var pattern3Format = "$1$2.$3 / $4$5.$6";
					var part1, part2;

					var pesq = $scope.searchText.trim().toUpperCase();
					var pesqParts;
					var cutPosition, itemDecimal;

					if (pesq.match(pattern1)) {
						//pattern1 => 00[ ]00[ ]00[ ]S[ ]/[ ][0]00[ ]00[ ]00[ ]W
						//[ ] => means a non-compulsory whitespace
						//[0] => means a non-compulsory digit
						//Just checking for spaces to be applied
						//Corrects for incorrect spaces or formatting of the accepted coordinate point
						pesq = pesq.replace(pattern1, pattern1Format);

					} else if (pesq.match(pattern2)) {
						//pattern2 => [+-]00[ ]00.00[0][ ]/[ ][+-][0]00[ ]00.00[0]
						//[ ] => means a non-compulsory whitespace
						//[0] => means a non-compulsory digit
						//Just checking for spaces to be applied
						//Corrects for incorrect spaces or formatting of the accepted coordinate point
						pesq = pesq.replace(pattern2, pattern2Format);

						//Splits for formatting as pattern1
						pesqParts = pesq.split(" / ");

						pesqParts.forEach(function (item, index) {
							item = item.trim();

							//Finds a pattern like 00.[000]
							cutPosition = item.search(/[0-5][0-9]\.(\d){0,3}/g);

							//Gets the decimal part of the string
							itemDecimal = item.slice(cutPosition);

							//Gets only decimal part of the number and transforms to degrees
							itemDecimal = parseFloat(itemDecimal) % 1;
							itemDecimal = (itemDecimal > 0) ? (itemDecimal * 60) : itemDecimal;

							//Padds the item for the output string
							itemDecimal = parseInt(Math.round(itemDecimal)).toString();
							itemDecimal = "00".substring(0, 2 - itemDecimal.length) + itemDecimal;

							//Puts the new format back to the output string
							item = item.replace(/\.(\d){0,3}/g, " " + itemDecimal);

							//Attaches the correct quadrant letter
							if (index == 0 && item.match(/-/g)) {
								part1 = item.replace(/-/g, "");
								part1 = part1 + " S";
							} else if (index == 0) {
								part1 = item.replace(/-/g, "");
								part1 = part1 + " N";
							} else if (index == 1 && item.match(/-/g)) {
								part2 = item.replace(/-/g, "");
								part2 = part2 + " W";
							} else {
								part2 = item.replace(/-/g, "");
								part2 = part2 + " E";
							}
						});

						//Rebuilds the string
						pesq = part1 + " / " + part2;
					} else if (pesq.match(pattern3)) {
						//pattern3 => [NS]00.0[000][ ]/[ ][EWOL]00[0].0[000]
						//[ ] => means a non-compulsory whitespace
						//[0] => means a non-compulsory digit
						//Just checking for spaces to be applied
						//Corrects for incorrect spaces or formatting of the accepted coordinate point
						pesq = pesq.replace(pattern3, pattern3Format);

						//Splits for formatting as pattern1
						pesqParts = pesq.split(" / ");

						pesqParts.forEach(function (item, index) {
							item = item.trim();

							//Finds a pattern like 00.0[000]
							cutPosition = item.search(/(\d){2}\./g);

							//Gets the decimal part of the string
							itemDecimal = parseFloat(item.slice(cutPosition));

							//Calculates the conversion of the decimal degrees to DMS
							var absolute = Math.abs(itemDecimal);
							var degrees = Math.floor(absolute);
							var minutesNotTruncated = (absolute - degrees) * 60;
							var minutes = Math.floor(minutesNotTruncated);
							var seconds = Math.floor((minutesNotTruncated - minutes) * 60);

							//Decides whether the format should be 3 or 2 digit (longitude or latitude)
							var degreesPad = item.match(/[NS]/gi) ? "00" : "000";

							//Pads degrees
							degrees = degrees.toString();
							degrees = degreesPad.substring(0, degreesPad.length - degrees.length) + degrees;

							//Pads minutes
							minutes = minutes.toString();
							minutes = "00".substring(0, 2 - minutes.length) + minutes;

							//Pads seconds
							seconds = seconds.toString();
							seconds = "00".substring(0, 2 - seconds.length) + seconds;

							//Sets the correct quadrant to be used
							var quadrant =
								item.match(/[NS]/gi) ?
									item.match(/[N]/gi) ? "N" : "S"
									: item.match(/[O]/gi) ? "W" : "E";

							//Builds the final string
							item = degrees + " " + minutes + " " + seconds + " " + quadrant;

							//Attaches the correct quadrant letter
							if (index == 0) {
								part1 = item;
							} else {
								part2 = item;
							}
						});

						//Rebuilds the string
						pesq = part1 + " / " + part2;

					} else if (pesq.match(coordPattentGeneral)) {
						$scope.clearSearch();
						$scope.searchMessage = $scope.language.labels.wrongSearchPattern;
						$scope.searchText = pesq;
						return;
					}

					if (pesq.length < 2) {
						$scope.searchResult.features = [];
						return;
					}

					$scope.searchRunning = true;
					$scope.searchMessage = "";

					var params = {
						text: pesq
					};
					if (nauticalMilesRadius) {
						params.r = nauticalMilesRadius;
					}

					//Server requests the results of the query
					WaypointService.get(params, {}).$promise.then(function (oResult) {
						// console.log(oResult);

						//Cancels the query loader
						$scope.searchRunning = false;

						if (oResult.featureCollection.features.length == 0) {
							$scope.searchResult.features = [];
							$scope.searchMessage = $scope.language.labels.noResult;
						} else if (oResult.featureCollection.features.length == 1
									&& oResult.featureCollection.features[0].properties.waypoint_type_code_name == "COORDS") {
							$scope.searchResult = oResult.featureCollection;
							$scope.searchResult.features.forEach(function (point) {
								$scope.generatePointTitles(point);
							});
							$scope.searchMessage = $scope.language.labels.noAdditionalResult;
						} else {
							$scope.searchMessage = null;
							$scope.searchResult = oResult.featureCollection;
							$scope.searchResult.features.forEach(function (point) {
								$scope.generatePointTitles(point);
							});
						}
					}, function (oResult) {
						//In case of serious error
						$scope.searchRunning = false;
						$scope.searchResult.features = [];
						if (response.status == 400) {
							$scope.searchMessage = response.statusText;
						} else if (response.status == 401) {
							$scope.usuarioLogado.id = null;
							$scope.usuarioLogado.nome = null;
							$scope.usuarioLogado.sobrenome = null;
							$scope.usuarioLogado.email = null;
							$scope.usuarioLogado.logado = false;

							$scope.exibirLogin = true;
						} else if (response.status == -1) {
							$scope.searchMessage = $scope.language.labels.noResultConnectionFailure;
						} else {
							console.log(response.status);
							$scope.searchMessage = $scope.language.labels.noResultConnectionFailure;
						}
					});
				};

				/*
				 * Generates each query result title
				 * Based on the type of the waypoint
				 */
				$scope.generatePointTitles = function (point) {

					//Current possible type code names
					//'AD',
					//'HELPN',
					//'COORDS',
					//'CITY',
					//'FIX',
					//'VOR',
					//'VOR/DME',
					//'NDB',

					//Sets the second title based on the waypoint type
					point.properties.title2 =
						$scope.language.dashboard.waypointTypes[point.properties.waypoint_type_code_name];

					switch (point.properties.waypoint_type_code_name) {
						case "AD":
						case "HELPN":
							point.properties.cardTitle = point.properties.icao;
							point.properties.title1 = point.properties.icao;
							point.properties.title2 = point.properties.name;
							break;
						case "CITY":
							point.properties.cardTitle = point.properties.name;
							point.properties.title1 = point.properties.name;
							break;
						case "FIX":
							point.properties.cardTitle = point.properties.code;
							point.properties.title1 = point.properties.code;
							break;
						case "NDB":
						case "VOR":
						case "VOR/DME":
							point.properties.cardTitle = point.properties.code;
							point.properties.title1 = point.properties.code;
							point.properties.title2 = point.properties.waypoint_type_code_name + " " + point.properties.name;
							break;
						case "COORDS":
							point.properties.cardTitle = point.properties.name;
							point.properties.title1 = point.properties.name.replace(/\//, "");
							break;
						default:
							//point.properties.title1 = point.properties.code;
							point.properties.cardTitle = point.properties.name;
							point.properties.title1 = point.properties.name;
							break;
					}
				};

				/*
				 * Inserts a point into the route
				 */
				$scope.insertRouteWaypoint = function (i) {
					var points = $scope.route.points.features;
					var newPoint = $scope.searchResult.features[i];
					$scope.clearSearch(true);

					if (points.length == 0) {
						points.push(newPoint);
					} else if (points.length == 1) {
						points.push(newPoint);
						//Moment where a route becomes officially a route
						$scope.route.creation_date = new Date().getTime();
						//Frames the route at the screen
						$scope.enquadrarRota();
					} else if (points.length == 2) {
						points.splice(1, 0, newPoint);
					} else {
						var shorterDistance = 10000000;
						var shorterDistancePosition;
						for (var i = 1; i < points.length; i++) {
							var line = turf.lineString([
								points[i].geometry.coordinates,
								points[i - 1].geometry.coordinates
							]);
							var nearestPoint = turf.pointOnLine(line, newPoint);
							var dist = turf.distance(newPoint, nearestPoint);
							if (dist < shorterDistance) {
								shorterDistance = dist;
								shorterDistancePosition = i;
							}
						}
						points.splice(shorterDistancePosition, 0, newPoint);
					}

					// menuShow("Esquerda", "menuRota");
					$scope.panel = 'routes';
					$scope.closeDescriptionMenu();
					$scope.route.updated_on = new Date().getTime();

					$scope.updateRouteTotal();
					$scope.updateRotaGrafica();
					if ($scope.route.id || $scope.route.points.features.length > 0) {
						$scope.updateServerRoute();
					}

					// check the amount of added waypoints
					$scope.amountAddedWaypoints++;
					if ($scope.amountAddedWaypoints === 2) {
						$scope.sendSimpleRoutePlanningEvent();
					} else if ($scope.amountAddedWaypoints === 3) {
						$scope.completeRoutePlanning.threeWaypoints = true;
						$scope.sendCompleteRoutePlanningEvent();
					}
				};

				/**
				 * Removes a waypoint from a route
				 * If it is a valid route, with ID, removes from server as well
				 */
				$scope.removeRouteWaypoint = function (i) {

					if ($scope.route.points.features[i].properties.id === $scope.originId
						|| $scope.route.points.features[i].properties.id === $scope.destinationId) {

						//Gets the waypoint by it's index for removal
						var removedPoint = $scope.route.points.features.splice(i, 1)[0];

						//Updates the route object
						$scope.updateRouteTotal()
						$scope.updateRotaGrafica()
						return $scope.updateServerRoute();
					}

					//Gets the waypoint by it's index for removal
					var removedPoint = $scope.route.points.features.splice(i, 1)[0];

					////Checks whether the item is an array
					//removedPoint = angular.isArray(removedPoint) ? removedPoint[0] : removedPoint;

					//console.log(removedPoint)

					if ($scope.route.id || $scope.route.points.features.length > 0) {
						//If it is a valid route
						//Grouping the info to send on to the server
						if (!removedPoint.properties
							|| !removedPoint.properties.id
							|| !$scope.route.id) {
								return;
						}

						//console.log("-- Removing waypoint:");
						//console.log(removedPoint);

						var waypointInfo = {
							id: removedPoint.properties.id,
							route_id: $scope.route.id
						};

						//console.log("-- Waypoint info:");
						//console.log(waypointInfo);

						//TODO: check if something at -> $scope.updateServerRoute(); will be missing
						//Executes a server removal of the item
						return NavigationRouteService.removeWaypoint({}, waypointInfo).$promise.then(function (oResult) {
							//Error, then the process should stop
							if (oResult.error) return console.log($scope.language.exceptions[oResult.message]);

							//Corrects updated_on time of the route
							$scope.route.updated_on = $scope.toJsTime(oResult.route.updated_on);

							//console.log("-- Removed waypoint from route:");
							//console.log(oResult.waypoint);

							$scope.updateRouteTotal();
							$scope.updateRotaGrafica();
							//$scope.route.updated_on = new Date().getTime();
							$scope.amountAddedWaypoints--;

							//Updates recent routes
							$scope.setRecentRoute($scope.route);

							return $scope.route
						});
					}
				}

				/**
				 * Clears the route object
				 */
				$scope.limparRota = function () {

					//Clears scope ground_speed
					$scope.ground_speed = null;

					$scope.route.id = null;
					$scope.route.name = $scope.language.dashboard.route.unnamedRoute;
					$scope.route.ground_speed = null;
					$scope.route.favourite = false;
					$scope.route.clone = false;
					$scope.route.creation_date = null;
					$scope.route.updated_on = null;
					$scope.route.points.features = [];

					//Updates functions
					$scope.updateRouteTotal();
					$scope.updateRotaGrafica();
					$scope.limpaRotaServidor();

					//Clears origin/destination objects
					$scope.originId = null;
					$scope.destinationId = null;

					//console.log("-- Cleared origin.");
					//console.log("-- Cleared destination.");

					$scope.amountAddedWaypoints = 0;
					$scope.clearCompleteRoutePlanningScope();
				};

				/**
				 * Centres a point at the map center
				 */
				$scope.centrePoint = function (point) {
					map.easeTo({
						center: point.geometry.coordinates
					});
				};

				$scope.framePoint = function (point) {
					$scope.clearLocationPin();
					$scope.setLocationPin({
						lat: point.geometry.coordinates[1],
						lng: point.geometry.coordinates[0]
					});
					var waypointTypeCode = point.properties.waypoint_type_code_name;
					var zoom;

					if (waypointTypeCode == "FIX"
						|| waypointTypeCode == "VOR"
						|| waypointTypeCode == "VOR/DME"
						|| waypointTypeCode == "NDB") {
						zoom = 8;
					} else if ($scope.camadasAtivas["baseAer"]) {
						zoom = 9;
					} else if ($scope.camadasAtivas["re"]) {
						zoom = 10;
					} else {
						if (waypointTypeCode == "AD") {
							zoom = 14;
						} else if (waypointTypeCode == "COORDS") {
							zoom = 16;
						} else {
							zoom = 12;
						}
					}

					//Flies to the specified point
					map.flyTo({
						center: point.geometry.coordinates,
						zoom: zoom
					});

					if ($scope.getScreenWidth() < 640) {
						$scope.toggleSearch();
					}
				};

				$scope.enquadrarRota = function () {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					// menuHide("Direita");
					// $scope.hidePanelAndTool();
					var points = $scope.route.points.features;

					// checks if it's needed to make the left padding bigger
					// this checks if the left panel is open or not
					var leftBarWidth = document.getElementById('barraEsquerda').offsetWidth;
					var panelWidth = document.getElementById('painelEsquerda').offsetWidth + leftBarWidth;
					// var totalWidth = getScreenWidth(); TODO: Is it existing somewhere?
					var totalWidth = screen.width;
					var leftPadding = 80;

					if (totalWidth > panelWidth && $scope.panel !== 'map') {
						leftPadding = 260;
					}

					if (points.length == 0) {
						map.fitBounds(fitBounds, {
							padding: {
								top: 80,
								bottom: 80,
								left: leftPadding,
								right: 80
							}
						});
					} else if (points.length == 1) {
						$scope.framePoint(points[0]);
					} else {
						var bbox = turf.bbox($scope.route.points);

						map.fitBounds([
							[bbox[0], bbox[1]],
							[bbox[2], bbox[3]]
						], {
							padding: {
								top: 80,
								bottom: 80,
								left: leftPadding,
								right: 80
							}
						});
					}
				};

				$scope.zoomIn = function () {
					//Renews the activity monitorer
					$scope.setActiveUser(true);
					// menuHide("Direita");
					// $scope.hidePanelAndTool();
					map.zoomIn();
				};

				$scope.zoomOut = function () {
					//Renews the activity monitorer
					$scope.setActiveUser(true);
					// menuHide("Direita");
					// $scope.hidePanelAndTool();
					map.zoomOut();
				};

				//-------------------------------------------------------------------
				$scope.logout = function () {
					$scope.deslogando = true;

					//No session user loaded
					if (!Session.current.user || !Session.current.user.id) $scope.navigate($scope.router.authentication);

					//Does the logout query
					UserService.logout({}, { token: Session.current.token }).$promise.then(
						function (response) {
							if (response.error) {
								console.log(response.message);
							}

							//Clears the user session and redirects it to authentication afterwards
							Session.clear(function (Session) {
								//After 200 miliseconds,
								$timeout(function (Session) {
									//Drives it to the default route
									//$scope.navigate($scope.router.authentication)
									console.log("Bye bye authentication. :)");
									window.location.href = "/";
									//console.log($scope);
									//$scope.$apply();
								}, 200)
							});
						});


					//$http({
					//	method: "post",
					//	url: $scope.getHost() + "/logout",
					//	timeout: 10000,
					//	withCredentials: true
					//}).then(function (response) {
					//	$scope.deslogando = false;
					//	$scope.usuarioLogado.id = null;
					//	$scope.usuarioLogado.nome = null;
					//	$scope.usuarioLogado.sobrenome = null;
					//	$scope.usuarioLogado.email = null;
					//	$scope.usuarioLogado.logado = false;

					//	$scope.mensagem.nome = "";
					//	$scope.mensagem.email = "";

					//	$scope.limparRota();
					//	$scope.recentRoutes = [];
					//}, function (response) {
					//	$scope.deslogando = false;
					//});

					//$scope.hidePanelAndTool();
				};

				//-------------------------------------------------------------------

				$scope.getSessao = function () {
					$http({
						method: "get",
						url: $scope.getHost() + "/sessao",
						timeout: 4000,
						withCredentials: true
					}).then(function (response) {
						var dados = response.data;

						if (dados.route) {
							var route = dados.route[0];
							//Generate pointe titles
							route.points.features.forEach(function (point, i) {
								$scope.generatePointTitles(point);
							});
							$scope.route = route;
							map.once("load", function () {
								$scope.updateRouteTotal();
								$scope.updateRotaGrafica();
								$scope.enquadrarRota();
							});
						}

						if (!dados.id) {
							$scope.usuarioLogado.id = null;
							$scope.usuarioLogado.nome = null;
							$scope.usuarioLogado.sobrenome = null;
							$scope.usuarioLogado.email = null;
							$scope.usuarioLogado.logado = false;
						} else {
							$scope.usuarioLogado.id = dados.id;
							$scope.usuarioLogado.nome = dados.nome;
							$scope.usuarioLogado.sobrenome = dados.sobrenome;
							$scope.usuarioLogado.email = dados.email;
							$scope.usuarioLogado.logado = true;

							$scope.mensagem.nome = dados.nome;
							$scope.mensagem.email = dados.email;

							$scope.loadRecentRoutes();
						}
					}, function (response) {
						window.setTimeout(function () {
							$scope.getSessao();
						}, 5000);
					});
				};

				// TODO: uncomment if necessary
				// $scope.getSessao();

				//-------------------------------------------------------------------

				/**
				 * Sets a route name, based on its waypoints
				 */
				$scope.setRouteName = function () {

					$scope.route.points.features.forEach(function (point, i) {
						if (i == 0) {
							$scope.route.name = $scope.completeName(point);
						} else if (i == $scope.route.points.features.length - 1) {
							$scope.route.name += " - " + $scope.completeName(point);
						}
					});

					//Sets the default route name
					$scope.route.title1 = $scope.route.name;
					$scope.route.cardTitle = $scope.route.name;
				}

				/**
				 * Sorter function for orderBy filter
				 */
				$scope.sortRoutes = function (route) {
					return route.updated_on;
				}

				/**
				 * Creates a new route based on added waypoints
				 *
				 */
				$scope.createRoute = function (waypoints) {
					var creatingRoute;

					//First, sets the route name (it will be saved newly)
					$scope.setRouteName();

					//Copies route object
					creatingRoute = angular.copy($scope.route);

					//Sets the default route name
					// $scope.language.dashboard.route.unnamedRoute;
					//Defines a creation date and updated date
					creatingRoute.creation_date = $scope.toServerTime(creatingRoute.creation_date); //miliseconds to seconds (server)
					creatingRoute.updated_on =
						(creatingRoute.updated_on ? $scope.toServerTime(creatingRoute.updated_on) : null); //miliseconds to seconds (server)
					creatingRoute.waypoints = waypoints;

					//console.log("-- Creating route with waypoints:");
					//console.log(creatingRoute.waypoints);

					//console.log("-- Needs to be cloned?");
					//console.log(creatingRoute.clone);

					return NavigationRouteService.create({}, creatingRoute).$promise.then(function (oRoute) {
						//If an error happened
						if (oRoute.error)
							return console.log($scope.language.exceptions[oRoute.message]);

						//console.log("-- Just created a route!");
						//console.log(oRoute.route);

						//Sets properties that were server created
						$scope.route.id = oRoute.route.id;
						$scope.route.name = oRoute.route.name;
						$scope.route.favourite = (oRoute.route.favourite == 1);
						$scope.route.ground_speed = oRoute.route.ground_speed > 0 ? oRoute.route.ground_speed : null;
						$scope.route.creation_date = $scope.toJsTime(oRoute.route.creation_date);
						$scope.route.updated_on = (oRoute.route.updated_on ? $scope.toJsTime(oRoute.route.updated_on) : null);
						//$scope.route.waypoints = oRoute.waypoints;

						//Updates points
						$scope.route.waypoints = $scope.updatePointInfo(oRoute.waypoints);

						////Corrects point identifiers (can have updated ids/route ids)
						//oRoute.waypoints.forEach(function (item, index) {
						//	console.log("-- -- Waypoint: " + $scope.route.points.features[index].properties.name + " - id switching:");
						//	console.log($scope.route.points.features[index].properties.id + " => " + item.id);

						//	//Sets the current ids of waypoints
						//	$scope.route.points.features[index].properties.id = item.id;
						//	$scope.route.points.features[index].properties.route_id = item.route_id;

						//	//Resets origin/destination id
						//	if (index === 0)
						//		$scope.originId = $scope.route.points.features[index].properties.id;
						//	else if (index === ($scope.route.points.features.length - 1))
						//		$scope.destinationId = $scope.route.points.features[index].properties.id;
						//})

						//console.log("Scope route:");
						//console.log($scope.route);

						//Gets the returning route item
						//route = oRoute.route;

						//Sets a recent route
						$scope.setRecentRoute($scope.route);

						return $scope.route;
					});
				}

				/**
				 * Updates route points info
				 */
				$scope.updatePointInfo = function (routeWaypoints) {

					//console.log("-- Updating point info:")
					//console.log(routeWaypoints)

					//Corrects point identifiers (can have updated ids/route ids)
					routeWaypoints.forEach(function (item, index) {
						//console.log("-- -- Waypoint: " + $scope.route.points.features[index].properties.name + " - id switching:");
						//console.log($scope.route.points.features[index].properties.id + " => " + item.id);

						//Sets the current ids of waypoints
						$scope.route.points.features[index].properties.id = item.id;
						$scope.route.points.features[index].properties.route_id = item.route_id;

						//Resets origin/destination id
						if (index === 0)
							$scope.originId = $scope.route.points.features[index].properties.id;
						else if (index === ($scope.route.points.features.length - 1))
							$scope.destinationId = $scope.route.points.features[index].properties.id;
					})

					return routeWaypoints;
				}

				/**
				 * Inserts a route waypoint based on all waypoints such route should comprise
				 */
				$scope.addRouteWaypoint = function (waypoints) {
					var route;

					route = angular.copy($scope.route);

					//Sets waypoints at the route object
					route.waypoints = waypoints;
					//Updates the modified date of the route
					route.updated_on = $scope.toServerTime(new Date().getTime()); //miliseconds to seconds

					//In case we already have
					return NavigationRouteService.addWaypoints({}, route).$promise.then(function (oResult) {
						if (oResult.error) {
							return console.log($scope.language.exceptions[oResult.message]);
						}
						//Gets the response of the route manipulation (route item)
						route = oResult.route;
						$scope.route.updated_on = route.updated_on = $scope.toJsTime(route.updated_on);

						//Gets the route waypoints and
						//Updates em
						//$scope.updatePointInfo(oResult.waypoints);
						//oResult.route. = $scope.updatePointInfo(oRoute.route);
						$scope.route.waypoints = $scope.updatePointInfo(oResult.waypoints);  //oResult.waypoints;

						//Sets a recent route
						route = $scope.setRecentRoute($scope.route);

						return route;
					});
				}

				/*
				 * Sets a route as recent route
				 */
				$scope.setRecentRoute = function (route) {
					//var route = angular.copy($scope.route);

					route = $scope.prepareRecentRoute(route);

					//console.log("-- Setting recent route: ");
					//console.log(route);

					if ($scope.recentRoutes.length == 0 || $scope.recentRoutes[0].id != route.id) {
						//Filters the recent routes, removing a previous version of the updated recent route
						$scope.recentRoutes =
							$scope.recentRoutes.filter(function (item) {
								return !(item.id == route.id);
							})

						//console.log('Adding route to recent:');
						//console.log(route);
						$scope.recentRoutes.unshift(route);
					} else {
						$scope.recentRoutes[0] = route;
					}

					return route;
				}

				/*
				 * Executes a server update of a route
				 */
				$scope.updateServerRoute = function () {
					var waypoints = [];//, originOrDestinationChanged = true;
					//If less than 2 points were specified
					if ($scope.route.points.features.length < 2) return;

					//Extracts just useful waypoint info
					var waypoints = $scope.route.points.features.map(function (point) {
						var item = {};
						item.id = point.properties.id;
						item.name = point.properties.name;
						item.route_id = !$scope.route.id ? null : $scope.route.id;
						item.waypoint_type_id = point.properties.waypoint_type_id;
						item.waypoint_type_code_name = point.properties.waypoint_type_code_name;
						item.longitude = point.geometry.coordinates[0];
						item.latitude = point.geometry.coordinates[1];
						return item;
					});

					//Defines whether the route is new or not
					//In case the user changed origin or destination
					if ($scope.routeOriginOrDestinationChanged) {
						//originOrDestinationChanged = false;
						//Clears the route id
						$scope.route.id = null;
						$scope.route.clone = true;

						//turns off change notifier flag
						$scope.routeOriginOrDestinationChanged = false;
					}

					var route;

					//New route needs to be created
					if (!$scope.route.id) {
						route = $scope.createRoute(waypoints);

						//Stop clone process
						$scope.route.clone = false;
					} else {
						route = $scope.addRouteWaypoint(waypoints);
					}
				};

				$scope.limpaRotaServidor = function () {
					//$http({
					//	method: "post",
					//	url: $scope.getHost() + "/plano",
					//	headers: {
					//		'Content-Type': 'application/x-www-form-urlencoded'
					//	},
					//	timeout: 10000,
					//	data: $.param({ noid: true }),
					//	withCredentials: true
					//}).then(function (response) { }, function (response) { });
				};

				//-------------------------------------------------------------------
				// USEFUL HELPERS
				//-------------------------------------------------------------------

				/**
				 * Prepares the time to PHP server time which works based on seconds
				 * Not miliseconds, like JS
				 */
				$scope.toServerTime = function (time) {
					return Math.floor(time / 1000);
				}

				/**
				 * Prepares the time to JS time
				 */
				$scope.toJsTime = function (time) {
					time = parseInt(time);

					return (time * 1000);
				}

				/**
				 * Sets a name for displaying on route points
				 */
				$scope.completeName = function (point) {
					switch (point.properties.waypoint_type_code_name) {
						case "CITY":
							return point.properties.name.replace(/ \- [A-Z][A-Z]$/g, "");
							break;
						case "AD":
						case "HELPN":
							return point.properties.icao;
							break;
						case "COORDS":
							return point.properties.name.replace(/[0-9][0-9] ([SNWEOL])/g, "$1").replace(/[/ ]/g, "");
							break;
						default:
							return point.properties.code;
							break;;
					}
				};

				/**
				 * Calculates route age
				 */
				$scope.calculateAge = function (route) {
					var updatedOn = angular.isNumber(route.updated_on) ? route.updated_on : Date.parse(route.updated_on);
					var diffMin = (new Date().getTime() - updatedOn) / 1000 / 60;

					//Route just created and active
					if (route.id == $scope.route.id && diffMin < 1) {
						return $scope.language.labels.now;
					}

					var age = diffMin < 1 //Less than a minute old
							? "1m"
							: diffMin < 60 //Less than an hour old
								? "" + Math.floor(diffMin) + "m"
								: diffMin < (24 * 60) //Less than a day old OR more than a day
									? "" + Math.floor(diffMin / 60) + "h"
									: "" + Math.floor(diffMin / 60 / 24) + "d";

					return age;
				};

				/**
				 * Prepares the route for recent routes
				 */
				$scope.prepareRecentRoute = function (route) {
					route.points2 = [];
					route.points.features.forEach(function (point, i) {
						if (i == 0) {
							route.title1 = $scope.completeName(point);
							route.cardTitle = $scope.completeName(point);
						} else if (i == route.points.features.length - 1) {
							route.title1 += " - " + $scope.completeName(point);
							route.cardTitle += " - " + $scope.completeName(point);
						} else {
							route.points2.push($scope.completeName(point));
						}
						$scope.generatePointTitles(point);
					});

					return route;
				};

				//Recent routes collection
				$scope.recentRoutes = [];
				//Flag for recent routes loader
				$scope.loadingRecentRoutes = false;

				/**
				 * Loads recent user routes
				 */
				$scope.loadRecentRoutes = function () {
					$scope.loadingRecentRoutes = true;

					var getRequestRecentRoutesParams = {
						user_id: Session.current.user.id,
					}

					//Queries the user routes
					NavigationRouteService.get(getRequestRecentRoutesParams, {}).$promise.then(function (response) {
						//Error found at the server, exception thrown
						if (response.error)
							return console.log($scope.language.exceptions[response.message]);

						var preparedRoutes = response.routes;
						//console.log(response);
						preparedRoutes.forEach(function (route) {
							//Working around features to manipulate the result
							route.points = route.features;
							//Correcting boolean values
							route.favourite = (route.favourite == "1");
							//Ground speed
							route.ground_speed = route.ground_speed > 0 ? route.ground_speed : null;
							//Correct time values
							route.creation_date = $scope.toJsTime(route.creation_date);
							route.updated_on = $scope.toJsTime(route.updated_on);
							//Preparing the route to be loaded
							$scope.prepareRecentRoute(route);
						});

						//Sets recent routes list
						$scope.recentRoutes = preparedRoutes;
						$scope.loadingRecentRoutes = false;
						$scope.recentRoutesMsg = "";
					});
				}

				/**
				 * Sets the route favourite at the server
				 */
				$scope.setFavourite = function (route) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					var routeToUpdate = angular.copy(route);

					//Sets the route favourire inverse state
					routeToUpdate.favourite = route.favourite = !routeToUpdate.favourite;
					//routeToUpdate.updated_on = $scope.toServerTime(new Date().getTime()); //miliseconds to seconds
					routeToUpdate.updated_on = $scope.toServerTime(routeToUpdate.updated_on); //miliseconds to seconds

					//Sets the route favourite at the server
					return NavigationRouteService.setFavourite({}, routeToUpdate).$promise.then(function (oResult) {
						if (oResult.error)
							return console.log($scope.language.exceptions[oResult.error]);

						//Sets the updated values incoming from server
						route.favourite = (oResult.route.favourite == "1");
						route.updated_on = $scope.toJsTime(oResult.route.updated_on);

						return route;
					});
				}

				/**
				 * Sets the route ground speed at the server
				 */
				$scope.updateGroundSpeed = function (gs) {
					var route, recentRoute;

					//If less than 2 digits were typed
					if (!gs || gs.length < 2 || parseFloat(gs) == parseFloat($scope.route.ground_speed)) return;

					//Updates the user activity
					$scope.setActiveUser(true);

					//Copies the route for manipulation
					route = angular.copy($scope.route);

					//Sets the updated speed and date
					route.ground_speed = gs;
					route.updated_on = $scope.toServerTime(route.updated_on); //miliseconds to seconds (server)

					//Sets the route favourite at the server
					return NavigationRouteService.updateGroundSpeed({}, route).$promise.then(function (oResult) {
						if (oResult.error)
							return console.log($scope.language.exceptions[oResult.error]);

						//Sets the updated values incoming from server
						$scope.route.ground_speed = oResult.route.ground_speed > 0 ? oResult.route.ground_speed : null;
						$scope.route.updated_on = $scope.toJsTime(oResult.route.updated_on);

						//Gets the recent route for update
						recentRoute = $scope.recentRoutes.filter(function (item, index) {
							return item.id == $scope.route.id
						})

						//If the recent route if an array
						recentRoute = angular.isArray(recentRoute) && recentRoute.length == 1 ? recentRoute[0] : recentRoute;

						//Sets the recent route ground speed
						recentRoute.ground_speed = $scope.route.ground_speed;

						$scope.completeRoutePlanning.speedInput = true;
						$scope.sendCompleteRoutePlanningEvent();

						return $scope.route;
					});
				}

				//TODO: Remove from comments
				//$scope.updateRotasRecentes = function () {
				//	$scope.loadingRecentRoutes = true;
				//	//Object to be used as a query param to server
				//	var getRequestRecentRoutesParams = {
				//		user_id: Session.current.user.id,
				//	}
				//	//Queries the user routes
				//	NavigationRouteService.get(getRequestRecentRoutesParams, {}).$promise.then(function (response) {
				//		//Error found at the server, exception thrown
				//		if (response.error)
				//			return console.log($scope.language.exceptions[response.message]);
				//		var preparedRoutes = response.routes;
				//		preparedRoutes.forEach(function (route) {
				//			//Mocking features
				//			route.points = { features: route.features };
				//			//Preparing the route to be loaded
				//			$scope.prepareRecentRoute(route);
				//		});
				//		//Sets recent routes list
				//		$scope.recentRoutes = preparedRoutes;
				//		$scope.loadingRecentRoutes = false;
				//		$scope.recentRoutesMsg = "";
				//	}, function (response) {
				//		if (response.status == -1) {
				//			$scope.recentRoutesMsg = "Não foi possível carregar suas rotas recentes. Verifique sua conexão à Internet e tente novamente. Se o problema persistir, por favor, avise-nos pelo e-mail contato@nexatlas.com.";
				//			window.setTimeout(function () {
				//				$scope.updateRotasRecentes();
				//			}, 11000);
				//		} else if (response.status == 401) {
				//			$scope.loadingRecentRoutes = false;
				//			$scope.usuarioLogado.id = null;
				//			$scope.usuarioLogado.nome = null;
				//			$scope.usuarioLogado.sobrenome = null;
				//			$scope.usuarioLogado.email = null;
				//			$scope.usuarioLogado.logado = false;
				//			$scope.exibirLogin = true;
				//		} else {
				//			$scope.recentRoutesMsg = "Não foi possível carregar suas rotas recentes. Erro: " + response.status + ". Se o problema persistir, por favor, avise-nos pelo e-mail contato@nexatlas.com.";
				//			window.setTimeout(function () {
				//				//-
				//				$scope.updateRotasRecentes();
				//			}, 11000);
				//		}
				//	});
				//};

				/**
				 * Loads the route details/waypoints from the server
				 */
				$scope.loadRoute = function (i) {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					var route = angular.copy($scope.recentRoutes[i]);
					//console.log(route);
					//route.id = "";
					delete route.title1;
					delete route.cardTitle;
					delete route.points2;
					delete route.features;
					$scope.route = route;

					// checks the window size and closes the panel if needed
					if ($scope.getScreenWidth() < 640) {
						$scope.panel = defaultPanel;
					}

					//Initially, the route is no clone at all
					$scope.route.clone = false;

					//Sets the scope ground speed
					$scope.ground_speed = $scope.route.ground_speed;

					//Gets origin/destination object references
					$scope.originId = $scope.route.points.features[0].properties.id;
					$scope.destinationId = $scope.route.points.features[$scope.route.points.features.length - 1].properties.id;

					//console.log("-- Route loading origin: ");
					//console.log($scope.originId);
					//console.log("-- Route loading destination: ");
					//console.log($scope.destinationId);

					$scope.updateRouteTotal();
					$scope.updateRotaGrafica();
					$scope.enquadrarRota();

					////Queries the route waypoints at the server
					//NavigationRouteService.getWaypoints({ route_id: $scope.route.id }).$promise.then(function (oResult) {
					//	//Error found at the server, exception thrown
					//	if (oResult.error)
					//		return console.log($scope.language.exceptions[oResult.message]);

					//	//This results are what the server actually sends
					//	$scope.route.waypoints = oResult.waypoints
					//	$scope.route.points.features = oResult.features

					//	$scope.updateRouteTotal();
					//	$scope.updateRotaGrafica();
					//	$scope.enquadrarRota();
					//});
				};

				//--------------------------------------------------------------
				// FINAL LOADINGS
				//--------------------------------------------------------------

				//Sets the current user picture
				$scope.userProfile = Session.current.userProfile;
				// $scope.userProfile.picture = undefined;

				$scope.usuarioLogado = Session.current.user;
				if ($scope.usuarioLogado)
					$scope.usuarioLogado.logado = true;


				// Ref to termsToggleModal
				$scope.getTermsModalInstance = function (modal) {
					$scope.termsToggleModal = modal.toggleModal;
				}

				//--------------------------------------------------------------

				$scope.routesPlanningSortOpts = {
					containment: '#idRoutesPlanning',
				};

				//--------------------------------------------------------------

				/**
				 * Initializable functions under scope
				 */
				$scope.init = function () {
					//Loads user recent routes
					$scope.loadRecentRoutes();
					//Sets the user active
					$scope.initUserActivity();
				}

				$scope.init();


				//---------------------------------------------------------
				// replicate description menu effect on left bar
				//---------------------------------------------------------
				$scope.descriptionMenuOpen = false;
				$scope.toggleDescriptionMenu = function () {
					//Renews the activity monitorer
					$scope.setActiveUser(true);

					if (!$scope.descriptionMenuOpen) {
						$scope.openDescriptionMenu();
					}
					else {
						$scope.closeDescriptionMenu();
					}
				}

				$scope.openDescriptionMenu = function () {
					if (!$scope.descriptionMenuOpen) {
						$("#barraEsquerda").animate(
							{ width: "256px" },
							{
								duration: 50,
								complete: function () {
									$("#barraEsquerda p")
										.css("opacity", 0)
										.show()
										.animate({ opacity: 1 }, { duration: 50 });
								}
							}
						);
						$scope.descriptionMenuOpen = true;
					}
				}

				$scope.closeDescriptionMenu = function () {
					if ($scope.descriptionMenuOpen) {
						$("#barraEsquerda p").animate(
							{ opacity: 0 },
							{
								duration: 50,
								complete: function () {
									$("#barraEsquerda p").hide();
									$("#barraEsquerda")
										.animate({ width: "48px" }, { duration: 50 });
								}
							}
						);
						$scope.descriptionMenuOpen = false;
					}
				}

				$scope.getContactFormRef = function (ref) {
					$scope.contactFormRef = ref;
				}

				$scope.shouldIUseAHandle = function () {
					// checks for specific mobile devices to enable handler
					var handleDevices = /ipad|iphone|ipod|android/;
					return handleDevices.test(navigator.userAgent.toLowerCase()) && !window.MSStream ? ".route-card-handle" : false;
				};

				// Sortable options
				$scope.sortableOptions = {
					axis: 'y',
					handle: $scope.shouldIUseAHandle(),
					start: function (e, ui) {
						console.log(ui);
						try {
							// gets the last route-info element from the draggable copy
							var el = ui.item[0].getElementsByClassName('route-info')[0]
							console.log(el);
							el.style.opacity = 0;
						} catch (e) { }
					},
					update: function (e, ui) {

					},
					stop: function (e, ui) {
						try {
							// gets the last route-info element from the draggable copy
							var el = ui.item[0].getElementsByClassName('route-info')[0]
							console.log(el);
							el.style.opacity = 1;
						} catch (e) { }
						$scope.updateRouteTotal();
						$scope.updateRotaGrafica();
						$scope.updateServerRoute();
					},
				}

				window.addEventListener('resize', function (e) {
					$scope.sortableOptions.handle = $scope.shouldIUseAHandle();
				});

				// -------------------------------------------------------------
				// ground speed input loses focus when 'RETURN' is pressed
				$scope.groundSpeedKeyPressHandler = function (ev) {
					if (ev.charCode === 13) {
						ev.currentTarget.blur();
					}
				}

				//--------------------------------------------------------------
				// Closes the layers popup
				$scope.layersToolJustClosed = false;
				$scope.closeLayersToolListener = function (event) {
					var layersTool = document.getElementById("painelDireita");
					var layersToolButton = document.getElementById("barraDireitaBotoesSup");
					if (!layersTool.contains(event.target)) {
						// if the layersToolButton was used to close the tool
						// should set a flag to avoid the click event
						if (layersToolButton.contains(event.target)) {
							$scope.layersToolJustClosed = true;
						} else {
							$scope.layersToolJustClosed = false;
						}
						$scope.tool = defaultTool
						$scope.$apply();
						$(document).off('.closeLayersTool');
					}
				};
			}
		]
	}

	return oDashboard;
})();
