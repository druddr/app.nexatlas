﻿var Authentication = (function () {

	var oAuthenticationController = {
		services: ['UserService.js'],
		components: [],
		/// Route usage:
		/// #/app/authentication/login
		login: ['$scope', '$location', 'UserService', 'Session', function ($scope, $location, UserService, Session) {
			$scope.user = {};

			//console.log("user logged:");
			//console.log(Session);

			////Event invoked
			//$scope.goToDefaultRoute = function () {
			//	$scope.navigate($scope.router.default_route);
			//}

			//Drives the user to registration
			$scope.userRegister = function () {
				$scope.navigate($scope.routes.user.register);
			}

			//Drives the user to lost password form
			$scope.userLostPassword = function () {
				$scope.navigate($scope.routes.user.lostPassword);
			};
		}]
	}

	return oAuthenticationController;
})();