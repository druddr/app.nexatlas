﻿angular.module("Axis-Dependency-Injection").load.factory("PasswordRequestService",
	['$resource', function ($resource) {
		return $resource(settings.app.services.api + 'user/', null, {
			checkPasswordRequest: {
				url: settings.app.services.api + 'user/checkPasswordRequest',
				method: 'POST',
				isArray: false
			},
			resetPassword: {
				url: settings.app.services.api + 'user/resetPassword',
				method: 'POST',
				isArray: false
			}
		});
	}]);
