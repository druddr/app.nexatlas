﻿angular.module("Axis-Dependency-Injection").load.factory("UserService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'user/', null, {
		authenticate: {
			url: settings.app.services.api + 'user/authenticate',
			method: 'POST',
			isArray: false
		},
		facebookAuthenticate: {
			url: settings.app.services.api + 'user/facebookAuthenticate',
			method: 'POST',
			isArray: false
		},
		googleAuthenticate: {
			url: settings.app.services.api + 'user/googleAuthenticate',
			method: 'POST',
			isArray: false
		},
		authorize: {
			url: settings.app.services.api + 'user/authorize',
			method: 'POST',
			isArray: false
		},
		renewActivity: {
			url: settings.app.services.api + 'user/renewActivity',
			method: 'POST',
			isArray: false
		},
		create: {
			url: settings.app.services.api + 'user/create',
			method: 'POST',
			isArray: false
		},
		confirmEmail: {
			url: settings.app.services.api + 'user/confirmEmail',
			method: 'POST',
			isArray: false
		},
		lostPasswordRequest: {
			url: settings.app.services.api + 'user/lostPasswordRequest',
			method: 'POST',
			isArray: false
		},
		checkPasswordRequest: {
			url: settings.app.services.api + 'user/checkPasswordRequest',
			method: 'POST',
			isArray: false
		},
		resetPassword: {
			url: settings.app.services.api + 'user/resetPassword',
			method: 'POST',
			isArray: false
		},
		logout: {
			url: settings.app.services.api + 'user/logout',
			method: 'POST',
			isArray: false
		},
		contact: {
			url: settings.app.services.api + 'user/contact',
			method: 'POST',
			isArray: false
		}
	});
}]);
