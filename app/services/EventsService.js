// A single service to control events request.
// the request data should be a event type:
// - simple_route_planning
// - complete_route_planning
angular.module("Axis-Dependency-Injection").load.factory("EventsService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'events/', {}, {
		generic: {
			url: settings.app.services.api + 'events/',
			method: 'POST',
			isArray: false
		}
	});
}]);
