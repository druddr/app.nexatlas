﻿angular.module("Axis-Dependency-Injection").load.factory("AuthenticationService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Authentication/', {}, {
		authenticate: {
			url: settings.app.services.api + 'Authentication/authenticate',
			method: 'POST',
			isArray: false
		},
		logout: {
			url: settings.app.services.api + 'Authentication/logout/',
			method: 'POST',
			isArray: false
		}
	});
}]);