﻿angular.module("Axis-Dependency-Injection").load.factory("NavigationRouteService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'navigationRoute/', null, {
		create: {
			url: settings.app.services.api + 'navigationRoute/create',
			method: 'POST',
			isArray: false
		},
		get: {
			url: settings.app.services.api + 'navigationRoute/get',
			method: 'GET',
			isArray: false
		},
		getWaypoints: {
			url: settings.app.services.api + 'navigationRoute/getWaypoints',
			method: 'GET',
			isArray: false
		},
		addWaypoint: {
			url: settings.app.services.api + 'navigationRoute/addWaypoint',
			method: 'POST',
			isArray: false
		},
		addWaypoints: {
			url: settings.app.services.api + 'navigationRoute/addWaypoints',
			method: 'POST',
			isArray: false
		},
		removeWaypoint: {
			url: settings.app.services.api + 'navigationRoute/removeWaypoint',
			method: 'POST',
			isArray: false
		},
		removeRoute: {
			url: settings.app.services.api + 'navigationRoute/removeRoute',
			method: 'POST',
			isArray: false
		},
		setFavourite: {
			url: settings.app.services.api + 'navigationRoute/setFavourite',
			method: 'POST',
			isArray: false
		},
		updateGroundSpeed: {
			url: settings.app.services.api + 'navigationRoute/updateGroundSpeed',
			method: 'POST',
			isArray: false
		}
	});
}]);
