﻿angular.module("Axis-Dependency-Injection").load.factory("UserProfileService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'userProfile/', null, {
		get: {
			url: settings.app.services.api + 'userProfile/get',
			method: 'get',
			isArray: true
		},
		create: {
			url: settings.app.services.api + 'userProfile/create',
			method: 'POST',
			isArray: false
		},
	});
}]);