﻿angular.module("Axis-Dependency-Injection").load.factory("WaypointService", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'waypoint/', null, {
		get: {
			url: settings.app.services.api + 'waypoint/query',
			method: 'get',
			isArray: false
		}
	});
}]);
