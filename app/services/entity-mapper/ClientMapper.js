﻿angular.module("Axis-Dependency-Injection").load.factory("ClientMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Cliente/:id', {
		id: '@id'
	});
}]);