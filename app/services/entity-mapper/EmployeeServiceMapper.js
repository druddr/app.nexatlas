﻿
angular.module("Axis-Dependency-Injection").load.factory("EmployeeServiceMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/FuncionarioServico/:id', {
		id: '@id'
	});
}]);