﻿
angular.module("Axis-Dependency-Injection").load.factory("FinancialDebtMovementMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/FinanceiroTituloMovimento/:id', {
		id: '@id'
	});
}]);