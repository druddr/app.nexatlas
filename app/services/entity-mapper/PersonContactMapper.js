﻿angular.module("Axis-Dependency-Injection").load.factory("PersonContactMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/PessoaContato/:id', {
		id: '@id'
	});
}]);