﻿
angular.module("Axis-Dependency-Injection").load.factory("ProductMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Produto/:id', {
		id: '@id'
	});
}]);