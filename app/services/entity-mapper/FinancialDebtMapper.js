﻿
angular.module("Axis-Dependency-Injection").load.factory("FinancialDebtMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/FinanceiroTitulo/:id', {
		id: '@id'
	});
}]);