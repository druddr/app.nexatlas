﻿angular.module("Axis-Dependency-Injection").load.factory("CompanyMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Empresa/:id', {
		id: '@id'
	});
}]);