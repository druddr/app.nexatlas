﻿angular.module("Axis-Dependency-Injection").load.factory("AdMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Ad/:id', {
		id: '@id'
	});
}]);