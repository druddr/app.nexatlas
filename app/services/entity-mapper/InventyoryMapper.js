﻿
angular.module("Axis-Dependency-Injection").load.factory("InventoryMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Estoque/:id', {
		id: '@id'
	});
}]);