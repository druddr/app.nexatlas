﻿
angular.module("Axis-Dependency-Injection").load.factory("OrderMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Pedido/:id', {
		id: '@id'
	});
}]);