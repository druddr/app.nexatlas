﻿angular.module("Axis-Dependency-Injection").load.factory("OrderProductViewMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/View/PedidoProduto/:id', {
		id: '@id'
	});
}]);