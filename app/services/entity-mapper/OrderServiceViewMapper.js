﻿angular.module("Axis-Dependency-Injection").load.factory("OrderServiceViewMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/View/PedidoServico/:id', {
		id: '@id'
	});
}]);