﻿angular.module("Axis-Dependency-Injection").load.factory("SubsidiaryRatingMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/EmpresaFilialAvaliacao/:id', {
		id: '@id'
	});
}]);