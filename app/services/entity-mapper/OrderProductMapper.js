﻿angular.module("Axis-Dependency-Injection").load.factory("OrderProductMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/PedidoProduto/:id', {
		id: '@id'
	});
}]);