﻿angular.module("Axis-Dependency-Injection").load.factory("SubsidiaryMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/EmpresaFilial/:id', {
		id: '@id'
	});
}]);