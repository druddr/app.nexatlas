﻿
angular.module("Axis-Dependency-Injection").load.factory("EventMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/Evento/:id', {
		id: '@id'
	});
}]);