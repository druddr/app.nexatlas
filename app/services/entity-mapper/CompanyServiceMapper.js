﻿angular.module("Axis-Dependency-Injection").load.factory("CompanyServiceMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/EmpresaServico/:id', {
		id: '@id'
	});
}]);