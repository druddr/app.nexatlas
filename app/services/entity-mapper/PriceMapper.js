﻿
angular.module("Axis-Dependency-Injection").load.factory("PriceMapper", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'EntityMapper/ProdutoPreco/:id', {
		id: '@id'
	});
}]);