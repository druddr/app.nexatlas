﻿angular.module("Axis-Dependency-Injection").load.factory("Ad", ['$resource', function ($resource) {
	return $resource(settings.app.services.api + 'Ad/', {}, {
		like: {
			url: settings.app.services.api + 'Ad/like',
			method: 'POST',
			isArray: false
		}
	});
}]);