angular.module("Axis-Dependency-Injection").load.directive("loginForm", [
	'$rootScope',
	'UserService',
	'UserProfileService',
	'Session',
	function($rootScope, UserService, UserProfileService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				//userId: '=',
				//form: '=?'
			},
			controller: [
				'$scope', function($scope) {}
			],
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;

				//Keeps the editable properties of this component
				scope.loginForm = {
					email: "",
					password: "",
					rememberMe: true
				}

				//Redirects the user to registration page
				scope.userRegister = function() {
					if (!angular.isFunction(scope.$parent.userRegister))
						throw new DOMException("This component requires a scope.$parent.userRegister function which redirects to user registration.");

					scope.$parent.userRegister();
				}

				//Redirects the user to registration page
				scope.userLostPassword = function() {
					if (!angular.isFunction(scope.$parent.userLostPassword))
						throw new DOMException("This component requires a scope.$parent.userLostPassword function which redirects to user registration.");

					scope.$parent.userLostPassword();
				}

				//Submits the form for authentication
				scope.loginFormSubmit = function() {
					UserService.authenticate({}, scope.loginForm).$promise.then(function(uAuth) {
						//In case of error
						if (uAuth.error) {
							switch (uAuth.message) {
								case 'dataNotAnObject':
									return console.log(uAuth.message);
									break;
								case 'invalidUserOrPassword':
									return alert(scope.language.exceptions[uAuth.message]);
									break;
								case 'userAccountInactive':
								case 'userProfileIncomplete':
									////User has to finish profile information filling up
									//alert(scope.language.exceptions[uAuth.message]);
									return $rootScope.navigate($rootScope.routes.user.profile);
									break;
								default:
									return console.log(uAuth);
									break;
							}
						}
						//Logs the user setting the session object
						scope.setLoggedUser(uAuth);
					})
				}

				//Sets the logged user info
				scope.setLoggedUser = function(oAuthUser) {
					//If somehow the process failed completely
					if (!oAuthUser || oAuthUser.error)
						return console.log(scope.language.messages.loginProcessFailed);

					//Logs the user setting the session object
					//Session.rememberUser(oAuthUser.remember);
					//Session.setCurrent.user(oAuthUser.user);
					//Session.setCurrent.userProfile(oAuthUser.userProfile);
					//Session.token(oAuthUser.token);
					Session.define(oAuthUser, function (session) {
						if (!session.current.user.active || !session.current.userProfile.complete || !session.current.token) {
							return $rootScope.navigate($rootScope.routes.user.profile)
						}
						return $rootScope.navigateToDefault()
					})

					//Keeps it in the scope for reusage
					scope.ngModel = oAuthUser;
					//Redirects the user to the default route
					//scope.$parent.navigateToDefault();
				}

				//scope.init = function() {
				//	//Checking Facebook and Google login state
				//	scope.facebookCheckLoginState();
				//	scope.googleCheckLoginState();
				//}
			},
			templateUrl: settings.path.components + 'login/form/template.html'
		}
	}
])
