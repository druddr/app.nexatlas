angular.module("Axis-Dependency-Injection").components.config('login-form', {
	components: [
		'oauth/facebook',
		'oauth/google'
	],
	directives: ['directive'],
	services: ['UserService', 'UserProfileService']
});
