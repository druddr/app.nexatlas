angular.module("Axis-Dependency-Injection").load.directive("userProfileForm", [
	'$rootScope',
	'UserService',
	'UserProfileService',
	'Session',
	function($rootScope, UserService, UserProfileService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				userId: '=',
				form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				scope.errors = [];

				var occupationChoicesIDPrefix = '__occupation_choice';
				var occupationChoicesFieldsIDPrefix = '__occupation_choice_field';

				var occupationChoices = [
					{
						name: "Sou piloto habilitado",
						id: 'OCPC001',
						value: true
					}, {
						name: "Não sou piloto habilitado",
						id: 'OCPC002',
						value: false
					}
				];
				var occupationChoicesFields = {
					OCPC001: [
						{
							id: 'OCPC001F001',
							//name: 'mostUsedAircraft',
							name: 'aircraft_type',
							label: 'Qual aeronave você opera com mais frequência?',
							options: [
								{
									value: "",
									category: true,
									label: "Asa Fixa"
								}, {
									value: "Asa fixa - com 1 motor a pistão",
									label: "com 1 motor a pistão"
								}, {
									value: "Asa fixa - com 2 motores a pistão",
									label: "com 2 motores a pistão"
								}, {
									value: "Asa fixa - com 1 motor turbo-hélice",
									label: "com 1 motor turbo-hélice"
								}, {
									value: "Asa fixa - com 2 motores turbo-hélice",
									label: "com 2 motores turbo-hélice"
								}, {
									value: "Asa fixa - com 2 motores turbo-jato/fan",
									label: "com 2 motores turbo-jato/fan"
								}, {
									value: "Asa fixa - com 3 motores turbo-jato/fan",
									label: "com 3 motores turbo-jato/fan"
								}, {
									value: "Asa fixa - planador",
									label: "planador"
								}, {
									value: "Asa fixa - outros",
									label: "outros"
								}, {
									value: "",
									category: true,
									label: "Asa rotativa"
								}, {
									value: "Asa rotativa - com 1 motor a pistão",
									label: "com 1 motor a pistão"
								}, {
									value: "Asa rotativa - com 1 motor turbo-hélice",
									label: "com 1 motor turbo-hélice"
								}, {
									value: "Asa rotativa - com 2 motores turbo-hélice",
									label: "com 2 motores turbo-hélice"
								}, {
									value: "Asa rotativa - outros",
									label: "outros"
								}
							]
						}, {
							id: 'OCPC001F002',
							//name: 'aviationOccupationArea',
							name: 'aviation_segment',
							label: 'Você atua em qual segmento da aviação?',
							options: [
								{
									value: "Particular",
									label: "Particular"
								}, {
									value: "Táxi aéreo",
									label: "Táxi aéreo"
								}, {
									value: "Instrução de voo (aluno)",
									label: "Instrução de voo (aluno)"
								}, {
									value: "Instrução de voo (instrutor)",
									label: "Instrução de voo (instrutor)"
								}, {
									value: "Linha aérea",
									label: "Linha aérea"
								}, {
									value: "Agrícola",
									label: "Agrícola"
								}, {
									value: "Militar",
									label: "Militar"
								}, {
									value: "Outros",
									label: "Outros"
								}
							]
						}
					],
					OCPC002: [
						{
							id: "OCPC002F001",
							//name: 'selfClassification',
							name: 'self_classification',
							label: 'Como você se classifica?',
							options: [
								{
									value: "Piloto aluno em habilitação",
									label: "Piloto aluno em habilitação"
								}, {
									value: "Piloto de simulador de voo",
									label: "Piloto de simulador de voo"
								}, {
									value: "Operador de sala AIS",
									label: "Operador de sala AIS"
								}, {
									value: "Controlador de tráfego aéreo",
									label: "Controlador de tráfego aéreo"
								}, {
									value: "Despachante operacional de voo",
									label: "Despachante operacional de voo"
								}, {
									value: "Coordenador de voo",
									label: "Coordenador de voo"
								}, {
									value: "Entusiasta",
									label: "Entusiasta"
								}, {
									value: "Outros",
									label: "Outros"
								}
							]
						}
					]
				};

				//Maps the language strings based on the context
				scope.strings = scope.language.profile;
				//scope.routes = routes;

				var occupationChoices = occupationChoices.map(function(occupation, index) {
					return {id: occupation.id, name: occupation.name}
				});
				scope.occupationChoices = occupationChoices;

				var occupationChoicesFieldsRef = occupationChoices.reduce(function(choicesFields, choice) {
					choicesFields[choice.id] = occupationChoicesFields[choice.id]
					return choicesFields;
				}, {});

				scope.occupationChoicesFields = occupationChoicesFieldsRef[occupationChoices[0].id];
				scope.profileForm = {
					remember: Session.current.remember,
					user_id: Session.current.user.id,
					pilotOption: occupationChoices[0].id,
					pilot: true,
					readTermsOnRegister: (Session.current.user.id && Session.current.user.read_terms == "1") || false,
					read_terms: false
				}

				//Sets the readTerms property correctly
				scope.profileForm.read_terms = scope.profileForm.readTermsOnRegister;

				scope.occupationChange = function(id) {
					scope.occupationChoicesFields = occupationChoicesFieldsRef[id]
				}.bind(this);

				//Processes the profile info
				scope.profileFormSubmit = function() {

					//if (!scope.profileForm.picture) {
					//	return alert(scope.language.messages.profilePhotoIsCompulsory);
					//}

					//Generates the correct output for pilot option
					scope.profileForm.pilot = scope.profileForm.pilotOption === "OCPC001"
					scope.errors = [];
					if (!scope.profileForm.read_terms) {
						scope.errors.push(scope.language.profile.errorTermsPolicy);
						return;
					}

					// Sends the data for storage at the profile info
					UserProfileService.create({}, scope.profileForm).$promise.then(function(oProfile) {
						//In case of error
						if (oProfile.error) {
							switch (oProfile.message) {
								case 'dataNotAnObject':
									return console.log(scope.language.exceptions[oProfile.message]);
									break;
									//case 'user-profile-already-exists':
									//	return alert(scope.language.messages.emailAlreadyUsed);
									//	break;
								default:
									return console.log(oProfile.message);
									break;
							}
						}
						scope.ngModel = oProfile.userProfile;
						//Remember this was set one screen before (if the user should be remembered)
						//Session.rememberUser(oUser.remember)
						//Sets the current user on the user session object
						//It means it is already authenticated
						Session.define(oProfile, function(session) {
							$rootScope.navigateToDefault()
						})

						//Drives the user to the profile creation
						//scope.$parent.navigate(scope.$parent.router.default_route);
						//$rootScope.navigate($rootScope.router.default_route);
					});
				}
				scope.naSelectTest = "";

				//Initializes any possible user info to the template
				scope.init = function() {
					if (!Session.current.userProfile.id)
						return false;

					//Information already provided by the OAuth process
					scope.profileForm.name = Session.current.userProfile.name;
					scope.profileForm.surname = Session.current.userProfile.surname;
					scope.profileForm.picture = Session.current.userProfile.picture;
				}

				scope.init();

			},
			templateUrl: settings.path.components + 'user/profile/form/template.html'
		}
	}
])
