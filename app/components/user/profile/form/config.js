﻿angular.module("Axis-Dependency-Injection").components.config('user-profile-form', {
	directives: [
		'imageOnLoad',
		'readFileInput',
		'directive'
	],
	services: ['UserService', 'UserProfileService']
});
