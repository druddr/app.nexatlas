angular.module("Axis-Dependency-Injection").load.directive("userRedefinePasswordForm", [
	'$rootScope',
	'PasswordRequestService',
	function ($rootScope, PasswordRequestService) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				passwordRequestToken: '=',
				// userId: '=',
				// form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				//  TODO: Validate Token
				// this.$onInit = function() {
				// 	console.log('validate redefine password token');
				// 	scope.validToken = true;
				// }.bind(this);

				scope.validToken = true;
				scope.formSubmitting = false;
				scope.redefinePasswordForm = {
					token: scope.passwordRequestToken,
					password: "",
					passwordConfirmation: ""
				};

				scope.redefinePasswordFormSubmit = function() {
					var form = scope.redefinePasswordForm;
					if (form.password === form.passwordConfirmation) {
						PasswordRequestService.resetPassword({}, form).$promise.then(function (oResult) {
							if (oResult.error) {
								switch (oResult.message) {
									case "passwordConfirmationMismatch":
										return alert(scope.language.exceptions.passwordConfirmationMismatch);
										break;
									case "invalidPasswordRequest":
										console.log(scope.language.exceptions.invalidPasswordRequest);
										$rootScope.navigate($rootScope.router.authentication);
										break;
									default:
										console.log(oResult)
										return alert(scope.language.exceptions.generalError);
										break;
								}
							}

							window.location.hash = "#" + settings.router.authentication;
						}).catch(function(error) {
							scope.formSubmitting = true;
						});
					} else {
						// TODO: raise Toast or error message
					}
				}.bind(this);
			},
			templateUrl: settings.path.components + 'user/redefine-password/form/template.html'
		}
	}
])
