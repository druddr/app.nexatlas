angular.module("Axis-Dependency-Injection").load.directive("userLostPasswordForm", [
	'$rootScope', 'UserService',
	function($rootScope, UserService) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				// userId: '=',
				// form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;
				scope.lostPasswordForm = {
					email: ''
				};
				scope.lostPasswordFormSubmit = function() {
					UserService.lostPasswordRequest({}, scope.lostPasswordForm).$promise.then(function (oResult) {
						if (oResult.error) {
							switch (oResult.message) {
								case "invalidUser":
									//The process should stop here
									alert(scope.language.exceptions.invalidUser);
									break;
								default:
									break;
							}
							scope.formSubmitting = false;
							return false;
						}

						scope.formSubmitting = false;
						scope.formSubmitted = true;
					}).catch(function(error){
					});
					scope.formSubmitting = true;
				};

				scope.formSubmitting = false;
				scope.formSubmitted = false;
			},
			templateUrl: settings.path.components + 'user/lost-password/form/template.html'
		}
	}
])
