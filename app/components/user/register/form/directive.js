angular.module("Axis-Dependency-Injection").load.directive("userRegisterForm", [
	'$rootScope',
	'UserService',
	'Session',
	function($rootScope, UserService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '=',
				// userId: '=',
				// form: '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				//Sets the scope language
				scope.language = $rootScope.language.instance;

				scope.errors = [];

				// Define Modal

				//Keeps the properties introduced through the component
				scope.registerForm = {
					email: "",
					emailConfirmation: "",
					password: "",
					readTerms: false
				}

				//Submits the registration form for creation
				scope.registerFormSubmit = function () {
					//Clears up errors array
					scope.errors = [];
					var emailConfirmationIsValid = scope.registerForm.emailConfirmation === scope.registerForm.email;
					if (emailConfirmationIsValid) {
						if (scope.registerForm.readTerms) {
							UserService.create({}, scope.registerForm).$promise.then(function(oUser) {
								//In case of error
								if (oUser.error) {
									switch (oUser.message) {
										case 'dataNotAnObject':
											return console.log(ouser.message);
											break;
										case 'emailAlreadyUsed':
											return alert(scope.language.messages.emailAlreadyUsed);
											break;
										default:
											return console.log(oUser.error);
											break;
									}
								}
								//Here, we have got no errors and a successful process
								scope.ngModel = oUser;
								//Sets the current user on the user session object
								//Session.rememberUser(oUser.remember);
								//Session.setCurrent.user(oUser.user);
								//Session.setCurrent.userProfile(oUser.userProfile);
								Session.define(oUser, function (session) {
									//Drives the user to the profile creation
									scope.$parent.navigate(scope.$parent.routes.user.profile);
								})

								//TODO: Make this mode of error management work
								//},
								//function (error) {
								//	//In case of error
								//	switch (error) {
								//		case 'data-not-an-object':
								//			return console.log("data-not-an-object");
								//			break;
								//		case 'email-already-used':
								//			return alert(scope.language.message.emailAlreadyUsed);
								//			break;
								//		default: return console.log(error); break;
								//	}
							});
						} else {
							// raise Terms / Policy needed error
							scope.errors = [];
							scope.errors.push(scope.language.register.errorTermsPolicy);
						}
					} else {
						// raise Terms / Policy needed error
						scope.errors = [];
						scope.errors.push(scope.language.register.errorEmailConfirmation);
					}
				}
			},
			templateUrl: settings.path.components + 'user/register/form/template.html'
		}
	}
])
