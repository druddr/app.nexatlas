﻿angular.module("Axis-Dependency-Injection").components.config('user-register-form', {
	components: [
		'oauth/facebook',
		'oauth/google'
	],
	directives: ['directive'],
	services: ['UserService']
});
