/*
NA Contact Form
A simple contact form
*/
angular.module("Axis-Dependency-Injection").load.directive("naContactForm", [
	'$rootScope',
	'UserService',
	'Session',
	function($rootScope, UserService, Session) {
		var naContactForm = {
			replace: true,
			restrict: 'E',
			scope: {
				ref: '=',
			},
			link: function(scope, elem, attr, ctrl) {
				var user = Session.current.user;
				var userProfile = Session.current.userProfile;

				scope.language = $rootScope.language.instance;

				scope.contactForm = {
					name: userProfile.name + " " + userProfile.surname,
					email: user.email,
					phone: "",
				};
				scope.responseMessage = null;
				scope.sendFormResultOK = false;
				scope.sendingForm = false;

				scope.sendContactMessage = function() {
					scope.sendingForm = true;

					var contactForm = Object.assign({}, scope.contactForm);

					contactForm.name = userProfile.name + " " + userProfile.surname;
					contactForm.email = user.email;
					contactForm.timestamp = (new Date()).toISOString();

					UserService.contact({}, contactForm).$promise.then(function (response) {
						scope.sendingForm = false;
						//In case of error
						if (response.error) {
							switch (response.message) {
								case 'appSendsEmailDisabled':
									console.log('Mail sent with SandBox mode ON');
									console.log(scope.language.exceptions[response.message]);
								default:
									scope.sendFormResultOK = false;
									return console.log(scope.language.exceptions[response.message]);
									break;
							}
						}

						scope.sendFormResultOK = true;
						scope.contactForm.message = "";
						delete scope.contactForm.motive;

					}, function(response) {
						scope.sendingForm = false;
						scope.sendFormResult = scope.language.contact.form.errorSend;
					});
				};

				var motives = scope.language.contact.motives;

				// List all contact motive options
				scope.contactMotiveOptions = motives.map(function(motive, index) {
					return {value: motive, label: motive};
				});

				scope.clearMessage = function(){
					scope.sendFormResultOK = false;
				}

				if (scope.ref) {
					scope.ref({clearMessage: scope.clearMessage, });
				}
			},
			templateUrl: settings.path.components + 'na/contact-form/template.html'
		}

		return naContactForm;
	}
])
