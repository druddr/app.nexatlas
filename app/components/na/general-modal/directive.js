/*
General Purpose modal
A simple material dialog that uses some nex-styles

Three components should be defined as children ( or inside the tag ):
	* generalModalTitle
	* generalModalContent
	* generalModalActions (better to use buttons)

A 'type' attr is defined to change the whole modal style
Available types:
	* general
	* terms

A 'ref' attr is defined so the parent can receive all public methods from the modal.
The ref method should receive only one param, the "modal instance".
The ref method is called as soon as the modal is rendered.

== For example:
//template
<div ng-controller="myController">
	<na-general-modal ref="getModal">
		<general-modal-title>Altos titles</general-modal-title>
		<general-modal-content>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
		</general-modal-content>
		<general-modal-actions><button type="button" ng-click="$parent.toggleModal()">CLOSE</button></general-modal-actions>
	</na-general-modal>
</div>

// controller
app.controller("myController", function($scope){
	$scope.getModalRef = function(modalRef){
		$scope.myModal = modalRef;
		$scope.myModal.toggleModa();
		$scope.myModal.closeModal();
		$scope.myModal.openModal();
	}
})
==

*/
function switchModalType(type) {
	switch (type) {
		case "terms":
			return 'nex-terms-modal';
		case "general":
		default:
			return 'nex-general-modal';
	}
}

angular.module("Axis-Dependency-Injection").load.directive("naGeneralModal", [
	'$rootScope',
	function($rootScope) {
		var naSelect = {
			replace: true,
			transclude: {
				'title': '?generalModalTitle',
				'content': '?generalModalContent',
				'actions': '?generalModalActions'
			},
			restrict: 'E',
			scope: {
				ref: '=',
				modalType: '=',
				'toggleCallback': '=?'
			},
			link: function(scope, elem, attr, ctrl) {
				scope.show = false;

				scope.typeClass = switchModalType(attr.modalType);

				scope.termsCloseButton = (attr.modalType === 'terms');

				function callback(showState) {
					if (scope.toggleCallback) {
						scope.toggleCallback(showState);
					}
				}

				scope.toggleModal = function() {
					scope.show = !scope.show;
					callback(scope.show);
				}

				scope.openModal = function() {
					scope.show = true;
					callback(scope.show);
				}

				scope.closeModal = function() {
					scope.show = false;
					callback(scope.show);
				}

				if (scope.ref) {
					scope.ref({toggleModal: scope.toggleModal, openModal: scope.openModal, closeModal: scope.closeModal});
				}
			},
			templateUrl: settings.path.components + 'na/general-modal/template.html'
		}

		return naSelect;
	}
])
