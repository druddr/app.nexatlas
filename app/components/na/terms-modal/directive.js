/*
Terms modal

A simple terms modal
Renders two links with one single modal to access terms text

*/
angular.module("Axis-Dependency-Injection").load.directive("naTermsModal", [
	'$rootScope',
	function ($rootScope) {
		var naSelect = {
			replace: true,
			restrict: 'E',
			scope: {
				'linkText': '@',
				'linkClass': '@',
				'previousText': '@',
				'toggleCallback': '=?',
			},
			link: function (scope, elem, attr, ctrl) {
				scope.language = $rootScope.language.instance;

				scope.linkText = attr.linkText || "";
				scope.linkClass = attr.linkClass || "";
				scope.previousText = attr.previousText || "";

				scope.getTermsModalInstance = function (modal) {
					scope.toggleModal = modal.toggleModal;
				}
			},
			templateUrl: settings.path.components + 'na/terms-modal/template.html'
		}

		return naSelect;
	}
])
