/*
How should it work:
Get an array with option
Each options follows this pattern:
{
  id: "",
  text: "",
  checked: true | false (default is false)
}
*/
angular.module("Axis-Dependency-Injection").load.directive("naSelect", [
	'$rootScope',
	function($rootScope) {
		//return {
		var naSelect = {
			replace: true,
			restrict: 'E',
			scope: {
				optionsObject: '=',
				optionsKey: "=",
				optionsText: "=",
				ngModel: '=?',
				ngRequired: '=',
				blank: '=',
				placeholder: '=',
			},
			link: function(scope, elem, attr, ctrl) {
				scope.showOptions = false;
				scope.openUp = false;
				scope.placeholder = attr.placeholder
				//naSelect.scope.elem = elem;

				var readOnlyInput = elem[0].getElementsByClassName("readonly-input")[0];
				readOnlyInput.onkeydown = handleReadOnlyInputOnkeypress;
				readOnlyInput.onclick = handleReadOnlyInputOnClick;
				readOnlyInput.addEventListener('click', handleReadOnlyInputOnClick);

				scope.options = scope.optionsObject.map(function(value, index) {
					var obj = {};
					if (attr.optionsKey && attr.optionsText) {
						obj = {
							id: value[attr.optionsKey],
							text: value[attr.optionsText]
						};
					} else {
						obj = {
							id: value,
							text: value
						};
					}

					if (value.category) {
						obj['category'] = true;
					}

					return obj;
				});

				scope.show = function() {
					var offset = getScreenHeight() - (elem.height() + elem.offset().top + 250);
					if (offset < 0) {
						scope.openUp = true;
					}
					scope.showOptions = true
					scope.$apply();
				};

				scope.hide = function(e) {
					setTimeout(function() {
						scope.showOptions = false;
						scope.openUp = false;
						scope.$apply();
					}, 350) //Changed so after 350ms scope props are changed and applied to the DOM
				};

				function handleReadOnlyInputOnClick(e) {
					e.preventDefault();
					scope.show();
				}

				function handleReadOnlyInputOnkeypress(e) {
					e.preventDefault();
					if (e.keyCode === 32) {
						scope.show()
					}
				}

				//TODO: Moved to the controller (not a link function)
				scope.onSelectItem = function(optionId, e) {
					if (optionId) {
						scope.ngModel = optionId;
						scope.hide(e)
					}
				};
			},
			templateUrl: settings.path.components + 'na/select/template.html'
		}

		return naSelect;
	}
])

function getScreenHeight() {
	var g = document.getElementsByTagName('body')[0];
	return window.innerHeight || document.documentElement.clientHeight || g.clientHeight;
}
