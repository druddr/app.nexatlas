angular.module("Axis-Dependency-Injection").load.directive("naSvg", [
	'$rootScope',
	function($rootScope) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				name: '='
			},
			link: function(scope, elem, attr, ctrl) {},
			templateUrl: function(elem, attr) {
				return 'assets/img/' + attr.name + '.svg'
			}
		}

	}
])
