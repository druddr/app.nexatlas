angular.module("Axis-Dependency-Injection").load.directive("oauthGoogle", [
	'$rootScope',
	'$timeout',
	'UserService',
	'Session',
	function ($rootScope, $timeout, UserService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '='
			},
			link: function (scope, elem, attr, ctrl) {
				//Sets the scope language
				scope.language = $rootScope.language.instance;

				scope.errors = [];

				//Keeps the properties introduced through the component
				scope.registerForm = {
					email: "",
					emailConfirmation: "",
					password: "",
					readTerms: false
				}

				////Validates the availability of the method in its parent
				//if (!angular.isFunction(scope.$parent.setLoggedUser))
				//	throw new DOMException(scope.language.messages.googleLoginRequiresSessionSetting);

				// loads google auth
				gapi.load('auth2', function () {
					auth2 = gapi.auth2.init({
						client_id: '851398788852-3ad2bopb122t7km3mn3ohqlend13a7o8.apps.googleusercontent.com', cookiepolicy: 'single_host_origin',
						//scope: 'additional_scope'
					});
					//Attaches the behaviour to the button
					auth2.attachClickHandler(document.getElementById('googleLoginButtonID'), {}, scope.googleLoginSuccess, scope.googleLoginError);

					// var currentUser = auth2.currentUser.get();
					// if (currentUser.isSignedIn()) {
					// 	// redirects and updates local storage
					// }
				});

				//Successful login with google
				scope.googleLoginSuccess = function (user) {
					var profile = user.getBasicProfile();
					var fullName = profile.getName().split(' ');

					scope.registerForm = {};
					scope.registerForm.email = profile.getEmail();
					scope.registerForm.name = fullName.slice(0, 1).join(' ');
					scope.registerForm.surname = fullName.slice(1).join(' ');
					scope.registerForm.google_id = true;
					scope.registerForm.google = true;

					//console.log(scope.registerForm)

					getBase64Image(profile.getImageUrl(), function (image) {
						//Expects for the picture response
						scope.registerForm.picture = image;

						UserService.googleAuthenticate({}, scope.registerForm).$promise.then(function (oAuth) {
							// check oAuth response
							if (oAuth.error) {
								switch (oAuth.message) {
									default:
										return alert(oAuth.message);
										break;
								}
							}

							//Here, we have got no errors and a successful process
							scope.ngModel = oAuth;
							//Sets the current user on the user session object
							console.log(oAuth);
							Session.define(oAuth, function (session) {
								console.log(session.current);
								if (!session.current.user.active || !session.current.userProfile.complete || !session.current.token) {
									return $rootScope.navigate($rootScope.routes.user.profile);
								}
								return $rootScope.navigateToDefault()
							})

							//Drives the user to the dashboard
							//$rootScope.navigate($rootScope.routes.user.profile);
						}).catch(function (error) {
							console.error("API Request: FAILED");
							console.error(error);
						});
					});
				}

				//Error trying to login with google
				scope.googleLoginError = function (error) {
					console.error("Google Auth: FAILED");
					console.error(JSON.stringify(error, undefined, 2));
				}
				// End: Google Login

				////Initializes the scope of this directive
				//scope.init = function () {
				//	//Checking Google login state
				//	scope.googleCheckLoginState();
				//}
				//
				////Executes the initialiser
				//scope.init();
			},
			templateUrl: settings.path.components + 'oauth/google/template.html'
		}
	}
])
