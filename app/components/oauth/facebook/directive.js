angular.module("Axis-Dependency-Injection").load.directive("oauthFacebook", [
	'$rootScope',
	'$timeout',
	'UserService',
	'Session',
	function ($rootScope, $timeout, UserService, Session) {
		return {
			replace: true,
			restrict: 'E',
			scope: {
				ngModel: '='
			},
			link: function (scope, elem, attr, ctrl) {
				//Sets the scope language
				scope.language = $rootScope.language.instance;

				scope.errors = [];

				//Keeps the properties introduced through the component
				scope.registerForm = {
					email: "",
					emailConfirmation: "",
					password: "",
					readTerms: false
				}

				////Validates the availability of the method in its parent
				//if (!angular.isFunction(scope.$parent.setLoggedUser))
				//	throw new DOMException(scope.language.messages.facebookLoginRequiresSessionSetting);

				//Start: Facebook login
				//This is called with the results from from FB.getLoginStatus().
				scope.facebookStatusChange = function (response) {
					console.log('facebookStatusChange');
					console.log(response);
					// The response object is returned with a status field that lets the
					// app know the current login status of the person.
					if (response.status === 'connected') {
						//Already connected
						console.log('Facebook already logged in.');
						//Facebook connnected, tries to gain access to the user account details
						//scope.facebookApiCall();
					} else {
						//Not yet logged into facebook/ or even not authorized
						//The person is not logged into your app or we are unable to tell.
						//scope.facebookLoginExecute();
					}
				}

				//Executes the login and forward processes
				scope.facebookLoginExecute = function () {
					console.log('facebookLoginExecute');
					//Needs to login
					FB.login(function (loginResponse) {
						if (loginResponse.status === 'connected') {
							//Calls the facebook API
							scope.facebookApiCall();
						} else {
							//Failed over all login process through facebook
							console.log(scope.language.messages.facebookLoginFailed);
							return false;
						}
					}, { scope: "public_profile,email" });
				}

				//Executes the API function call on facebook
				scope.facebookApiCall = function () {
					//Here the info the user allowed is provided
					FB.api('/me', {
						fields: 'email,first_name,last_name,picture.height(320).width(320)',
						redirect: false
					}, function (apiResponse) {
						//Creates the user registration object
						scope.registerForm = {};
						scope.registerForm.email = apiResponse.email;
						scope.registerForm.name = apiResponse.first_name;
						scope.registerForm.surname = apiResponse.last_name;
						scope.registerForm.facebook_id = apiResponse.id;
						scope.registerForm.facebook = true;

						//If a profile picture was provided
						if (apiResponse.picture)
							//This function is defined inside the app.js file
							getBase64Image(apiResponse.picture.data.url, function (image) {
								//After the image is downloaded, then, it is possible to record all user data
								scope.registerForm.picture = image;
								//console.log(scope.registerForm);

								//Register information for post-usage
								UserService.facebookAuthenticate({}, scope.registerForm).$promise.then(function (oAuth) {
									if (oAuth.error) {
										switch (oAuth.message) {
											//case 'userAccountInactive':
											//case 'userProfileIncomplete':
											//	//User has to finish profile information filling up
											//	alert(scope.language.exceptions[oAuth.message]);
											//	return $rootScope.navigate($rootScope.routes.user.profile);
											//	break;
											default:
												return alert(oAuth.message);
												break;
										}
									}

									//Here, we have got no errors and a successful process
									scope.ngModel = oAuth;
									//Sets the current user on the user session object
									Session.define(oAuth, function (session) {
										if (!session.current.user.active || !session.current.userProfile.complete || !session.current.token) {
											return $rootScope.navigate($rootScope.routes.user.profile);
										}
										return $rootScope.navigateToDefault()
									})

									//Drives the user to the dashboard
									//$rootScope.navigate($rootScope.routes.user.profile);
								});
							});
					});
				}

				//Checking the login state with facebook API
				scope.facebookCheckLoginState = function () {
					console.log('Facebook check login state triggered');

					FB.getLoginStatus(function (response) {
						//Facebook login status changed
						scope.facebookStatusChange(response);
					});
				}
				//End: Facebook login

				//Initializes the scope of this directive
				scope.init = function () {

					//Checking Facebook login state
					scope.facebookCheckLoginState();
				}

				//Executes the initialiser after 500ms
				$timeout(function () { scope.init() }, 500)
			},
			templateUrl: settings.path.components + 'oauth/facebook/template.html'
		}
	}
])
