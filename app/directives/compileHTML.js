// ref: http://stackoverflow.com/questions/26386470/generate-html-string-from-angularjs-template-string
angular.module('System').directive('ngHtmlCompile', ["$compile", function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            // watches for changes
            scope.$watch(attrs.ngHtmlCompile, function (newValue, oldValue) {
                // sets the HTML
                element.html(newValue);
                // render
                $compile(element.contents())(scope);
            });
        }
    }
}]);
