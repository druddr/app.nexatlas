﻿
angular.module("Sugar").directive("appBarTabs", ["$rootScope", function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			tabs: '=?'
		},
		link: function (scope, element, attrs) {
			scope.$watch('tabs.visible', function () {
				//console.log('visible changed',scope.tabs.visible);
			});

			var $activeBar = element.find(".active-bottom-bar"),
				$activeTab,
				$activeContent;

			function activate(tabId) {
				element.find(".tab").removeClass("active");
				$activeTab = element.find("[data-tab-id=" + tabId + "]");
				if (!$activeTab.length) return false;
				$activeTab.addClass("active");
				$activeBar.css("width", $activeTab.outerWidth());

				var width = $activeTab.outerWidth();
				var left = $activeTab.position().left;
				var scroll = (left + width) - window.innerWidth > 0 ? (left + width + 50) - window.innerWidth : 0;
				//element.scrollLeft(scroll);
				element.animate({ scrollLeft: scroll }, { queue: false, duration: 400 });
				Sugar.transform.translate($activeBar, left);

				$activeContent = $("[data-tab-content-id=" + tabId + "]");
				$activeContent.addClass("active").fadeIn();

				document.body.scrollTop = 0;

				//Sugar.transform.translate($activeContent, 0);
				$("[data-tab-content-id]").not($activeContent).hide().removeClass("active");

				//Added after a problem with gmaps javaScript API
				$rootScope.$broadcast('sugar:activate-tab', tabId);

				return true;
			}

			scope.activate = function (sTabId) {
				activate(sTabId);
			}
			scope.widthStyle = '';
			var activated = false;
			scope.$watch('tabs.list.length', function (a, b) {
				//if (!scope.tabs || !scope.tabs.list || scope.tabs.list.length == 0) return;

				if (scope.tabs && scope.tabs.centered) {
					scope.widthStyle = 'width:' + ((100 / scope.tabs.list.length).toFixed(2)) + '%;';
				}

				activated = false;
				for (var i = 0; i < scope.tabs.list.length; i++) {
					if (scope.tabs.list[i].active) {
						activated = activate(scope.tabs.list[i].contentId);
					}
				}
				setTimeout(function () {
					if (!activated && scope.tabs.list.length) {
						activate(scope.tabs.list[0].contentId);
					}
				}, 0);
			});
			Sugar.touch.fnQueue.swipeLeft.push(function () {
				var $tab = element.find(".tab.active").next(".tab");
				if ($tab.length == 0) return;
				activate($tab.attr("data-tab-id"));
			});
			Sugar.touch.fnQueue.swipeRight.push(function () {
				var $tab = element.find(".tab.active").prev(".tab");
				if ($tab.length == 0) return;
				activate($tab.attr("data-tab-id"));
			});
		},
		template: '' +
		'<div class="tab-scroll">' +
		'	<ul class="tabs" ng-class="{ \'visible\': tabs.visible && tabs.list.length>0, \'centered\': tabs.centered }" >' +
		'		<li ng-repeat="tab in tabs.list" class="tab" data-tab-id="{{tab.contentId}}" tabindex="0" ng-click="activate(tab.contentId)" style="{{widthStyle}}">' +
		'			<i class="icon material-icons" ng-if="tab.icon">{{tab.icon}}</i>' +
		'			<span>{{tab.label}}</span>' +
		'		</li>' +
		'		<li class="active-bottom-bar"></li>' +
		'	</ul>' +
		'</div>'
	}
}])