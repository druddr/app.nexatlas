﻿angular.module("Sugar", ["ui.utils.masks"]);
angular.module("Sugar").directive('longPress', function ($timeout) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs, ctrl) {
			scope.timeout = null;
			element.bind('mousedown', function (e) {
				clearTimeout(scope.timeout);
				scope.timeout = setTimeout(function () {
					scope.$apply(function () {
						scope.$eval(attrs.longPress);
					})
				}, 500);
			})
			element.bind('mouseup mouseleave', function (e) {
				clearTimeout(scope.timeout);
				scope.timeout = null;
			})
		}
	}
});