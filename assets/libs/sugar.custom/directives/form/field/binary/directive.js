﻿angular.module("Sugar").directive("formFieldBinary",['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			model: '=',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			type: '@?',
			name: '@?'
		},
		controller: ['$scope', function ($scope) {
		}],
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.label = '';
			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}
			scope.$watch('model', function () {
				if (typeof scope.model != 'boolean') {
					scope.model = scope.model==1;
				}
			});
			scope.type = scope.type ? scope.type : 'multi-select';

			element.find(".checkbox").on("click", function (e) {
				e.stopPropagation();
			})
			element.on('click keyup', '.form-field-binary', function (e) {
				e.preventDefault();
				if (e.type == 'keyup') {
					var keycode = e.keyCode || e.which || e.charCode;
					if (keycode != 32 && keycode != 13) return;
				}
				var self = angular.element(this);
				self.focus();
				self.find('.checkbox:first').trigger('click');
			});

		},
		template:''+
		'<div class="form-field-group form-field-binary">'+
		'	<div class="form-control {{type}} form-field-binary" tabindex="0" ng-class="{\'selected\': model }">'+
		'		<input type="checkbox"'+
		'			   class="checkbox"'+
		'			   ng-model="model"'+
		'			   ng-class="{\'selected\': model }"'+
		'			   name="{{::name}}"'+
		'			   id="binary-{{uid}}"'+
		'			   ng-required="required"'+
		'			   ng-disabled="disabled" />'+
		'		<label ng-if="label" for="binary-{{uid}}" onclick="return false" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}">{{label}}</label>'+
		'		<div class="input-error-message">'+
		'			<span ng-if="form[name].$invalid && form[name].$dirty">'+
		'	{{invalidMessage}}'+
		'			</span>'+
		'		</div>'+
		'	</div>'+
		'</div>',
	}

}])