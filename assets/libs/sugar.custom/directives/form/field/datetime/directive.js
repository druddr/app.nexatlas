﻿angular.module("Sugar").directive("formFieldDatetime", ['$rootScope', '$locale', function ($rootScope, $locale) {
	return {
		replace: true,
		restrict: 'E',
		require:'ngModel',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			ngModel: '=ngModel',
			required: '=?',
			disabled: '=?',
			invalidMessage: '=?',
			form: '=?',
			name: '@?',
			id: '@?',
			size: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.oController = controller;
			scope.form = controller.$$parentForm;
			scope.dateFormat = $locale.DATETIME_FORMATS.shortDate;
			scope.timeFormat = $locale.DATETIME_FORMATS.shortTime;
			scope.label = $rootScope.language.instance.labels[scope.labelKey];
			scope.uid = Sugar.generateGuid();
			scope.dateFieldName = scope.uid + "_date";
			scope.timeFieldName = scope.uid + "_time";
			scope.date = null;
			scope.time = '';


			if (scope.ngModel) {
				scope.date = new Date(date("Y-m-d H:i:s", strtotime(scope.ngModel)));
				scope.time = scope.date;
			}

			var bPreventUpdate = false;
			scope.$watch('ngModel', function (a, b) {
				if (bPreventUpdate) {
					bPreventUpdate = false;
					return;
				}
				if (!scope.ngModel) {
					scope.date = null;
					scope.time = null;
					return;
				}
				scope.date = new Date(date("Y-m-d H:i:s", strtotime(scope.ngModel)));
				scope.time = scope.date;
			});

			scope.$watch('date', function () {
				if (!scope.date) return;
				bPreventUpdate = true;
				scope.ngModel = date("Y-m-d H:i:s", strtotime(scope.date + ""));
				scope.time = scope.date;
			});

			scope.$watch('time', function () {
				if (!scope.time) return;
				bPreventUpdate = true;
				scope.ngModel = date("Y-m-d H:i:s", strtotime(scope.date + ""));
				scope.date = scope.time;
			});
		},
		template: '' +
		'<div class="form-field-input form-field-datetime form-field-group {{size}} input-type-{{inputType}}">' +
		'	<label ng-if="label" for="input-{{uid}}" class="focused always-focused" ng-class="{' +
		'				\'invalid\': (form[timeFieldName].$invalid && form[timeFieldName].$dirty)|| (form[dateFieldName].$invalid && form[dateFieldName].$dirty),' +
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>' +
		'	<div class="cols with-padding">' +
		'		<div class="col cols-size-5">' +
		'			<input type="date"' +
		'				   class="form-control date {{size}}"' +
		'				   ng-model="date"' +
		'				   name="{{dateFieldName}}"' +
		'				   placeholder="{{dateFormat}}" ' +
		'				   ng-required="required"' +
		'				   ng-disabled="disabled"' +
		'				   language="language"' +
		'				   autocapitalize="off"' +
		'				   autocorrect="off"' +
		'				   autocomplete="off"' +
		'				   spellcheck="false" />' +
		'		</div>' +
		'		<div class="col cols-size-5">' +
		'			<input type="time"' +
		'				   class="form-control time{{size}}"' +
		'				   ng-model="time"' +
		'				   placeholder="{{timeFormat}}" ' +
		'				   name="{{timeFieldName}}"' +
		'				   placeholder="" ' +
		'				   ng-required="required"' +
		'				   ng-disabled="disabled"' +
		'				   language="language"' +
		'				   autocapitalize="off"' +
		'				   autocorrect="off"' +
		'				   autocomplete="off"' +
		'				   spellcheck="false" />' +
		'		</div>' +
		'	</div>' +
		'	<div class="clear"></div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\':(form[timeFieldName].$invalid && form[timeFieldName].$dirty)|| (form[dateFieldName].$invalid && form[dateFieldName].$dirty)}"></div>' +
		'	<div class="input-error-message">' +
		'		<div ng-if="form[dateFieldName].$invalid && form[dateFieldName].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'data-invalida\']"|translate)}}' +
		'		</div>' +
		'		<div ng-if="form[timeFieldName].$invalid && form[timeFieldName].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'horario-invalido\']"|translate)}}' +
		'		</div>' +
		'		<div ng-if="(form[dateFieldName].$dirty || form[dateFieldName].$dirty) && ngModel==\'\' ">' +
		'			{{invalidMessage ? invalidMessage : ("phrases[\'por-favor-verifique-campo\']"|translate)}}' +
		'		</div>' +
		'	</div>' +
		'</div>'
	}
}]);