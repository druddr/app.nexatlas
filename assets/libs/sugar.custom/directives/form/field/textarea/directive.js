﻿
angular.module("Sugar").directive("formFieldTextarea",['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			model: '=',
			required: '=?',
			disabled: '=?',
			maxlength: '=?',
			minlength: '=?',
			invalidMessage: '=?',
			name: '@?',
			form: '=?'
		},
		controller: ['$scope', function ($scope) {
			$scope.uid = Sugar.generateGuid();
		}],
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';
			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}
			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}
			element.removeAttr('label placeholder model required disabled maxlength minlength invalid-message size type mask name');
			element.find('textarea').removeAttr('ng-required ng-model ng-disabled ng-maxlength ng-minlength ng-pattern')
		},
		template:''+
		'<div class="form-field-textarea form-field-group">'+
		'	<label ng-if="label" for="textarea-{{uid}}" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>'+
		'	<textarea class="form-control {{size}}" '+
		'		   placeholder="{{placeholder}}" '+
		'		   ng-model="model" '+
		'		   name="{{::name}}"'+
		'		   id="textarea-{{uid}}"'+
		'		   ng-required="required" '+
		'		   ng-minlength="minlength" '+
		'		   ng-maxlength="maxlength" '+
		'		   ng-disabled="disabled"'+
		'		   ng-pattern="{{pattern}}"'+
		'		   >'+
		'	</textarea>'+
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>'+
		'	<div class="input-error-message">'+
		'		<span ng-if="form[name].$invalid && form[name].$dirty">'+
		'			{{invalidMessage}}'+
		'		</span>'+
		'	</div>'+
		'</div>'
	}

}])