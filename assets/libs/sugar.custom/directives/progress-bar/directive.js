﻿angular.module("Sugar").directive("progressBar", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			//By Amount
			amount: '=?',
			expectedAmount: '=?',
			closeAmount: '=?',
			relativeTopAmount:'=?',//This is the amount that represents the 100%
			//By Percent
			percent: '=?',
			expectedPercent: '=?',
			closePercent: '=?',//Indicates how much points to consider it close to the expected
			//Inverts the logic
			expectLess: '=?'
		},
		transclude: true,
		link: function (scope, element, attrs) {
			if (scope.amount) {
				scope.amount = scope.amount > 0 ? scope.amount : 0;
				scope.expectedAmount = scope.expectedAmount ? scope.expectedAmount : scope.relativeTopAmount;
				scope.closeAmount = scope.closePercent ? scope.closePercent : 1;


				scope.percent = (100 * scope.amount) / scope.relativeTopAmount;
				scope.expectedPercent = (100 * scope.expectedAmount) / scope.relativeTopAmount;
				scope.closePercent = (100 * scope.closeAmount) / scope.relativeTopAmount;
			}

			scope.expectedPercent = scope.expectedPercent ? scope.expectedPercent : 100;
			scope.closePercent = scope.closePercent ? scope.closePercent : 1;
			scope.type = "far";

			if (!scope.expectLess) {
				if (scope.expectedPercent - scope.closePercent > scope.percent) {
					scope.type = "far";
				} else if (scope.expectedPercent - scope.closePercent < scope.percent && scope.expectedPercent > scope.percent) {
					scope.type = "close";
				} else {
					scope.type = "match"
				}
			}
			if (scope.expectLess) {
				if (scope.expectedPercent + scope.closePercent < scope.percent) {
					scope.type = "far";
				} else if (scope.expectedPercent + scope.closePercent > scope.percent && scope.expectedPercent < scope.percent) {
					scope.type = "close";
				} else {
					scope.type = "match"
				}
			}
		},
		template: '' +
		'	<div class="progress-bar">' +
		'		<div class="current {{type}}" style="width:{{percent}}%"></div>' +
		'	</div>'
	}
}])