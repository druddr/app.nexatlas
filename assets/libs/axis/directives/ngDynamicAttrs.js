﻿angular.module("Axis").directive('ngDynamicAttrs', ['$compile', function ($compile) {
	return {
		restrict: 'A',
		scope: { list: '=ngDynamicAttrs' },
		link: function (scope, elem, attrs) {

			for (attr in scope.list) {
				//attrs.$set(attr, scope.list[attr]);
				elem[0].setAttribute(attr, scope.list[attr]);
				//elem.attr(attr, scope.list[attr]);
			}
		},
	};
}]);