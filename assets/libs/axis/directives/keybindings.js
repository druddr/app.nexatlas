﻿angular.module("Axis").directive('onEnterPress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (keycode !== 13) return;

				scope.$apply(function () {
					console.log(attrs.onEnterPress);
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	}
});
angular.module("Axis").directive('onSavePress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(!event.shiftKey && event.ctrlKey && keycode === 83)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	}
});
angular.module("Axis").directive('onSaveShiftedPress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(event.shiftKey && event.ctrlKey && keycode === 83)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	};
});

angular.module("Axis").directive('onDeletePress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(!event.shiftKey && !event.ctrlKey && keycode === 46)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	};
});