﻿angular.module("Axis").factory('Authorization-httpInterceptor', ['$q', '$rootScope', '$log', '$location', function ($q, $rootScope, $log, $location) {
	return {
		request: function (config) {			
			return config || $q.when(config);
		},
		response: function (response) {
			return response || $q.when(response);
		},
		responseError: function (response) {
			if (response.status == 401) {
				$location.path(settings.router.authentication);
			}
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.defaults.withCredentials = true;
	$httpProvider.interceptors.push('Authorization-httpInterceptor');
}])