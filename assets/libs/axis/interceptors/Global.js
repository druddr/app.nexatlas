﻿angular.module("Axis").factory('Global-httpInterceptor', ['$q', '$rootScope', '$log', function ($q, $rootScope, $log) {

	var numLoadings = 0;

	return {
		request: function (config) {

			numLoadings++;
			// Show loader
			$rootScope.$broadcast("axis:request:start", { $config: config });
			return config || $q.when(config);

		},
		response: function (response) {

			if ((--numLoadings) === 0) {
				// Hide loader
				$rootScope.$broadcast("axis:request:all:finished", { $config: response });
			}
			$rootScope.$broadcast("axis:request:success", { $response: response });

			return response || $q.when(response);

		},
		responseError: function (response) {

			if (!(--numLoadings)) {
				// Hide loader
				$rootScope.$broadcast("axis:request:all:finished", { $response: response });
			}
			$rootScope.$broadcast("axis:request:error", { $response: response });
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('Global-httpInterceptor');
}]);
