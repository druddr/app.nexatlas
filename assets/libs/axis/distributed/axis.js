var settings = {
	debug:false,
	router: {
		default_route: '/app/dashboard/index',
		authentication: '/app/authentication/login',
	},
	path: {
		app: 'app',
		controllers: 'app/controllers/',
		services: 'app/services/',
		models: 'app/models/',
		components: 'app/components/',
		views: 'app/views/',
		partials: 'app/partials/',
		directives: 'directives/',
		css: 'css/',
		js: 'js/'
	},
	components: {
		default_file: 'directive',
		default_components: [ ]
	}
};
"use strict"

angular.module('Axis', ['ngRoute', 'ngResource']);


angular.module("Axis").config(['$compileProvider', function ($compileProvider) {
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(http|file|geo|tel|sms|https?|ftp|mailto|chrome-extension):/);	
}]);



var Loader = {
	scripts: {},
	styles: {},
	intervals: {},
	interval_limit: 1000,
	debug: false,
	getDebugTime: function () {
		return (Loader.debug) ? (new Date()).getTime() : 'standard';
	},
	setCallback: function (script, callback) {
		script.onload = function () {
			callback();
		}
		// IE 6 & 7
		script.onreadystatechange = function () {
			if (this.readyState == 'complete') callback();
		}
	},
	setErrorCallback: function (script, errorcallback) {
		script.onerror = errorcallback;
	},
	checks: function (objectName, callback) {
		if (typeof callback != "function") return false;
		Loader.intervals[objectName] = { interval: null, count: 0 };
		Loader.intervals[objectName].interval = setInterval(function () {
			if (Loader.intervals[objectName].count == Loader.interval_limit) {
				clearInterval(Loader.intervals[objectName].interval);
				return false;
			}
			if (!window[objectName]) {
				Loader.intervals[objectName].count++;
				return false;
			}
			clearInterval(Loader.intervals[objectName].interval);			
			callback();
			return objectName;
		}, 1);
	},
	load: {
		async: true,
		synchronously: function () {
			Loader.load.async = false;
			return Loader.load;
		},
		withnocache:function(){
			Loader.debug = true;
			return Loader.load;
		},
		js: function (path, callback, errorcallback,cssClass,force) {
			var originalpath = path;

			path = path.replace(".js", "") + ".js?version=" + (Loader.getDebugTime()), Loader.load.async;


			if (Loader.scripts[path] && !force) {
				if (callback) {
					callback();
				}
				Loader.load.async = true;
				Loader.debug = false;
				return;
			}
			var script = document.getElementById(originalpath);

			Loader.unload.byClass(cssClass);

			//só vai cair aqui quando estiver usando withnocache
			if (script != null) {
				document.body.removeChild(script, document.body);
			}

			var script = document.createElement("script");
			script.src = path;

			if (Loader.debug) {
				script.id = path;
			}

			if (cssClass) {
				script.className = cssClass;
			}

			//seta os callbacks
			document.head.appendChild(script);

			Loader.scripts[path] = script;

			if (callback) {
				Loader.setCallback(script, callback);
			}
			if (errorcallback) {
				Loader.setErrorCallback(script, errorcallback);
			}
			Loader.load.async = true;
			Loader.debug = false;
			return;
		},
		css: function (path, cssClass) {
			path = path.replace(".css", "");
			var originalpath = path;
			path = path.replace(".css", "") + ".css?version=" + (Loader.getDebugTime())
			Loader.load.async=true;
			Loader.debug = false;
			if (Loader.styles[path]) return;
			var elm = document.createElement("link");
			elm.href = path;
			elm.rel = "stylesheet";
			if (cssClass) elm.className = cssClass;
			document.head.appendChild(elm);
			Loader.styles[path] = cssClass;
		}
	},
	unload:{
		js:function(path){
		},
		css:function(path){
			document.body.removeChild(script);
		},
		byClass: function (cssClass) {
			if (!cssClass) return;
			for (var i in Loader.styles) {
				if (Loader.styles[i] == cssClass) delete Loader.styles[i];
			}
			for (var i in Loader.scripts) {
				if (Loader.scripts[i] == cssClass) delete Loader.scripts[i];
			}
			$("."+cssClass).remove();
		}
	}
}

String.prototype.upperCaseFirst = function () {
	var f = this.charAt(0).toUpperCase();
	return f + this.substr(1);
}
var Storage = function () {
    var is_extension = window.location.protocol == 'chrome-extension:';
    var storage = is_extension ? chrome.storage.local : window.localStorage;
    var not_set_value = ('@VALUE_NOT_SET@');
    var max_seconds_waiting = 1;
    return {
        local: {
            return_value: not_set_value,
            set: function (key, value) {
                if (!key == undefined || value == undefined) { return; }
                if (is_extension) {
                    var obj = {};
                    obj[key] = value;
                    chrome.storage.local.set(obj);
                    return;
                }
                storage.setItem(key, JSON.stringify(value));
            },
            get: function (key, default_value) {
                if (is_extension) {
                    Storage.local.return_value = not_set_value;
                    var initial = strtotime("now");
                    console.log("1111")
                    chrome.storage.local.get(key, function (result) {
                        console.log(key);
                        if (result[key] == undefined) return default_value == undefined ? null : default_value;
                        Storage.local.return_value = result[key];
                    });
                    while (Storage.local.return_value == not_set_value) {
                        //var interval_get = setInterval(function () {
                        if (strtotime("now") - initial > max_seconds_waiting) {
                            Storage.local.return_value = null;
                            //clearInterval(interval);
                            //return null;
                        }
                        console.log("222");
                        //if (Storage.local.return_value != not_set_value) {
                        //	console.log("TEST", Storage.local.return_value);
                        //	clearInterval(interval_get);
                        //}
                    }
                    //}, 100);
                    console.log("333");
                    return Storage.local.return_value;
                }
                var value = storage.getItem(key);
                if (!value) return default_value == undefined ? null : default_value;
                return JSON.parse(value);
            },
            getAll: function () {
                var data = new Array();
                for (var i = 0; i < storage.length; i++) {
                    var key = storage.key(i);
                    var value = storage.getItem(key);

                    data.push({ key: key, value: value });
                }
                return data;
            },
            clear: function () {
                storage.clear(function () { });
            }
        }
    }
}();
angular.module('Axis-Dependency-Injection', ['Axis']);
//Lazy Load Providers 
angular.module("Axis-Dependency-Injection").config(['$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
	function ($controllerProvider, $compileProvider, $filterProvider, $provide) {
		angular.module("Axis-Dependency-Injection").load = {
			directive: $compileProvider.directive,
			controller: $controllerProvider.register,
			filter: $filterProvider.register,
			factory: $provide.factory
		}

		//Configuring Components object
		angular.module("Axis-Dependency-Injection").components = {
			_components_config: {},
			config: function (name, object) {
				if (!object) return angular.module("Axis-Dependency-Injection").components._components_config[name];
				angular.module("Axis-Dependency-Injection").components._components_config[name.trim()] = object;
				return angular.module("Axis-Dependency-Injection").components._components_config[name];
			}
		}
	}
]);

angular.module('Axis-Dependency-Injection').factory('Component', ['$injector', '$rootScope', '$timeout', function ($injector, $rootScope, $timeout) {
	var loaded_components = {};
	var factory = {
		load: function (components, fncallback) {
			components.total_components_loaded = 0;
			var load = {
				config: function (component) {
					var directory = component.directory;
					var name = directory.replace(/\//g, '-').trim();
					if (settings.debug) console.log('Loading config file for ', directory)
					Loader.load.js(settings.path.components + directory + '/config.js', function () {
						if (settings.debug) console.log('Loading config object for', directory);
						var interval = setInterval(function () {
							var config = angular.module('Axis-Dependency-Injection').components.config(name);
							if (!config) return;
							clearInterval(interval);
							loadcomponent(config);
						}, 1);
						function loadcomponent(config) {
							var component = config;
							component.directives = component.directives ? component.directives : [];
							component.directory = directory;
							component.componentServices = component.componentServices ? component.componentServices : [];
							component.services = component.services ? component.services : [];
							component.models = component.models ? component.models : [];
							component.controllers = component.controllers ? component.controllers : [];
							component.components = component.components ? component.components : [];
							load.components(component, directory);
						}
					});
				},
				components: function (component, directory) {
					if(settings.debug) console.log('Loading components for directory', directory)
					if (component.components.length == 0) {
						return load.componentServices(component, directory);
					}
					factory.load(component.components, function () {
						load.componentServices(component, directory);
					});
				},
				componentServices: function (component, directory) {
					if (settings.debug) console.log('Loading component services for directory', directory)
					var total = component.componentServices.length;
					if (total == 0) {
						load.services(component, directory);
						return;
					}
					for (var i = 0; i < component.componentServices.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.componentServices[i], function () {
							total--;
							if (total == 0) {
								load.services(component, directory);
							}
						})
					}
				},
				services: function (component, directory) {
					if (settings.debug) console.log('Loading services for directory', directory)
					var total = component.services.length;
					if (total == 0) {
						load.models(component, directory);
						return;
					}
					for (var i = 0; i < component.services.length; i++) {
						Loader.load.js(settings.path.services + '/' + component.services[i], function () {
							total--;
							if (total == 0) {
								load.models(component, directory);
							}
						})
					}
				},
				models: function (component, directory) {
					if (settings.debug) console.log('Loading models for directory', directory)
					var total = component.models.length;
					if (total == 0) {
						load.controllers(component, directory);
						return;
					}
					for (var i = 0; i < component.models.length; i++) {
						Loader.load.js(settings.path.models + '/' + component.models[i], function () {
							total--;
							if (total == 0) {
								load.controllers(component, directory);
							}
						})
					}
				},
				controllers: function (component, directory) {
					if (settings.debug) console.log('Loading controllers for directory', directory)
					var total = component.controllers.length;
					if (total == 0) {
						load.directives(component, directory);
						return;
					}
					for (var i = 0; i < component.controllers.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.controllers[i], function () {
							total--;
							if (total == 0) {
								load.directives(component, directory);
							}
						})
					}
				},
				directives: function (component, directory) {
					if (settings.debug) console.log('Loading directives for directory', directory)
					var total = component.directives.length;
					if (total == 0) {
						fncallback();
						return;
					}
					for (var i = 0; i < component.directives.length; i++) {
						Loader.load.js(settings.path.components + directory + '/' + component.directives[i], function () {
							total--;
							var interval;
							if (total == 0) {
								//	fncallback();
								interval = setInterval(function () {
									var directive = directory;
									var directive_name_parts = directive.split('/');
									directive_name = directive_name_parts[0];
									for (var i = 1; i < directive_name_parts.length; i++) {
										directive_name += directive_name_parts[i].upperCaseFirst();
									}
									var directive_name_parts = directive_name.split('-');
									directive_name = directive_name_parts[0];
									for (var i = 1; i < directive_name_parts.length; i++) {
										directive_name += directive_name_parts[i].upperCaseFirst();
									}
									directive_name = directive_name.replace('Directive', '') + 'Directive';
									if (total == 0 && $injector.has(directive_name)) {
										components.total_components_loaded++;
										loaded_components[directory] = true;
										if (settings.debug) {
											console.log('%cLoaded directive:' + directive_name, 'background:#0C5;color:#fff');
											console.log('%cDirective ready:', 'background:#0C5;color:#fff', $injector.has(directive_name));
											console.log('%cTotal loaded', 'background:#08c;color:#fff', components.total_components_loaded);
											console.log('%cFrom total', 'background:#08c;color:#fff', components.length, "IN", components);
										}
										clearInterval(interval);
									}
									else {
										if (settings.debug) console.log('%cDirective not loaded yet:'+ directive_name,'background:#fff;color:#fff');
									}
								}, 1);
							}
						});
					}
				}
			}
			for (var i = 0; i < components.length; i++) {
				var comp = components[i];
				if (typeof comp == 'string') {
					comp = {
						directory: comp
					}
				}
				if (loaded_components[comp.directory] != undefined) {
					components.total_components_loaded++;
					continue;
				}
				load.config(comp);
			}
			var callbackInterval = setInterval(function () {
				if (components.total_components_loaded != components.length) return;
				if (settings.debug) console.log('Components loaded for ', components)
				fncallback();
				clearInterval(callbackInterval);
			},1000);
		}
	}
	return factory;
}]);
angular.module('Axis-Router', ['Axis', 'Axis-Dependency-Injection']);
angular.module("Axis-Router").config([
	function () {
		angular.module("Axis-Router").Router = {
			getRoute: function (location) {
				location = location.replace("#/" + settings.path.app + "/", "");
				location = location.replace("/" + settings.path.app + "/", "");
				location = location[location.length - 1] == '/' ? location : location + '/';
				var route = {
					uri_parts: location.split("/"),
					//Defines the index of controller within all the uri parts
					controller_index: null,
					//Used to create CamelCase name
					controller_parts: null,
					//Name of the controller class object
					controller_name: '',
					//Controller might be under some module folder
					controller_path: '',
					//Name of the method which will be the real controller. Must be controller_class method
					action_name: '',
					//User to create pascalCase of action name
					action_name_parts: null,
					//Url broken into pieces by : separator
					params_parts: {},
					//Params index on the uri parts
					params_index: 0,
					//Params passed thru url
					params: {},
					//Route path without parameters
					path: '',
					//Array containing the files of dependencies not loaded. Will warn if some dependencie could not be loaded
					loader_dependencie_error: [],
					//View path
					view_path: '',
					a: -1
				}
				for (i in route.uri_parts) {
					route.params_parts = route.uri_parts[i].split('=');
					if (route.params_parts.length == 1) continue;
					route.params_index = route.params_index == 0 ? parseInt(i) : route.params_index;
					try {
						route.params[route.params_parts[0]] = JSON.parse(decodeURI(route.params_parts[1]));
					}
					catch (e) {
						route.params[route.params_parts[0]] = (decodeURI(route.params_parts[1]));
					}
				}
				route.params_index = route.params_index == 0 ? route.uri_parts.length - 1 : route.params_index;
				route.controller_index = route.params_index - 2;

				//This iterate uri_parts to find controller, action and possible parameters
				for (i in route.uri_parts) {
					if (route.uri_parts[i] == '') continue;

					if (i == route.controller_index + 1) {
						route.action_name = route.uri_parts[i];
					}
					if (i == route.controller_index && route.uri_parts[i - 1] != undefined) {
						route.controller_path += route.uri_parts[i - 1] + '/';
					}
				}

				route.path = '/' + settings.path.app + "/" + route.controller_path + route.uri_parts[route.controller_index] + '/' + route.action_name;

				route.controller_parts = route.uri_parts[route.controller_index].split("-");
				//Create the CamelCase controller name based on - separators
				for (var i = 0; i < route.controller_parts.length; i++) {
					route.controller_name += route.controller_parts[i].upperCaseFirst();
				}
				return route
			},
			resolve: ['$q', '$route', '$rootScope', '$injector', '$timeout', 'Component', function ($q, $route, $rootScope, $injector, $timeout, Component) {
				$rootScope.$broadcast("axis:resolve:default_components:load");

				//Loads default components
				Component.load(settings.components.default_components, function () {
					$rootScope.defaultComponentsLoaded = true;
				})

				$rootScope.$broadcast("axis:resolve:start");

				var route = angular.module('Axis-Router').Router.getRoute(window.location.hash),
					//Queue object
					deferred = $q.defer(),
					//Interval object for Loader.load.js events
					loader_interval = null,
					//Controller class object itself
					controller_class = null,
					//Controller method found matching the action. This will be Angular Controller
					controller_instance = null,
					//Correct path to access route object
					route_path = '/app/:name*';


				$rootScope.$broadcast("axis:resolve:unload_css:start");
				Loader.unload.byClass("dynamic-generated-css");
				$rootScope.$broadcast("axis:resolve:unload_css:finish");

				Loader.load.js(settings.path.controllers + route.controller_path + route.controller_name, function () {
					$rootScope.$broadcast("axis:resolve:controller_loading:start");

					controller_class = window[route.controller_name];
					if (!controller_class) {
						throw "Controller class was suposed to be '" + route.controller_name + "' but wasn\'t found";
					}
					//set the params property to give controller access to url given parameters
					controller_class.params = route.params;

					//Get controller method definition
					controller_instance = controller_class[route.action_name];

					//$route.routes[route_path].templateUrl = settings.path.views + controller_path + controller_name + '/' + action_name + '.html';
					//$route.routes[route_path].template = 'TESTE';
					if (!controller_instance && route.action_name) {
						//creating pascalCase for the action name if the dashed wasn't found
						route.action_name_parts = route.action_name.split('-');
						route.action_name = route.action_name_parts[0];
						for (var i = 1; i < route.action_name_parts.length; i++) {
							route.action_name += route.action_name_parts[i].upperCaseFirst();
						}
						controller_instance = controller_class[route.action_name];
					}

					//Sets Controller function declaration to the route object
					$route.routes[route_path].controller = controller_instance;

					var loaded = {
						js: (controller_class.js && controller_class.js.length > 0) ? controller_class.js.length : 0,
						css: (controller_class.css && controller_class.css.length > 0) ? controller_class.css.length : 0,
						models: (controller_class.models && controller_class.models.length > 0) ? controller_class.models.length : 0,
						services: (controller_class.services && controller_class.services.length > 0) ? controller_class.services.length : 0,
						directives: (controller_class.directives && controller_class.directives.length > 0) ? controller_class.directives.length : 0,
						components: (controller_class.components && controller_class.components.length > 0) ? controller_class.components.length : 0
					}

					$rootScope.$broadcast("axis:resolve:custom_css_loading:start");
					//Loading CSS
					if (controller_class.css && controller_class.css.length > 0) {
						for (var i in controller_class.css) {
							Loader.load.css(settings.path.css + controller_class.css[i].replace(".css", "") + ".css", "dynamic-generated-css");
							loaded.css--;
						}
					}
					function loadDependencyFile(dependency, type) {
						var dependencyFileName = settings.path[type] + dependency.replace(".js", "") + ".js";
						if (dependency.indexOf("http://") == 0 || dependency.indexOf("https://") == 0) {
							dependencyFileName = dependency;
						}
						Loader.load.js(dependencyFileName, function () {
							loaded[type]--;
							//if (type == 'models') {
							//	window[dependency].fill = function (properties) {
							//		for(var i in argument)
							//	};
							//}
						}, function () {
							route.loader_dependencie_error.push(dependency + ' on path ' + settings.path[type] + dependency.replace(".js", ""));
							console.error("Router could not find file from " + type + ":\r\n" + dependency);
						});
					}

					function loadDependency(type) {
						if (controller_class[type] && controller_class[type].length > 0) {
							for (var i in controller_class[type]) {
								loadDependencyFile(controller_class[type][i], type);
							}
						}
					}

					//Loading JS
					$rootScope.$broadcast("axis:resolve:custom_javascript_loading:start");
					loadDependency('js');

					//Loading services
					$rootScope.$broadcast("axis:resolve:services_loading:start");
					loadDependency('services');

					//Loading models
					$rootScope.$broadcast("axis:resolve:models_loading:start");
					loadDependency('models');

					//Loading directives
					$rootScope.$broadcast("axis:resolve:directives_loading:start");
					loadDependency('directives');

					//Loading components
					$rootScope.$broadcast("axis:resolve:components_loading:start");
					if (controller_class.components && controller_class.components.length > 0) {
						Component.load(controller_class.components, function () {
							loaded.components = 0;
						});
					}


					$rootScope.$broadcast("axis:resolve:models_loading:finish");


					//Start interval checking if dependencies are loaded correctly
					loader_interval = setInterval(function () {
						if (route.loader_dependencie_error.length > 0) {
							var loader_error_message = "Some controller dependencies could not be loaded due to some reason.";
							for (i in route.loader_dependencie_error) loader_error_message += "\r\n\t - " + route.loader_dependencie_error[i];
							console.error(loader_error_message);
							clearInterval(loader_interval);
							return;
						}
						
						if (loaded.css == 0 &&
							loaded.js == 0 &&
							loaded.models == 0 &&
							loaded.directives == 0 &&
							loaded.components == 0 &&
							loaded.services == 0 &&
							$rootScope.defaultComponentsLoaded) {
							clearInterval(loader_interval);
							
							$timeout(function () {
								$rootScope.$apply(function () {
									deferred.resolve();
									$rootScope.$broadcast("axis:resolve:controller_loading:finish");
								});
							});
						}
					}, 10);
				});
				return deferred.promise;
			}]
		}

	}
]);
angular.module("Axis-Router").config(['$routeProvider',
	function ($routeProvider) {
		$routeProvider.
			when('/' + settings.path.app + '/:name*', {
				templateUrl: function (a) {
					var name = a.name == undefined ? a : a.name,
						parts = a.name.split('/'),
						part,
						page = '';
					for (i in parts) {
						part = parts[i].split("=");
						if (part[1] != undefined) continue;
						if (parts[i] == '') continue;
						page += parts[i] + '/';
					}
					page = page.substr(0, page.length - 1);
					return settings.path.views + page + '.html';
				},
				resolve: {
					load: angular.module('Axis-Router').Router.resolve
				}
			}).
			otherwise({
				redirectTo: settings.router.default_route
			});
	}
]);

angular.module("Axis").factory('Authorization-httpInterceptor', ['$q', '$rootScope', '$log', '$location', function ($q, $rootScope, $log, $location) {
	return {
		request: function (config) {			
			return config || $q.when(config);
		},
		response: function (response) {
			return response || $q.when(response);
		},
		responseError: function (response) {
			if (response.status == 401) {
				$location.path(settings.router.authentication);
			}
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.defaults.withCredentials = true;
	$httpProvider.interceptors.push('Authorization-httpInterceptor');
}])
angular.module("Axis").factory('Global-httpInterceptor', ['$q', '$rootScope', '$log', function ($q, $rootScope, $log) {

	var numLoadings = 0;

	return {
		request: function (config) {

			numLoadings++;
			// Show loader
			$rootScope.$broadcast("axis:request:start", { $config: config });
			return config || $q.when(config);

		},
		response: function (response) {

			if ((--numLoadings) === 0) {
				// Hide loader
				$rootScope.$broadcast("axis:request:all:finished", { $config: response });
			}
			$rootScope.$broadcast("axis:request:success", { $response: response });

			return response || $q.when(response);

		},
		responseError: function (response) {

			if (!(--numLoadings)) {
				// Hide loader
				$rootScope.$broadcast("axis:request:all:finished", { $response: response });
			}
			$rootScope.$broadcast("axis:request:error", { $response: response });
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('Global-httpInterceptor');
}]);

angular.module("Axis").directive('onEnterPress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (keycode !== 13) return;

				scope.$apply(function () {
					console.log(attrs.onEnterPress);
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	}
});
angular.module("Axis").directive('onSavePress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(!event.shiftKey && event.ctrlKey && keycode === 83)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	}
});
angular.module("Axis").directive('onSaveShiftedPress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(event.shiftKey && event.ctrlKey && keycode === 83)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	};
});

angular.module("Axis").directive('onDeletePress', function () {
	return {
		link: function (scope, element, attrs) {
			element.bind("keydown keypress", function (event) {
				var keycode = event.which || event.keyCode || event.charCode;

				if (!(!event.shiftKey && !event.ctrlKey && keycode === 46)) return;

				scope.$apply(function () {
					scope.$eval(attrs.onEnterPress);
				});
				event.preventDefault();
			});
		}
	};
});
angular.module("Axis").directive('ngDynamicAttrs', ['$compile', function ($compile) {
	return {
		restrict: 'A',
		scope: { list: '=ngDynamicAttrs' },
		link: function (scope, elem, attrs) {

			for (attr in scope.list) {
				//attrs.$set(attr, scope.list[attr]);
				elem[0].setAttribute(attr, scope.list[attr]);
				//elem.attr(attr, scope.list[attr]);
			}
		},
	};
}]);