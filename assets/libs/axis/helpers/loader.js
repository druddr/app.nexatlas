﻿var Loader = {
	scripts: {},
	styles: {},
	intervals: {},
	interval_limit: 1000,
	debug: false,
	getDebugTime: function () {
		return (Loader.debug) ? (new Date()).getTime() : 'standard';
	},
	setCallback: function (script, callback) {
		script.onload = function () {
			callback();
		}
		// IE 6 & 7
		script.onreadystatechange = function () {
			if (this.readyState == 'complete') callback();
		}
	},
	setErrorCallback: function (script, errorcallback) {
		script.onerror = errorcallback;
	},
	checks: function (objectName, callback) {
		if (typeof callback != "function") return false;
		Loader.intervals[objectName] = { interval: null, count: 0 };
		Loader.intervals[objectName].interval = setInterval(function () {
			if (Loader.intervals[objectName].count == Loader.interval_limit) {
				clearInterval(Loader.intervals[objectName].interval);
				return false;
			}
			if (!window[objectName]) {
				Loader.intervals[objectName].count++;
				return false;
			}
			clearInterval(Loader.intervals[objectName].interval);			
			callback();
			return objectName;
		}, 1);
	},
	load: {
		async: true,
		synchronously: function () {
			Loader.load.async = false;
			return Loader.load;
		},
		withnocache:function(){
			Loader.debug = true;
			return Loader.load;
		},
		js: function (path, callback, errorcallback,cssClass,force) {
			var originalpath = path;

			path = path.replace(".js", "") + ".js?version=" + (Loader.getDebugTime()), Loader.load.async;


			if (Loader.scripts[path] && !force) {
				if (callback) {
					callback();
				}
				Loader.load.async = true;
				Loader.debug = false;
				return;
			}
			var script = document.getElementById(originalpath);

			Loader.unload.byClass(cssClass);

			//só vai cair aqui quando estiver usando withnocache
			if (script != null) {
				document.body.removeChild(script, document.body);
			}

			var script = document.createElement("script");
			script.src = path;

			if (Loader.debug) {
				script.id = path;
			}

			if (cssClass) {
				script.className = cssClass;
			}

			//seta os callbacks
			document.head.appendChild(script);

			Loader.scripts[path] = script;

			if (callback) {
				Loader.setCallback(script, callback);
			}
			if (errorcallback) {
				Loader.setErrorCallback(script, errorcallback);
			}
			Loader.load.async = true;
			Loader.debug = false;
			return;
		},
		css: function (path, cssClass) {
			path = path.replace(".css", "");
			var originalpath = path;
			path = path.replace(".css", "") + ".css?version=" + (Loader.getDebugTime())
			Loader.load.async=true;
			Loader.debug = false;
			if (Loader.styles[path]) return;
			var elm = document.createElement("link");
			elm.href = path;
			elm.rel = "stylesheet";
			if (cssClass) elm.className = cssClass;
			document.head.appendChild(elm);
			Loader.styles[path] = cssClass;
		}
	},
	unload:{
		js:function(path){
		},
		css:function(path){
			document.body.removeChild(script);
		},
		byClass: function (cssClass) {
			if (!cssClass) return;
			for (var i in Loader.styles) {
				if (Loader.styles[i] == cssClass) delete Loader.styles[i];
			}
			for (var i in Loader.scripts) {
				if (Loader.scripts[i] == cssClass) delete Loader.scripts[i];
			}
			$("."+cssClass).remove();
		}
	}
}
