﻿var Storage = function () {
    var is_extension = window.location.protocol == 'chrome-extension:';
    var storage = is_extension ? chrome.storage.local : window.localStorage;
    var not_set_value = ('@VALUE_NOT_SET@');
    var max_seconds_waiting = 1;
    return {
        local: {
            return_value: not_set_value,
            set: function (key, value) {
                if (!key == undefined || value == undefined) { return; }
                if (is_extension) {
                    var obj = {};
                    obj[key] = value;
                    chrome.storage.local.set(obj);
                    return;
                }
                storage.setItem(key, JSON.stringify(value));
            },
            get: function (key, default_value) {
                if (is_extension) {
                    Storage.local.return_value = not_set_value;
                    var initial = strtotime("now");
                    console.log("1111")
                    chrome.storage.local.get(key, function (result) {
                        console.log(key);
                        if (result[key] == undefined) return default_value == undefined ? null : default_value;
                        Storage.local.return_value = result[key];
                    });
                    while (Storage.local.return_value == not_set_value) {
                        //var interval_get = setInterval(function () {
                        if (strtotime("now") - initial > max_seconds_waiting) {
                            Storage.local.return_value = null;
                            //clearInterval(interval);
                            //return null;
                        }
                        console.log("222");
                        //if (Storage.local.return_value != not_set_value) {
                        //	console.log("TEST", Storage.local.return_value);
                        //	clearInterval(interval_get);
                        //}
                    }
                    //}, 100);
                    console.log("333");
                    return Storage.local.return_value;
                }
                var value = storage.getItem(key);
                if (!value) return default_value == undefined ? null : default_value;
                return JSON.parse(value);
            },
            getAll: function () {
                var data = new Array();
                for (var i = 0; i < storage.length; i++) {
                    var key = storage.key(i);
                    var value = storage.getItem(key);

                    data.push({ key: key, value: value });
                }
                return data;
            },
            clear: function () {
                storage.clear(function () { });
            }
        }
    }
}();