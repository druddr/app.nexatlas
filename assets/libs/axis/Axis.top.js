﻿"use strict"

angular.module('Axis', ['ngRoute', 'ngResource']);


angular.module("Axis").config(['$compileProvider', function ($compileProvider) {
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(http|file|geo|tel|sms|https?|ftp|mailto|chrome-extension):/);	
}]);


