﻿var settings = {
	debug:false,
	router: {
		default_route: '/app/dashboard/index',
		authentication: '/app/authentication/login',
	},
	path: {
		app: 'app',
		controllers: 'app/controllers/',
		services: 'app/services/',
		models: 'app/models/',
		components: 'app/components/',
		views: 'app/views/',
		partials: 'app/partials/',
		directives: 'directives/',
		css: 'css/',
		js: 'js/'
	},
	components: {
		default_file: 'directive',
		default_components: [ ]
	}
};