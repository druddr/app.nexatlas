function initUI() {
//  if (!mapboxgl.supported({ failIfMajorPerformanceCaveat: true })) {
//    $("#mapaDiv").css("left", 0);
//    $("#mapaDiv").html("" +
//        "<h3>Desculpe, mas o NexAtlas não está conseguindo executar aqui.  :-(</h3>" +
//        "<p>O NexAtlas precisa de recursos de aceleração gráfica por hardware, mas não está conseguindo ter acesso a estes recursos.<br/>" +
//        "Em alguns casos, atualizar o driver de vídeo resolve o problema. Você pode também tentar usar outro computador mais recente.<br/>" +
//        "<br/>" +
//        "Se precisar de ajuda, entre em contato conosco pelo e-mail contato@nexatlas.com.<br/>" +
//        "<br/>" +
//        "Atenciosamente,<br/>" +
//        "<br/>" +
//        "Equipe NexAtlas");
//    return;
//  }

//  initSVG();
  initUIEvents();

  $("#menuMapa").css("background-color", "#0066ff");
  $("#barraEsquerda").show();
  $("#barraDireita").show();
  $("#barraPesquisa").show();
}

function initSVG() {
    jQuery('img.svg').each(function() {
        var $img = jQuery(this);
        var imgID = $img.attr('id');
        var imgClass = $img.attr('class');
        var imgURL = $img.attr('src');
        jQuery.get(imgURL, function(data) {
            var $svg = jQuery(data).find('svg');
            if(typeof imgID !== 'undefined') {
                $svg = $svg.attr('id', imgID);
            }
            if(typeof imgClass !== 'undefined') {
                $svg = $svg.attr('class', imgClass+' replaced-svg');
            }
            $svg = $svg.removeAttr('xmlns:a');
            $img.replaceWith($svg);
        }, 'xml');
    });
}

var menuControl = {
  "Esquerda": {
    ativo: false,
    idAtivo: "menuMapa",
    neutro: "menuMapa",
    bgColor: "#0066ff",
    opacity: 1
  },
  "Direita": {
    ativo: false,
    idAtivo: null,
    bgColor: "#0066ff",
    opacity: 1
  }
}

var menuDescAtivo = false;

function initUIEvents() {
    $("#hamburguer").click( function() {
      if (!menuDescAtivo) {
        menuHide("Esquerda");
        showMenuDesc();
      }
      else {
        hideMenuDesc();
      }
    });

    $("#barraEsquerdaBotoesSup > div, #barraEsquerdaBotoesInf > div").click(function() {
      var id = $(this).attr("id");
      if (menuControl["Esquerda"].ativo && (id==menuControl["Esquerda"].neutro || menuControl["Esquerda"].idAtivo==id)) {
//        console.log('hide');
        menuHide("Esquerda");
      }
      else {
//        console.log('show  ' + id);
        menuShow("Esquerda", id);
      }
    });

    $("#barraDireitaBotoesSup > div").click( function() {
      var id = $(this).attr("id");
      if (menuControl["Direita"].ativo && menuControl["Direita"].idAtivo==id) {
        menuHide("Direita");
      }
      else {
        menuShow("Direita", id);
      }
    });
}

function showMenuDesc() {
  if (!menuDescAtivo) {
    $("#barraEsquerda").animate({width: "256px"}, {duration: 50, complete: function() {
      $("#barraEsquerda p").css("opacity", 0).show().animate({opacity: 1}, {duration: 50});
    }});
    menuDescAtivo = true;
  }
}

function hideMenuDesc() {
  if (menuDescAtivo) {
    $("#barraEsquerda p").animate({opacity: 0}, {duration: 50, complete: function() {
      $("#barraEsquerda p").hide();
      $("#barraEsquerda").animate({width: "48px"}, {duration: 50});
    }});
    menuDescAtivo = false;
  }
}

function menuShow(menu, id) {
  hideMenuDesc();
  if (!menuControl[menu].ativo || menuControl[menu].idAtivo!=id) {

    var idTab = id + "-tab";
    var idAtivoTab = menuControl[menu].idAtivo + "-tab";
    if(!menuControl[menu].ativo) {
      if (menuControl[menu].neutro && menuControl[menu].neutro==id) return;

      if (menuControl[menu].neutro && menuControl[menu].neutro==menuControl[menu].idAtivo) {
        $("#"+menuControl[menu].idAtivo).css({
          "background-color": "",
          "opacity": ""
        });
      }

      $("#"+idTab+" > div").css({"opacity": 0, "padding-top": "20px"});
      $("#"+idTab).show();
      $("#painel"+menu).show();
      $("#"+idTab+" > div").animate({"opacity": 1, "padding-top": 0}, {duration: 100, complete: function() {
          if (id=="menuPesquisa") {
            $("#pesquisa").focus();
          }
        }
      });

      $("#"+id).css({
        "background-color": menuControl[menu].bgColor,
        "opacity": menuControl[menu].opacity
      });
      menuControl[menu].idAtivo = id;
      menuControl[menu].ativo = true;
    }
    else if(menuControl[menu].idAtivo!=id) {
      if (id!="menuPesquisa") {
        $('.na-searchInput').css('z-index', '');
      }

      $("#"+idAtivoTab).hide();
      $("#"+menuControl[menu].idAtivo).css({
        "background-color": "",
        "opacity": ""
      });

      if (id!="menuPesquisa") {
        $("#pesquisa").val("");
        angular.element($('#pesquisa')).triggerHandler('input');
      }

      $("#"+idTab+" > div").css({"opacity": 0, "padding-top": "20px"});
      $("#"+idTab).show();
      $("#"+idTab+" > div").animate({"opacity": 1, "padding-top": 0}, {duration: 100, complete: function() {
          if (id=="menuPesquisa") $("#pesquisa").focus();
        }
      });

      $("#"+id).css({
        "background-color": menuControl[menu].bgColor,
        "opacity": menuControl[menu].opacity
      });
      menuControl[menu].idAtivo = id;
    }
  }
}

function menuHide(menu) {
  hideMenuDesc();

  $('.na-searchInput').css('z-index', '');

  var idAtivoTab = menuControl[menu].idAtivo + "-tab";
  if (menuControl[menu].ativo) {
    $("#painel"+menu).hide();
    $("#"+idAtivoTab).hide();
    menuControl[menu].ativo = false;

    $("#" + menuControl[menu].idAtivo).css({
      "background-color": "",
      "opacity": ""
    });
    if (menuControl[menu].neutro) {
      $("#"+menuControl[menu].neutro).css({
        "background-color": menuControl[menu].bgColor,
        "opacity": menuControl[menu].opacity
      });
    }

    menuControl[menu].idAtivo = menuControl[menu].neutro;

    $("#pesquisa").val("");
    angular.element($('#pesquisa')).triggerHandler('input');
  }
}

$(document).ready(function() {
  $('.na-searchInput input').on('focus', function() {
    $('.na-searchInput').css('z-index', '30');
  });
});
