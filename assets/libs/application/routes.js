'use strict';

var routes = {
	baseurl: 'http://app.nexatlas',
	app: '#/app',
	register: '#/register',
	profile: '#/profile',
	login: '#/login',
	lostPassword: '#/lostPassword',
	redefinePassword: '#/redefinePassword',
	ajax: {
		services: {
			api: 'http://api.nexatlas'
		},
		login: '/user/authenticate',
		register: '/user/register'
	},
	get: {
		from: {
			api: function (route) {
				return routes.ajax.services.api + routes.ajax[route];
			}
		}
	}
}

window.routes = routes;

angular.module('nexatlas').config(function ($routeProvider, $locationProvider) {
	$routeProvider.when(routes.app, { template: '<nexatlas-component></nexatlas-component>' });
	$routeProvider.when(routes.register, { template: '<register></register>' });
	$routeProvider.when(routes.login, { template: '<login></login>' });
	$routeProvider.when(routes.profile, { template: '<profile></profile>' });
	$routeProvider.when(routes.lostPassword, { template: '<lost-password></lost-password>' });
	$routeProvider.when(routes.redefinePassword, { template: '<redefine-password></redefine-password>' });

	$locationProvider.html5Mode(true);
});

//Sets crucial URI information based on executing location
switch (window.location.hostname) {
	case 'api.nexatlas':
		routes.baseurl = 'http://app.nexatlas';
		routes.ajax.services.api = 'http://api.nexatlas';
		break;
	default:
		routes.baseurl = 'http://' + window.location.hostname + '';
		routes.ajax.services.api = 'http://api.nexatlas.com';
		break;

}
