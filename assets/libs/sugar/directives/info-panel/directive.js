﻿angular.module("Sugar").directive("infoPanel", [function () {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			tabs: '=?'
		},
		transclude: true,
		link: function (scope, element, attrs) {
			var $tabs = element.find(".tab-scroll");
			scope.fixed = false;
			$(window).on('scroll', function () {
				if (!element.find(".tabs .tab").size()) return;
				var outerHeight = element.outerHeight();

				if (outerHeight - document.body.scrollTop <= $(".app-bar").outerHeight()) {
					scope.$apply(function () { scope.fixed = true; });
				}
				else {
					scope.$apply(function () { scope.fixed = false; });
				}
			});
			scope.$watch('tabs.list.length', function (a, b) {
				setTimeout(function () {
					outerHeight = element.outerHeight();
				}, 0);
			});

			if (!scope.tabs) {
				scope.tabs = { list: [] };
			}
		},
		template: '' +
		'<div class="info-panel" ng-class="{ \'tabs-fixed\' : fixed }" >' +
		'	<div class="content" ng-transclude></div>' +
		'	<app-bar-tabs tabs="tabs" ></app-bar-tabs>' +
		'</div>'
	}
}])