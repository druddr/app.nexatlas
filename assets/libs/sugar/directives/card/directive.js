﻿angular.module("Sugar").directive("card", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		scope: {
			title: '@',
			smallTitle: '@?',
			autoSize: '=?',
			description: '@?',
			image: '@?',
			actions: '=?',
			selectable: '@?',
			icon: '@?',
			cardAction: '&?',
			type: '@?',
		},
		template: '' +
		'<section class="card {{selectable}} {{type}}"' +
		'		 ng-class="{ \'selectable\':selectable, \'auto-size\' : autoSize, \'icon-card\':icon }"' +
		'		 tabindex="0"' +
		'		 on-enter-press="!cardAction || cardAction()"' +
		'		 onkeydown="Sugar.card.events.select(this, event)">' +
		'	<div onclick="Sugar.card.events.select(this.parentNode,event)" ng-click="!cardAction || cardAction()">' +
		'		<div ng-if="title" class="card-title">{{title}}</div>' +
		'		<a ng-if="icon" class="card-icon">' +
		'			<i class="icon material-icons">{{icon}}</i>' +
		'			<label>{{icon.text}}</label>' +
		'		</a>' +
		'		<div ng-if="smallTitle" class="card-small-title">{{smallTitle}}</div>' +
		'		<div ng-transclude="" class="card-description">{{description}}</div>' +
		'		<div ng-if="image" class="card-image">' +
		'			<img src="{{image}}" />' +
		'		</div>' +
		'	</div>' +
		'	<div ng-if="actions" class="actions ">' +
		'		<action ng-if="action.click||action.url" ng-repeat="action in actions" url="{{action.url}}" action="action.click()" on-enter-press="action.click()" label="{{action.label}}"></action>' +
		'	</div>' +
		'</section>',
		link: function (scope, element, attrs, ctrl, transclude) {
			scope.language = $rootScope.language;
		}
	}
}])