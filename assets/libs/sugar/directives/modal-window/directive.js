﻿angular.module("Sugar").directive("modalWindow", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		transclude: true,
		scope: {
			contentId: '@',
			title: '=',
			okAction: '&?',
			okLabelKey: '@?okLabel',
			cancelAction: '&?',
			cancelLabel: '=?',
			searchText: '=?',
			searchMethod: '&?',
			filterSearch: '=?',
			keepNesting: '=?'
		},
		template: '' +
		'<div class="modal" id="{{contentId}}">' +
		'	<div class="modal-box" data-modal-id="{{contentId}}">' +
		'		<div class="modal-title" tabindex="0" role="dialog" data-modal-id="{{contentId}}">' +
		'			{{title}}' +
		'			<div class="modal-close" data-close-modal="{{contentId}}" tabindex="0"></div>' +
		'		</div>' +
		'		<search-box ng-if="filterSearch" search-text="model.searchText" search-method="search"></search-box>' +
		'		<div class="modal-content" ng-transclude>' +
		'			teste' +
		'		</div>' +
		'		<div class="modal-actions right" ng-if="okAction || cancelAction">' +
		'			<button tabindex="0" ng-if="okAction" type="button" class="" data-close-modal="{{contentId}}" ng-click="okAction()">{{okLabel}}</button>' +
		'			<button tabindex="0" ng-if="cancelAction" type="button" class="" data-close-modal="{{contentId}}" ng-click="cancelAction()">{{cancelLabel}}</button>' +
		'		</div>' +
		'	</div>' +
		'	<div class="modal-overlay" data-modal-id="{{contentId}}"></div>' +
		'</div>',
		link: function (scope, element, attr) {
			if (!scope.keepNesting) {
				//To prevent nested Modal Windows
				$("#application-view").append(element);
			}

			scope.language = $rootScope.language;
			scope.okLabelKey = scope.okLabelKey ? scope.okLabelKey : 'ok';
			scope.okLabel = scope.language.instance.labels[scope.okLabelKey];
			scope.cancelLabelKey = scope.cancelLabelKey ? scope.okLabelKey : 'cancelar';
			scope.cancelLabel = scope.language.instance.labels[scope.cancelLabelKey];

			scope.model = {
				searchText: scope.searchText
			}
			scope.filterSearch = scope.searchMethod ? true : scope.filterSearch;

			scope.$watch('model.searchText', function () {
				scope.searchText = scope.model.searchText;
			})
			scope.search = function () {
				scope.searchMethod()(scope.model.searchText);
			}
		}
	}

}]);

angular.module("Sugar").directive("openModal", ['$rootScope', function ($rootScope) {
	return {
		link: function (scope, element) {
			element.on(Sugar.events.click + ' keyup', function (e) {
				if (e.type == 'keyup') {
					var keycode = e.which || e.keyCode || e.charCode;
					if (keycode != 13 && keycode != 32) return;
				}
				var id = this.getAttribute("data-open-modal"),
					$modal = angular.element(document.getElementById(id));
				if (!$modal.length) return;
				$modal.show().addClass("open").css("z-index",$(".modal.open").size()+6001);
				$modal.find('.modal-box[data-modal-id=' + id + ']').animateCss('zoomIn');
				$modal.find('.modal-overlay[data-modal-id=' + id + ']').removeClass('fade-out').addClass('fade-in');
				$modal.find('.modal-title[data-modal-id=' + id + ']').focus();
			});
		}
	}
}]);

angular.module("Sugar").directive("closeModal", ['$rootScope', function ($rootScope) {
	return {
		link: function (scope, element) {
			element.on(Sugar.events.click + ' keyup', function (e) {
				if (e.type == 'keyup') {
					var keycode = e.which || e.keyCode || e.charCode;
					if (keycode != 13 && keycode != 32) return;
				}

				var id = this.getAttribute("data-close-modal"),
					$modal = angular.element(document.getElementById(id));
				$modal.find('.modal-box[data-modal-id=' + id + ']').animateCss("zoomOut", function () {
					$modal.hide().removeClass("open");
				});
				$modal.find('.overlay[data-modal-id=' + id + ']').removeClass('fade-in').addClass('fade-out');
			});
		}
	}
}]);