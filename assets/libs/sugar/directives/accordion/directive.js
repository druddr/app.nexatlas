﻿angular.module("Sugar").directive("accordion", function () {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude: {

		},
		scope: {

		},
		template: '<article class="accordion" ng-transclude role="list"></article>',
		link: function (scope, element, attrs, ctrl, transclude) {

		}
	}
})