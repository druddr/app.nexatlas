﻿angular.module("Sugar").directive("simpleList", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
		},
		transclude: true,
		link: function (scope, element, attrs) {
		},
		template: '' +
		'<div>' +
		'	<ul class="list" ng-transclude>' +
		'	</ul>' +
		'</div>'
	}
}])