﻿angular.module("Sugar").directive("action", ['$location','$rootScope',function ($location,$rootScope) {
	return {
		replace: true,
		transclude: true,
		restrict: 'E',
		transclude:{
			
		},
		scope: {
			action: '&?',
			icon: '@?',
			labelKey: '@?label',
			url: '@?',
			href: '@?url',
			active: '=?'
		},
		template:''+
		'<button class="action" ng-click="action()" ng-class="{ active: active,\'only-icon\':icon && !labelKey }">'+
		'	<i ng-if="icon" class="icon material-icons">{{icon}}</i>'+
		'	<span ng-if="label">{{label}}</span>'+
		'</button>',
		link: function (scope, element, attrs, ctrl, transclude) {
			if (scope.labelKey) {
				var label = $rootScope.language.instance.labels[scope.labelKey];
				
				scope.label = label ? label : scope.labelKey;
				
			}
			if (scope.url) {
				scope.action = function () {
					$location.path(scope.url);
					scope.active = true;
				}
			}
		}
	}
}])