﻿angular.module("Sugar").directive("formSelect", [function () {
	return {
		restrict: 'E',
		replace: true,
		require: 'ngModel',
		scope: {
			label: '@?',
			ngModel: '=',
			name: '@?',
			disabled: '=?',
			type: '@?',
			required: '=?',
			minlength: '=?',
			maxlength: '=?',
			invalidMessage: '@?'
		},
		link: function (scope, element, attr, controller) {
			scope.oController = controller;
			scope.name = scope.name ? scope.name : Sugar.generateGuid();
			element.removeAttr('disabled ng-model type required minlength maxlength name label')
		},
		template:''+
		'<div class="form-field-select form-field-group {{size}}">' +
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{' +
		'				\'disabled\': (disabled),'+
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix || (disabled && ngModel),' +
		'				\'invalid\':oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty' +
		'		   }">{{label}}</label>' +
		'	<label ng-if="label" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>' +
		'	<div class="select-box {{size}} form-control"' +
		'		 data-open-modal="{{uid}}"' +
		'		 ng-disabled="disabled"' +
		'		 ng-required="required"' +
		'		 tabindex="0">' +
		'		<div class="select-text ng-binding">' +
		'			<span ng-if="model.selectedOptions">' +
		'				{{model.selectedOptions}}' +
		'			</span>' +
		'			<span ng-if="!model.selectedOptions">' +
		'				{{placeholder}}' +
		'			</span>' +
		'		</div>' +
		'	</div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage}}' +
		'		</span>' +
		'	</div>' +
		'	<input type="{{type}}"' +
		'		   class="form-control {{size}}"' +
		'		   ng-model="ngModel"' +
		'		   name="{{::name}}"' +
		'		   ng-required="required"' +
		'		   ng-minlength="minlength"' +
		'		   ng-maxlength="maxlength"' +
		'		   ng-disabled="disabled"' +
		'		   autocapitalize="off"' +
		'		   autocorrect="off"' +
		'		   autocomplete="off"' +
		'		   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}' +
		'		</span>' +
		'	</div>' +
		'</div>'

	}
}]);