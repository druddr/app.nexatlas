﻿function inputBase(aExtraInputDirectives) {
	aExtraInputDirectives = aExtraInputDirectives instanceof Array ? aExtraInputDirectives : [];

	var sTemplate = '' +
		'<div class="form-field-input form-field-group {{size}}">' +
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{' +
		'				\'disabled\': (disabled),' +
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix || (disabled && ngModel),' +
		'				\'invalid\':oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty' +
		'		   }">' +
		'		{{label}}' +
		'		<i ng-if="required" class="font-size-1" >*</i>' +
		'	</label>' +
		'	<input type="{{type}}"' +
		'		   class="form-control {{size}}"' +
		'		   ng-model="ngModel"' +
		'		   name="{{::name}}"' +
		'		   ng-required="required"' +
		'		   ng-minlength="minlength"' +
		'		   ng-maxlength="maxlength"' +
		'		   ' + aExtraInputDirectives.join(' ') + 
		'		   ng-disabled="disabled"' +
		'		   autocapitalize="off"' +
		'		   autocorrect="off"' +
		'		   autocomplete="off"' +
		'		   spellcheck="false" />' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<div ng-if="oController.$$parentForm[name].$invalid && oController.$$parentForm[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : ("messages.formFieldInvalid"|translate)}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.required && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldRequiredInvalid"|translate)}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.minlength && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldMinLengthInvalid"|translate)}} {{minlength}}' +
		'		</div>' +
		'		<div ng-if="oController.$$parentForm[name].$error.maxlength && oController.$$parentForm[name].$dirty">' +
		'			{{("messages.formFieldMaxLengthInvalid"|translate)}} {{maxlength}}' +
		'		</div>' +
		'	</div>' +
		'</div>';


	return {
		restrict: 'E',
		replace: true,
		require: 'ngModel',
		scope: {
			label: '@?',
			ngModel: '=',
			name: '@?',
			disabled: '=?',
			type: '@?',
			required: '=?',
			minlength: '=?',
			maxlength: '=?',
			invalidMessage: '@?'
		},
		link: function(scope, element, attr, controller) {
			scope.oController = controller;
			scope.name = scope.name ? scope.name : Sugar.generateGuid();
			element.removeAttr('disabled ng-model type required minlength maxlength name label')
		},
		template: sTemplate

	}
}