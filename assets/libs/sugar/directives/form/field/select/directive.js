﻿angular.module("Sugar").directive("formFieldSelect", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			ngModel: '=',
			options: '=',
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			required: '=?',
			disabled: '=?',
			idProperty: '@?',
			size: '@?',
			type: '@?',
			optionsLabel: '@',
			labelFromLanguage: '@?',
			searchMethod: '&?'
		},
		link: function (scope, element, attrs, ctrl, transclude) {
			element.removeAttr('ng-required ng-model ng-disabled label placeholder model required disabled options options-label id-property');
			element.find('.select-box').removeAttr('ng-required ng-model ng-disabled');
		},
		controller: ['$scope', function (scope) {
			scope.model = {
				ngModel: scope.ngModel ? scope.ngModel : null,
				selectedOptions: '',
				searchText: ''
			};

			scope.uid = Sugar.generateGuid();
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';
			scope.labelFromLanguage = scope.labelFromLanguage == 'true';

			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}

			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}

			scope.header = scope.label ? scope.label : scope.placeholder;
			scope.type = scope.type ? scope.type : 'single-select';
			scope.search = function () {
				if (!scope.searchMethod) return;
				scope.searchMethod()(scope.model.searchText, function (result) {

				});
			}

			var doNotUpdateNgModel = false;
			scope.$watch('ngModel', function () {
				if (!scope.ngModel) return;
				scope.model.ngModel = scope.ngModel;
			});

			scope.$watch('model.ngModel', function () {
				if (doNotUpdateNgModel) {
					doNotUpdateNgModel = false;
					return;
				}
				scope.ngModel = scope.model.ngModel;
			});

			scope.$watch('model.selectedOptions', function () {
				scope.selectedOptions = scope.model.selectedOptions
			});

			var labelPropertyTypes = scope.optionsLabel.split(';');
			scope.titleProperty = labelPropertyTypes[0];
			scope.subtitleProperty = labelPropertyTypes[1] ? labelPropertyTypes[1] : '';
			scope.descriptionProperty = labelPropertyTypes[2] ? labelPropertyTypes[2] : '';
		}],
		//templateUrl: settings.path.components + 'form/field/select/template.html',
		template: ' ' +
		'<div class="form-field-select form-field-group">' +
		'	<label ng-if="label" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty,\'disabled\': (disabled)}">{{label}}</label>' +
		'	<div class="select-box {{size}} form-control"' +
		'		 data-open-modal="{{uid}}"' +
		'		 ng-disabled="disabled"' +
		'		 ng-required="required"' +
		'		 tabindex="0">' +
		'		<div class="select-text ng-binding">' +
		'			<span ng-if="model.selectedOptions">' +
		'				{{model.selectedOptions}}' +
		'			</span>' +
		'			<span ng-if="!model.selectedOptions">' +
		'				{{placeholder}}' +
		'			</span>' +
		'		</div>' +
		'	</div>' +
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>' +
		'	<div class="input-error-message">' +
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage}}' +
		'		</span>' +
		'	</div>' +
		'	<modal-window content-id="{{uid}}"' +
		'				  title="header"' +
		'				  class="form-select-window"' +
		'				  search-method="search"' +
		'				  search-text="model.searchText"' +
		'				  ok-action="select"' +
		'				  ok-label="selecionar">' +
		'			<list items="options"' +
		'				  id-property="{{idProperty}}"' +
		'				  title-property="{{titleProperty}}"' +
		'				  subtitle-property="{{subtitleProperty}}"' +
		'				  description-property="{{descriptionProperty}}"' +
		'				  selectable="true"' +
		'				  selectable-type="{{type}}"' +
		'				  search-text="searchText"' +
		'				  label-from-language="{{labelFromLanguage}}"' +
		'				  ng-model="model.ngModel"' +
		'				  selected-options="model.selectedOptions">' +
		'			</list>' +
		'	</modal-window>' +
		'</div>'
	}

}])