﻿function validarCPF(str) {
	if (!str) return false;
	str = str.replace('.', '');
	str = str.replace('.', '');
	str = str.replace('-', '');
	var Soma;
	var Resto;
	Soma = 0;
	if (str == "00000000000") return false;
	for (i = 1; i <= 9; i++) {
		Soma = Soma + parseInt(str.substring(i - 1, i)) * (11 - i);
	}
	Resto = (Soma * 10) % 11;
	if ((Resto == 10) || (Resto == 11)) {
		Resto = 0;
	}
	if (Resto != parseInt(str.substring(9, 10))) return false;

	Soma = 0;
	for (i = 1; i <= 10; i++) {
		Soma = Soma + parseInt(str.substring(i - 1, i)) * (12 - i);
	}
	Resto = (Soma * 10) % 11;
	if ((Resto == 10) || (Resto == 11)) {
		Resto = 0;
	}
	if (Resto != parseInt(str.substring(10, 11))) return false;
	return true;

}
function validarCNPJ(cnpj) {

	cnpj = cnpj.replace(/[^\d]+/g, '');

	if (cnpj == '') return false;

	if (cnpj.length != 14)
		return false;

	// LINHA 10 - Elimina CNPJs invalidos conhecidos
	if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
		return false; // LINHA 21

	// Valida DVs LINHA 23 -
	tamanho = cnpj.length - 2
	numeros = cnpj.substring(0, tamanho);
	digitos = cnpj.substring(tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(0))
		return false;

	tamanho = tamanho + 1;
	numeros = cnpj.substring(0, tamanho);
	soma = 0;
	pos = tamanho - 7;
	for (i = tamanho; i >= 1; i--) {
		soma += numeros.charAt(tamanho - i) * pos--;
		if (pos < 2)
			pos = 9;
	}
	resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
	if (resultado != digitos.charAt(1))
		return false; // LINHA 49

	return true; // LINHA 51

}
angular.module("Sugar").directive("formFieldInput", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			labelKey: '@?label',
			placeholderKey: '@?placeholder',
			model: '=',
			required: '=?',
			disabled: '=?',
			maxlength: '=?',
			minlength: '=?',
			invalidMessage: '=?',
			size: '@?',
			type: '@?',
			prefix: '@?',
			sufix: '@?',
			name: '@?',
			form: '=?',
			id: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.language = $rootScope.language;
			scope.label = '';
			scope.placeholder = '';

			if (scope.labelKey) {
				scope.label = scope.language.instance.labels[scope.labelKey];
			}

			if (scope.placeholderKey) {
				scope.placeholder = scope.language.instance.labels[scope.placeholderKey];
			}

			var input = element.find('input');
			scope.noPlaceholder = attrs.hasOwnProperty('noPlaceholder');
			scope.uid = scope.id || Sugar.generateGuid();
			scope.pattern;
			var type = scope.type ? scope.type : 'text';
			scope.inputType = scope.type;

			function checkInputType() {
				var type = scope.type ? scope.type : 'text';
				scope.inputType = scope.type;
				switch (type) {
					case 'text':
						scope.inputType = 'text';
						break;
					case 'time':
					case 'date':
						scope.inputType = 'text';
						break;
					case 'cpf':
						scope.inputType = 'text';
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.cpf();
							$(this).trigger('input');
						});
						break;
					case 'cnpj':
						scope.inputType = 'text';
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.cnpj();
							$(this).trigger('input');
						});
						break;
					default:
						if (scope.mask) break;
						input.on('click', function (e) {
							if (!e.ctrlKey) return;
							this.value = Sugar.valueGenerator.name();
							$(this).trigger('input');
						});
						break;
				}

			}

			checkInputType();

			scope.$watch('type', function () {
				checkInputType();
			});

			if (!scope.placeholder) {
				scope.placeholder = scope.label;
			}
			if (scope.noPlaceholder) {
				scope.placeholder = '';
			}

			element.removeAttr('label placeholder model required disabled maxlength minlength invalid-message size type mask name');
			input.removeAttr('ng-required ng-model ng-disabled ng-maxlength ng-minlength ng-pattern');


			scope.$watch('model', function () {
				var oLabel = element.find('label');
				if (scope.model) {
					oLabel.addClass('focused');
				}
				else {
					if (document.activeElement == input.get(0) || oLabel.hasClass('always-focused')) return;
					oLabel.removeClass('focused');
				}
			})
		},
		template:''+
		'<div class="form-field-input form-field-group {{size}} input-type-{{inputType}}">'+
		'	<label ng-if="label" for="input-{{uid}}" ng-class="{'+
		'				\'focused always-focused\':(type && type != \'text\' && type != \'password\') || prefix || sufix ,'+
		'				\'invalid\': form[name].$invalid && form[name].$dirty,'+
		'				\'disabled\': (disabled)'+
		'		   }">{{label}}</label>'+
		'	<!--placeholder="{{placeholder}}"-->'+
		'	<input type="{{inputType}}"'+
		'		   class="form-control {{size}}"'+
		'		   ng-model="model"'+
		'		   name="{{::name}}"'+
		'		   id="input-{{uid}}"'+
		'		   ng-required="required"'+
		'		   ng-minlength="minlength"'+
		'		   ng-maxlength="maxlength"'+
		'		   ng-disabled="disabled"'+
		'		   virtual-mask="{{type}}"'+
		'		   virtual-mask-sufix="{{sufix}}"'+
		'		   virtual-mask-prefix="{{prefix}}"'+
		'		   virtual-mask-precision="{{precision}}"'+
		'		   language="language"'+
		'		   autocapitalize="off"'+
		'		   autocorrect="off"'+
		'		   autocomplete="off"'+
		'		   spellcheck="false" />'+
		'	<div class="input-border-bottom" ng-class="{\'invalid\': form[name].$invalid && form[name].$dirty}"></div>'+
		'	<div class="input-error-message">'+
		'		<span ng-if="form[name].$invalid && form[name].$dirty">' +
		'			{{invalidMessage ? invalidMessage : language.instance.phrases[\'por-favor-verifique-campo\']}}'+
		'		</span>'+
		'	</div>'+
		'</div>'
	}
}]);

angular.module("Sugar").directive('virtualMask', ['$rootScope',function ($rootScope) {
	return {
		restrict: 'A',
		require: 'ngModel',
		scope: {
			type: '@?virtualMask',
			prefix: '@?virtualMaskPrefix',
			sufix: '@?virtualMaskSufix',
			precision: '@?virtualMaskPrecision',
			placeholder: '@?',
		},
		link: function (scope, element, attrs, controller) {
			scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
			scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
			scope.language = $rootScope.language;
			function formatNumeric(value, precision, config) {
				value = isNaN(parseFloat(value)) ? 0 : parseFloat(value);
				value = (number_format(value, precision, config.decimal_point, config.thousand_point));
				if (value[0] == '.') {
					value = '0' + value;
				}
				return value;
			}
			function unformatNumeric(value, precision, config) {
				//10.00
				var zeroBeforeFix = precision;
				while (zeroBeforeFix--) {
					value = '0' + value;
				}
				value = value.replace(new RegExp("\\" + config.thousand_point, 'g'), '').replace(new RegExp("\\" + config.decimal_point, 'g'), '');
				value = value.substr(0, value.length - precision) + '.' + value.substr(-precision);
				if (value[0] == '.') {
					value = '0' + value;
				}
				value = !precision ? parseInt(value) : parseFloat(value);
				return value;
			}
			var aSkipChars = [];
			function skipChar(sChar) {
				return aSkipChars.indexOf(sChar);
			}
			function mask(value) {
				if (value == undefined || value == null) {
					return undefined;
				}

				scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var precision = scope.precision ? scope.precision : 2;
				var config = scope.language.instance.defaults.number;
				var value = unmask(value);
				switch (scope.type) {
					case 'integer':
						precision = 0;
						value = formatNumeric(value, precision, config);
						break;
					case 'decimal':
						value = formatNumeric(value, precision, config);
						aSkipChars = ['.', ','];

						break;
					case 'cpf':
						value = value.toString();
						value = str_pad(value.substr(0, 3), 3, "_") +
							'.' +
							str_pad(value.substr(3, 3), 3, "_") +
							'.' +
							str_pad(value.substr(6, 3), 3, "_") +
							'-' +
							str_pad(value.substr(9, 2), 2, "_");
						aSkipChars = ['.', '-'];
						controller.$setValidity('CPF', validarCPF(value));
						break;
					case 'cnpj':
						value = value.toString();
						value = str_pad(value.substr(0, 2), 2, "_") +
							'.' +
							str_pad(value.substr(2, 3), 3, "_") +
							'.' +
							str_pad(value.substr(5, 3), 3, "_") +
							'/' +
							str_pad(value.substr(8, 4), 4, "_") +
							'-' +
							str_pad(value.substr(12, 2), 2, "_");
						aSkipChars = ['.', '-', '/'];
						controller.$setValidity('CNPJ', validarCNPJ(value));
						break;
					case 'date':
						var format = scope.language.instance.defaults.datetime.format.toLowerCase();
						var divider = scope.language.instance.defaults.datetime.placeholder.toLowerCase().replace(/(d|m|y)/gi, '')[0];
						var re = new RegExp(divider, "gi");
						var day = value.substr(8, 2);
						var month = value.substr(5, 2);
						var year = value.substr(0, 4);


						value = "";
						value = value.toString();
						value = format.replace("d", str_pad(day, 2, "_"))
									  .replace("m", str_pad(month, 2, "_"))
									  .replace("y", str_pad(year, 4, "_"));
						aSkipChars = [divider];
						break;
					case 'time':
						var second_part_index = value.indexOf(":");
						value = str_pad(value.substr(0, 2), 2, "0") + ":" + str_pad(value.substr(second_part_index + 1, 2), 2, "0");
						aSkipChars = [':'];
						break;
				}

				return scope.prefix + value + scope.sufix;
			}
			function unmask(value) {
				if (value == undefined || value == null) {
					return undefined;
				}
				scope.prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				scope.sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var precision = scope.precision ? scope.precision : 2;
				var config = scope.language.instance.defaults.number;

				if (value.indexOf && value.indexOf(scope.prefix) > -1) {
					value = value.substr(scope.prefix.length);
				}
				if (value.indexOf && value.indexOf(scope.sufix) > -1) {
					value = value.substr(0, value.length - scope.sufix.length);
				}
				switch (scope.type) {
					case 'integer':
						precision = 0;
						value = unformatNumeric(value, precision, config);
						break;
					case 'decimal':
						value = unformatNumeric(value, precision, config);
						break;
					case 'cnpj':
					case 'cpf':
						value = value.replace(/(\.|\-|\/|\_)/gi, '');
						break;
					case 'date':
						var format = scope.language.instance.defaults.datetime.placeholder.toLowerCase();
						var divider = format.replace(/(d|m|y)/gi, '')[0];
						var dayIndex = format.indexOf("d");
						var monthIndex = format.indexOf("m");
						var yearIndex = format.indexOf("y");
						var re = new RegExp(divider, "gi");

						if (value.indexOf("-") != 4) {
							value = str_pad(value.substr(yearIndex, 4).replace(re, ''), 4, "_") +
								"-" +
								str_pad(value.substr(monthIndex, 2).replace(re, ''), 2, "_") +
								"-" +
								str_pad(value.substr(dayIndex, 2).replace(re, ''), 2, "_");
						}
						break;
					case 'time':
						value = value;
						break;
				}

				return value;
			}
			var last_state = false;
			var char = getCaretPosition(element.get(0));

			element.on("keydown", function (e) {
				//if (scope.type != 'time') return;
				char = element.caret();
				last_state = element.val();

				var keycode = e.which | e.keyCode | e.charCode;
				if (keycode == 8) {
					char = char - 2;
				}
				if (keycode == 46) {
					char = char - 1;
				}
			});
			element.on("paste", function (event) {
				char = char + event.originalEvent.clipboardData.getData('text').length - 1;
			});
			controller.$formatters.push(function (value) {
				switch (scope.type) {
					case 'decimal':
					case 'integer':
						//When model comes from server sometimes i will come as an integer
						//and the masking system for decimal expects a float 123.45
						//so we provide a float with .00 at the end if this is the case
						var float = parseFloat(value);
						if (float % 1 === 0 && value.toString().indexOf('.') == -1) {
							value = value.toString() + (scope.type == 'integer' ? '' : '.00');
						}
						if (value == undefined || value == null) {
							value = '0';
						}
						break;
					case 'date':
					case 'time':
					case 'cpf':
					case 'cnpj':
						if (!value) {
							value = '';
						}
				}
				var parsed = mask(value);
				controller.$viewValue = (parsed);
				controller.$render();
				return parsed;
			});
			controller.$parsers.push(function (value) {
				var parsed = mask(value);
				controller.$viewValue = (parsed);
				controller.$render();

				var position = getCaretPosition(element);
				var prefix = scope.prefix ? scope.prefix.trim() + ' ' : '';
				var sufix = scope.sufix ? ' ' + scope.sufix.trim() : '';
				var caret = controller.$viewValue.length - sufix.length;

				char = char + 1;

				if (prefix.length && char < prefix.length + 1) {
					char = prefix.length;
				}
				if (sufix.length && char > parsed.length - sufix.length) {
					char = parsed.length - sufix.length;
				}
				while (char <= parsed.length && skipChar(parsed[char]) > -1) {
					console.log(skipChar(parsed[char]) > -1, parsed[char], aSkipChars, char);
					char = char + 1;
				}

				element.caret(char);

				return unmask(value);
			});
		}
	}
}])

var lastContentLength = 0;
function fixCaretPosition(ngModel, position, element, prefix, sufix, keycode) {
	var change = false;
	var content = element.value.length;
	var currentPosition = position;
	var prefixLenth = prefix ? (prefix.trim().length + 1) : 0;
	var sufixLength = sufix ? (sufix.trim().length + 1) : 0;
	var total = content - prefixLenth - sufixLength;
	switch (keycode) {
		case 8://BACKSPACE
			if (currentPosition == (content - sufixLength - 1)) {
				position = position + 1;
				setCaretPosition(element.firstChild, position, position);
				return position;
			}
			break;
		case 37://LEFT
			currentPosition = currentPosition - 1;
			break;
		case 39://RIGHT
			currentPosition = currentPosition + 1;
			break;
		case 46://DELETE
			content = content - 1;
			break;
		default:
			if (lastContentLength - content > 1) {
				currentPosition = currentPosition - 1;
			}
			else if (lastContentLength - content < -1) {
				currentPosition = currentPosition + 1;
			}
			else {
				currentPosition = currentPosition;
			}
			break;
	}
	if (prefix) {
		if (currentPosition < (prefixLenth) && content > 0) {
			currentPosition = prefixLenth + 1;
			change = true;
		}
	}
	if (sufix) {
		if (currentPosition > (content - sufixLength) && content > 0) {
			currentPosition = content - sufixLength;
			change = true;
		}
		if (currentPosition == (content - sufixLength)) {
			currentPosition = content - sufixLength;
			change = true;
		}
	}
	if (change && currentPosition >= 0) {
		setCaretPosition(element.firstChild, currentPosition, currentPosition);
	}
	lastContentLength = content;
	position = currentPosition;
	return position;

}
function getCaretPosition(control) {
	var caretPos = 0,
	  sel, range;
	if (window.getSelection) {
		sel = window.getSelection();
		if (sel.rangeCount) {
			range = sel.getRangeAt(0);
			if (range.commonAncestorContainer.parentNode == control) {
				caretPos = range.endOffset;
			}
		}
	} else if (document.selection && document.selection.createRange) {
		range = document.selection.createRange();
		if (range.parentElement() == control) {
			var tempEl = document.createElement("span");
			control.insertBefore(tempEl, control.firstChild);
			var tempRange = range.duplicate();
			tempRange.moveToElementText(tempEl);
			tempRange.setEndPoint("EndToEnd", range);
			caretPos = tempRange.text.length;
		}
	}
	return caretPos;
}
function setCaretPosition(elem, caretPos) {
	if (elem != null) {
		if (elem.createTextRange) {
			var range = elem.createTextRange();
			range.move('character', caretPos);
			range.select();
		}
		else {
			if (elem.selectionStart) {
				elem.focus();
				elem.setSelectionRange(caretPos, caretPos);
			}
			else
				elem.focus();
		}
	}
}
var stripTags = function (input, allowed) {
	//  discuss at: http://phpjs.org/functions/strip_tags/
	allowed = (((allowed || '') + '')
	  .toLowerCase()
	  .match(/<[a-z][a-z0-9]*>/g) || [])
	  .join('');
	var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
	  commentsAndTags = /<!--[\s\S]*?-->/gi;
	return input.replace(commentsAndTags, '').replace(tags, function ($0, $1) {
		return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
	});
}