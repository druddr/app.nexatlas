﻿angular.module("Sugar").directive("searchBox", ['$rootScope', function ($rootScope) {
	return {
		replace: true,
		restrict: 'E',
		scope: {
			searchMethod: '&',
			placeholder: '@?',
			searchText: '=',
			fixed: '=?'
		},
		template: '' +
		'<div class="search-box">' +
		'	<button class="icon material-icons toggle-menu" onclick="Sugar.sidebar.toggle()">menu</button>' +
		'	<input type="text" placeholder="{{searchPlaceholder}}" ng-model="searchText" />' +
		'	<button class="search-button" ng-click="search()">' +
		'		<i class="icon material-icons">search</i>' +
		'	</button>' +
		'</div>',
		link: function (scope, element) {
			scope.language = $rootScope.language;
			scope.model = {
				searchText: (scope.searchText)
			}
			scope.searchPlaceholder = scope.placeholder ? scope.language.instance.labels[scope.placeholder] : scope.language.instance.labels.procurar;
			scope.$watch('model.searchText', function () {
				scope.searchText = scope.model.searchText;
			});

			scope.search = function () {
				if (!scope.searchMethod || typeof scope.searchMethod() != "function") return;
				scope.searchMethod()();
			}

			if (scope.fixed) {
				element.addClass("fixed");
				setTimeout(function(){
					$rootScope.header.hide();
				},1)
			}
		}
	}

}])