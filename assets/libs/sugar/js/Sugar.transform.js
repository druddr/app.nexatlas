﻿Sugar.transform = {

	translate: function (element, x, y, z) {
		x = x ? x : 0;
		y = y ? y : 0;
		z = z ? z : 0;
		element.css("-webkit-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-moz-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-ms-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-o-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
	},
	rotate: function (element, x, y, z, degrees) {
		x = x ? x : 0;
		y = y ? y : 0;
		z = z ? z : 0;
		degrees = degrees ? degrees : 0;
		element.css("-webkit-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-moz-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-ms-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-o-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
	}
}