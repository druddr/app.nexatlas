﻿Sugar.calendar = {
	month: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
	week: ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
	create: function (year, month, day) {
		day = parseInt(day);
		month = parseInt(month);
		year = parseInt(year);

		var initial_time = strtotime(year + "-" + month + "-" + str_pad(day, 2, '0', 'STR_PAD_LEFT'));
		var prev_time = strtotime("-1 month", initial_time);
		var next_time = strtotime("+1 month", initial_time);
		var month_first_day_time = strtotime(year + "-" + month + "-01");
		var month_total_days = parseInt(date("t", initial_time));
		var prev_month_total_days = parseInt(date("t", prev_time));
		var next_month_total_days = parseInt(date("t", next_time));
		var first_day_week_month = parseInt(date("N", month_first_day_time));
		var total_days_before_first = 7 - first_day_week_month;


		var calendar = document.createElement("div");
		calendar.className = "sugar-calendar";
		var divday = document.createElement("div");
		divday.setAttribute("data-calendar-day", "");
		divday.setAttribute("data-calendar-month", "");
		divday.setAttribute("data-calendar-year", "");
		divday.className = "day";

		for (var i = 0; i < Sugar.calendar.week.length; i++) {
			var d = document.createElement("div");
			d.className = "week-day";
			d.innerHTML = Sugar.calendar.week[i];
			calendar.appendChild(d);
		}

		var a = 1, b = first_day_week_month - 1, c = 1, d = prev_month_total_days - first_day_week_month, e = 1, f, g, h, j;
		for (var i = 1; i <= 42; i++) {
			g = (i <= first_day_week_month && i <= 7);
			a = g ? ++d : c++;
			h = (a > month_total_days && i > 27);
			a = h ? e++ : a;
			j = g ? prev_time : (h ? next_time : initial_time);
			f = divday.cloneNode(true);
			f.innerHTML = a;

			f.className = a == day && (date("Ymd", j) == date("Ymd", strtotime(year + "-" + str_pad(month, 2, "0", 'STR_PAD_LEFT') + "-" + str_pad(day, 2, '0', 'STR_PAD_LEFT')))) ? "day current" : "day";
			f.setAttribute("data-calendar-day", a);
			f.setAttribute("data-calendar-month", date("m", j));
			f.setAttribute("data-calendar-year", date("Y", j));
			calendar.appendChild(f);



		}
		return calendar;
	}
};
