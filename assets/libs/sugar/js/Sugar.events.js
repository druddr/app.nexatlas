﻿Sugar.events = {
	click: Sugar.IsMobileOrTablet ? 'click' : 'click',
	touchstart: Sugar.IsMobileOrTablet ? 'touchstart' : 'mousedown',
	touchend: Sugar.IsMobileOrTablet ? 'touchend' : 'mouseup',
	touchmove: Sugar.IsMobileOrTablet ? 'touchmove' : 'mousemove',
	touchcancel: Sugar.IsMobileOrTablet ? 'touchcancel' : 'mouseleave',
	contextmenu: Sugar.IsMobileOrTablet ? 'contextmenu' : 'contextmenu'
};
Sugar.preventBackButton = false;
Sugar._backButtonActions=[];
Sugar.addBackButtonAction = function (fnBack) {
	Sugar._backButtonActions.push(fnBack);
}
Sugar.removeBackButtonAction = function (fnBack) {
	Sugar._backButtonActions.slice().forEach(function (fnTest,iIndex) {
		if (fnBack === fnTest) {
			Sugar._backButtonActions.splice(iIndex, 1);
		}
	});
}
Sugar.getBackButtonActions = function () {
	return Sugar._backButtonActions;
}