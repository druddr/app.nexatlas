﻿Sugar.sidebar = {
	all: null,
	element: null,
	blocker: null,
	isopening: false,
	isodragging: false,
	settings: {
		width: 300,
	},
	init: function (sidebar_element) {
		Sugar.sidebar.reset();
		//This event is responsable for the drag and drop left menu functionality
		angular.element(document).on(Sugar.events.touchmove, function (e) {
			if (document.querySelectorAll(".modal.fade-in").length > 0) return;
			Sugar.sidebar.isdragging = false;
			//Somente irá abrir o menu quando pegar bem do cantinho esquerdo da tela
			if (Sugar.touch.direction.x == "R" && Sugar.touch.position.x.initial <= 15) {
				e.preventDefault();
				Sugar.sidebar.translate(Sugar.touch.position.x.current);
				Sugar.sidebar.isopening = true;
				Sugar.sidebar.isdragging = true;
				return;
			}
			if (Sugar.touch.direction.x == "L" /*&& Sugar.touch.position.x.initial >= Sugar.sidebar.settings.width*/ && !Sugar.sidebar.get().hasClass("closed")) {
				e.preventDefault();
				if (Sugar.touch.position.x.current > Sugar.touch.position.x.initial) return;
				var translate = Sugar.sidebar.settings.width - Math.abs(Sugar.touch.position.x.initial - Sugar.touch.position.x.current);
				Sugar.sidebar.translate(translate);
				Sugar.sidebar.isopening = false;
				Sugar.sidebar.isdragging = true;
				return;
			}
		});

		//this event will decide if sidebar will close or open
		angular.element(document).on(Sugar.events.touchend, function (e) {
			if (!Sugar.sidebar.isdragging) return;
			Sugar.sidebar.isdragging = false;
			if (Sugar.sidebar.isopening) {
				Sugar.sidebar.show();
			}
			else {
				Sugar.sidebar.hide();
			}
		});
		//this event will hide sidebar
		//everytime that a link, inside of it, is clicked
		angular.element(document).on(Sugar.events.click, "#sidebar a", function (e) {
			Sugar.sidebar.hide();
		});
		
		//When drawer-blocker is clicked it hides sidebar
		angular.element(document.querySelector(".nav-drawer-blocker")).on(Sugar.events.click, Sugar.sidebar.hide);

		//ng-child-active means that this element has submenus
		//it will open this submenus
		angular.element(document).on(Sugar.events.click, ".contains-child", function (e) {
			var navsub = angular.element(this).parent().find(".nav-sub");
			Sugar.sidebar.openSub(navsub);
		});

		//When the arrow up submenu in sidebar is clicked it closes submenu
		angular.element(document).on(Sugar.events.click, ".ng-sidebar-back", function () {
			Sugar.sidebar.closeSub(angular.element(this).parent().parent())
		});
		angular.element(document).ready(function () {
			if (Sugar.sidebar.isPinned()) {
				Sugar.sidebar.pin();
			}
		});
	},
	reset: function () {
		Sugar.sidebar.all = angular.element(document.querySelectorAll(".toggle-sidebar,#sidebar,.nav-drawer-blocker"));
		Sugar.sidebar.element = angular.element(document.querySelectorAll("#sidebar"));
		Sugar.sidebar.blocker = angular.element(document.querySelectorAll(".nav-drawer-blocker"));
	},
	get: function (all) {
		if (Sugar.sidebar.all == null) Sugar.sidebar.all = angular.element(document.querySelectorAll(".toggle-sidebar,#sidebar,.nav-drawer-blocker"));
		if (Sugar.sidebar.element == null) Sugar.sidebar.element = angular.element(document.querySelectorAll("#sidebar"));
		if (Sugar.sidebar.blocker == null) Sugar.sidebar.blocker = angular.element(document.querySelectorAll(".nav-drawer-blocker"));
		return all ? Sugar.sidebar.all : Sugar.sidebar.element;
	},
	getToggler: function () {
		return angular.element(document.querySelectorAll(".toggle-sidebar"));
	},
	isOpen: function () {
		return Sugar.sidebar.get().hasClass("open");
	},
	translate: function (translate, element,iTopTranslateToKeep) {
		var sidebar = element == undefined ? Sugar.sidebar.get() : element, translate = translate > Sugar.sidebar.settings.width ? Sugar.sidebar.settings.width : translate;
		sidebar.removeClass("open").removeClass("closed").addClass("dragging-sidebar");
		
		Sugar.transform.translate(sidebar, translate, iTopTranslateToKeep);

		if (element) return;
		var opacity = (translate / Sugar.sidebar.settings.width) / 2;
		opacity = opacity > 0.2 ? opacity : 0.2;
		var visibility = opacity == 0 ? 'hidden' : 'visible';
		Sugar.sidebar.blocker.removeClass("open").removeClass("closed").removeClass("dragging-sidebar").addClass("menu-sliding").css("opacity", opacity).css("visibility", visibility).addClass("dragging-sidebar");
	},
	show: function () {
		Sugar.sidebar.get().removeAttr("style");
		Sugar.sidebar.get(true).addClass("open").removeClass("closed").removeClass("dragging-sidebar");
		Sugar.sidebar.get().find(".ng-item-name").addClass("ng-child-active");
	},
	hide: function () {
		Sugar.sidebar.get().removeAttr("style");
		Sugar.sidebar.get(true).addClass("closed").removeClass("open").removeClass("dragging-sidebar")[0].scrollTop = 0;
		Sugar.sidebar.closeSub();
		Sugar.sidebar.get().find("ul:first .profile").height("");
	},
	toggle: function () {
		if (Sugar.sidebar.isOpen()) {
			Sugar.sidebar.hide();
			return;
		}
		Sugar.sidebar.show();
	},
	isPinned: function () {
		return localStorage.getItem("sidebar-pinned") == '1';
	},
	pin: function () {
		localStorage.setItem("sidebar-pinned", '1');
		Sugar.sidebar.get(true).addClass("pinned");
		angular.element(document.querySelectorAll("#main, .app-bar, .panel")).addClass("sidebar-pinned");
		var tabs = angular.element(document.querySelectorAll(".tabs li[data-tab-id].active"));
		if (tabs.length > 0) {
			tabs.trigger(Sugar.events.touchstart);
		}
	},
	unpin: function () {
		console.log('here',localStorage.getItem("sidebar-pinned"));
		localStorage.setItem("sidebar-pinned", '0');
		Sugar.sidebar.get(true).removeClass("pinned");
		angular.element(document.querySelectorAll("#main, .app-bar, .panel")).removeClass("sidebar-pinned");
		var tabs = angular.element(document.querySelectorAll(".tabs li[data-tab-id].active"));
		if (tabs.length > 0) {
			tabs.trigger(Sugar.events.touchstart);
		}
	},
	togglePin: function () {
		if (Sugar.sidebar.isPinned()) {
			Sugar.sidebar.unpin();
			return;
		}
		Sugar.sidebar.pin();
	},
	showSubmenu: function () {

	},
	events: function () {
		if (window.innerWidth >= 768) {
			Sugar.sidebar.show();
		}
		else {
			Sugar.sidebar.hide();
		}
	},
	openSub: function (submenu) {
		var navsubs = document.querySelectorAll(".nav-sub"),
			nestingLevel = submenu.attr('data-nesting-level');
		if (navsubs.length == 0) return;
		angular.element(navsubs).not(submenu.parent().parent().parent().get(0)).hide();
		submenu.show();
		Sugar.sidebar.get()[0].scrollTop = 0;
		var oFirst = Sugar.sidebar.get().find("ul:first");
		Sugar.sidebar.translate(Sugar.sidebar.settings.width * -1 * nestingLevel, oFirst);
	},
	closeSub: function (submenu) {
		var navsubs = document.querySelectorAll(".nav-sub");
		if (navsubs.length == 0) return;
		if (!submenu) {
			angular.element(navsubs).hide();
			Sugar.sidebar.translate(0, Sugar.sidebar.get().find("ul:first"));
			return;
		}
		var nestingLevel = submenu.attr('data-nesting-level')
		Sugar.sidebar.get()[0].scrollTop = 0;
		var translateback = Sugar.sidebar.settings.width - (Sugar.sidebar.settings.width * (nestingLevel ));
		Sugar.sidebar.translate(translateback, Sugar.sidebar.get().find("ul:first"));
		setTimeout(function () {
			angular.element(navsubs).not(submenu.parent().parent().parent().get(0)).hide();
		}, 250);
	}
}
