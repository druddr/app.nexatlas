// import node modules
var gulp     = require('gulp');
var $        = require('gulp-load-plugins')();
var gulpSync = require('gulp-sync')(gulp);
var bs       = require('browser-sync').create();



////////////
// TASK's //
////////////

gulp.task('default', gulpSync.sync(['build', 'watch']));

gulp.task('build', gulpSync.sync(['scss', 'inject']));



/////////////////
// DEVELOPMENT //
/////////////////

gulp.task('server', function() {
  return bs.init({
    server: {
      baseDir: './app'
    },
    browser: []
  });
});

gulp.task('watch', function() {
  gulp.watch(['assets/scss/**/*.scss'], ['scss']);
  gulp.watch(['**/*.html']).on('change', bs.reload);
});



////////////
// INJECT //
////////////

gulp.task('inject', function() {
  var opt = { read: false };
  var lib = gulp.src([ 'assets/libs/*.css',], opt);
  var app = gulp.src([ 'assets/css/*.css', ], opt);

  return gulp.src('index.html')
    .pipe($.inject(lib, {
      relative: true,
      starttag: '<!-- inject:lib:{{ext}} -->'
    }))
    .pipe($.inject(app, {
      relative: true,
      starttag: '<!-- inject:app:{{ext}} -->'
    }))
    .pipe(gulp.dest('app'));
});



/////////////
// COMPILE //
/////////////


gulp.task('scss', function() {
  return gulp.src('assets/scss/**/*.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass().on('error', $.sass.logError))
    .pipe($.autoprefixer())
    // .pipe($.cleancss())
    // .pipe($.rename({ extname: ".min.css" }))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('css'))
    .pipe(bs.stream());
});
