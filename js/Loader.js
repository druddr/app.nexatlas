﻿angular.module('System').directive("loader", ['$rootScope', '$route', function ($rootScope, $route) {
	return function ($scope, element, attrs) {
		$rootScope.$on("$locationChangeStart", function () {
			//$("#main").hide();
			//$("#application-view").html(" ");
			element.show().css('opacity', .5).removeClass("manual").addClass("resolving");
		});
		$rootScope.$on("axis:resolve:default_components:load", function () {
			//element.show().css('opacity', 1).removeClass("manual").addClass("resolving");
		});
		$rootScope.$on("$viewContentLoaded", function () {
			$("#main").fadeIn();
			element.removeClass("resolving");
			element.hide().removeClass("manual");
		});
		$scope.$on("axis:request:start", function () {
			//$("#main").hide();
			//element.show();
		});
		$scope.$on("axis:request:all:finished", function () {
			if (element.hasClass("resolving")) return;
			element.hide().removeClass("manual");
			//$("#main").fadeIn();
		});

		$scope.reloadPage = false;
		$scope.showTimeout;


		Sugar.touch.on("move", function (e) {
			if (element.hasClass("resolving")) return;
			var distance = 30;


			if (document.body.scrollTop > 0 || Sugar.touch.movement == 'H' /*|| Sugar.touch.distance.y < distance*/ || Sugar.touch.position.y.initial > 60 || Sugar.touch.direction.y == 'U') {
				element.hide();
				return;
			}
			e.preventDefault();
			element.addClass("manual");



			var $icon = element.find(".icon");
			//var height = angular.element(document).height();
			var height = angular.element(window).outerHeight();
			var positionStart = height / 2 + ($icon.outerHeight()/2);
			var rotation = 180 + Sugar.touch.distance.y - distance;
			var top = -positionStart + Sugar.touch.distance.y /*- distance*/;

			Sugar.transform.rotate($icon.find("i"), 0, 0, 1, rotation);


			var cutPoint = 150;
			$scope.showTimeout = setTimeout(function () {
				element.show();
				$scope.reloadPage = top >= -cutPoint;
			}, 10);

			//element.removeClass("cut-point");
			if (top >= -cutPoint) {
				element.addClass("cut-point");
				return;
			} else {
				element.removeClass("cut-point");
			}

			Sugar.transform.translate($icon, 0, (top >= -14 ? -14 : top));
		});
		Sugar.touch.on("end", function () {
			if ($scope.showTimeout) {
				clearTimeout($scope.showTimeout);
				$scope.showTimeout = null;
			}
			if (element.hasClass("resolving")) return;
			if (document.body.scrollTop > 0 || !$scope.reloadPage || Sugar.touch.movement == 'H' || Sugar.touch.distance.y < 100 || Sugar.touch.direction.y == 'U') {
				if (element.hasClass("manual")) {
					var height = angular.element(window).outerHeight();
					element.addClass("returning");
					Sugar.transform.translate(element.find(".icon"), 0, -(height / 2 + 200));
					setTimeout(function () {
						element.css("transform", "").find(".icon").css("transform", "");
						element.removeClass("returning").removeClass("manual");
						element.hide();
					}, 100);
				}

				element.fadeOut(100, function () {
					element.css("transform", "").find(".icon").css("transform", "");
				});
				return;
			}
			element.fadeIn().css("transform", "").removeClass("manual").find(".icon").css("transform", "");
			$scope.reloadPage = false;
			$route.reload();
		});
	};
}])
