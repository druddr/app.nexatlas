﻿window.fbAsyncInit = function () {
	FB.init({
		appId: '1876635415995014',
		autoLogAppEvents: true,
		xfbml: true,
		version: 'v2.9'
	});
	FB.AppEvents.logPageView();
};

//TODO: Implement logout function
//FB.logout(function (response) {
//	// Person is now logged out
//});

(function (d, s, id) {
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) { return; }
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));