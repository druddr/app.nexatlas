var Sugar = {
	IsMobileOrTablet: (function () {
		var check = false;
		(function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
		return check;
	})(),
	isIE: (function () {
		var userAgent = navigator.userAgent;
		return !!window.StyleMedia || userAgent.indexOf('Trident') > -1 || userAgent.indexOf('Edge') > -1;
	})()
};

Sugar.dateToISO = function (date) {
	if (date.indexOf("/") > -1) {
		var sdate = date.split("/");
		return sdate[2] + "-" + sdate[1] + "-" + sdate[0];
	}
	if (date.indexOf("/") == -1 && date.indexOf("-") == -1) {
		date = date + "";
		return date[4] + date[5] + date[6] + date[7] + '-' + date[2] + date[3] + '-' + date[0] + date[1];
	}
	return date;
};
Sugar.numberToDateISO = function (number) {
	var sdate = (number + "");
	return sdate.substr(4, 4) + "-" + sdate.substr(2, 2) + "-" + sdate.substr(0, 2);
};
Sugar.generateGuid = function () {
	var d = new Date().getTime();
	if (window.performance && typeof window.performance.now === "function") {
		d += performance.now(); //use high-precision timer if available
	}
	var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
	return uuid;
};
Sugar.capitalize = function (s) {
	return s.charAt(0).toUpperCase() + s.slice(1);
}
Sugar.generateRandom = function (n) {
	var ranNum = Math.round(Math.random() * n);
	return ranNum;
}

// Função para retornar o resto da divisao entre números (mod)
Sugar.mod = function (dividendo, divisor) {
	return Math.round(dividendo - (Math.floor(dividendo / divisor) * divisor));
}
Sugar.valueGenerator = {

	// Função que gera números de CPF válidos
	cpf: function () {
		var n = 9;
		var n1 = Sugar.generateRandom(n);
		var n2 = Sugar.generateRandom(n);
		var n3 = Sugar.generateRandom(n);
		var n4 = Sugar.generateRandom(n);
		var n5 = Sugar.generateRandom(n);
		var n6 = Sugar.generateRandom(n);
		var n7 = Sugar.generateRandom(n);
		var n8 = Sugar.generateRandom(n);
		var n9 = Sugar.generateRandom(n);
		var d1 = n9 * 2 + n8 * 3 + n7 * 4 + n6 * 5 + n5 * 6 + n4 * 7 + n3 * 8 + n2 * 9 + n1 * 10;
		d1 = 11 - (Sugar.mod(d1, 11));
		if (d1 >= 10) d1 = 0;
		var d2 = d1 * 2 + n9 * 3 + n8 * 4 + n7 * 5 + n6 * 6 + n5 * 7 + n4 * 8 + n3 * 9 + n2 * 10 + n1 * 11;
		d2 = 11 - (Sugar.mod(d2, 11));
		if (d2 >= 10) d2 = 0;
		return '' + n1 + n2 + n3 + '.' + n4 + n5 + n6 + '.' + n7 + n8 + n9 + '-' + d1 + d2;
	},

	// Função que gera números de CNPJ válidos
	cnpj: function () {
		var n = 9;
		var n1 = Sugar.generateRandom(n);
		var n2 = Sugar.generateRandom(n);
		var n3 = Sugar.generateRandom(n);
		var n4 = Sugar.generateRandom(n);
		var n5 = Sugar.generateRandom(n);
		var n6 = Sugar.generateRandom(n);
		var n7 = Sugar.generateRandom(n);
		var n8 = Sugar.generateRandom(n);
		var n9 = 0;//Sugar.generateRandom(n);
		var n10 = 0;//Sugar.generateRandom(n);
		var n11 = 0;//Sugar.generateRandom(n);
		var n12 = 1;//Sugar.generateRandom(n);
		var d1 = n12 * 2 + n11 * 3 + n10 * 4 + n9 * 5 + n8 * 6 + n7 * 7 + n6 * 8 + n5 * 9 + n4 * 2 + n3 * 3 + n2 * 4 + n1 * 5;
		d1 = 11 - (Sugar.mod(d1, 11));
		if (d1 >= 10) d1 = 0;
		var d2 = d1 * 2 + n12 * 3 + n11 * 4 + n10 * 5 + n9 * 6 + n8 * 7 + n7 * 8 + n6 * 9 + n5 * 2 + n4 * 3 + n3 * 4 + n2 * 5 + n1 * 6;
		d2 = 11 - (Sugar.mod(d2, 11));
		if (d2 >= 10) d2 = 0;
		return '' + n1 + n2 + '.' + n3 + n4 + n5 + '.' + n6 + n7 + n8 + '/' + n9 + n10 + n11 + n12 + '-' + d1 + d2;
	},

	name: function () {
		var firstName = ["Runny", "Buttercup", "Dinky", "Stinky", "Crusty",
		"Greasy", "Gidget", "Cheesypoof", "Lumpy", "Wacky", "Tiny", "Flunky",
		"Fluffy", "Zippy", "Doofus", "Gobsmacked", "Slimy", "Grimy", "Salamander",
		"Oily", "Burrito", "Bumpy", "Loopy", "Snotty", "Irving", "Egbert"];
		var middleName = ["Waffer", "Lilly", "Rugrat", "Sand", "Fuzzy", "Kitty",
		 "Puppy", "Snuggles", "Rubber", "Stinky", "Lulu", "Lala", "Sparkle", "Glitter",
		 "Silver", "Golden", "Rainbow", "Cloud", "Rain", "Stormy", "Wink", "Sugar",
		 "Twinkle", "Star", "Halo", "Angel"];
		var lastName1 = ["Snicker", "Buffalo", "Gross", "Bubble", "Sheep",
		 "Corset", "Toilet", "Lizard", "Waffle", "Kumquat", "Burger", "Chimp", "Liver",
		 "Gorilla", "Rhino", "Emu", "Pizza", "Toad", "Gerbil", "Pickle", "Tofu",
		"Chicken", "Potato", "Hamster", "Lemur", "Vermin"];
		var lastName2 = ["face", "dip", "nose", "brain", "head", "breath",
		"pants", "shorts", "lips", "mouth", "muffin", "butt", "bottom", "elbow",
		"honker", "toes", "buns", "spew", "kisser", "fanny", "squirt", "chunks",
		"brains", "wit", "juice", "shower"];

		return firstName[Sugar.generateRandom(firstName.length - 1)] +
				" " + middleName[Sugar.generateRandom(middleName.length - 1)] +
				" " + lastName1[Sugar.generateRandom(lastName1.length - 1)] +
				lastName2[Sugar.generateRandom(lastName2.length - 1)];
	}
}
Sugar.events = {
	click: Sugar.IsMobileOrTablet ? 'click' : 'click',
	touchstart: Sugar.IsMobileOrTablet ? 'touchstart' : 'mousedown',
	touchend: Sugar.IsMobileOrTablet ? 'touchend' : 'mouseup',
	touchmove: Sugar.IsMobileOrTablet ? 'touchmove' : 'mousemove',
	touchcancel: Sugar.IsMobileOrTablet ? 'touchcancel' : 'mouseleave',
	contextmenu: Sugar.IsMobileOrTablet ? 'contextmenu' : 'contextmenu'
};
Sugar.preventBackButton = false;
Sugar._backButtonActions=[];
Sugar.addBackButtonAction = function (fnBack) {
	Sugar._backButtonActions.push(fnBack);
}
Sugar.removeBackButtonAction = function (fnBack) {
	Sugar._backButtonActions.slice().forEach(function (fnTest,iIndex) {
		if (fnBack === fnTest) {
			Sugar._backButtonActions.splice(iIndex, 1);
		}
	});
}
Sugar.getBackButtonActions = function () {
	return Sugar._backButtonActions;
}
Sugar.touch = {
	fnQueue: {
		start: [],
		move: [],
		end: [],
		leave: [],
		cancel: [],
		swipeRight: [],
		swipeLeft: [],
		swipeUp: [],
		swipeDown: [],
		swipe:[]
	},
	direction: {
		x: '',
		y: '',
	},
	movement:'',
	distance: {
		x: 0,
		y: 0,
	},
	position: {
		x: { initial: 0, current: 0, last: 0 },
		y: { initial: 0, current: 0, last: 0 }
	},
	time:{
		start:0,
		end: 0,
		total: 0,
		last: 0,
	},
	speed: {
		average: 0,
		current: 0,
		history:[]
	},
	multiple:{

	},
	init: function(){
		document.addEventListener("touchstart", function (e) {
			Sugar.touch.start(e);
		});
		document.addEventListener("touchmove", function (e) {
			Sugar.touch.move(e);
		});
		document.addEventListener("touchend", function (e) {
			Sugar.touch.end(e);
		});
		document.addEventListener("touchleave", function (e) {
			Sugar.touch.leave(e);
		});
		document.addEventListener("touchcancel", function (e) {
			Sugar.touch.cancel(e);
		});
	},
	clear: function () {
		this.position.x = { initial: 0, current: 0, last: 0 };
		this.position.y = { initial: 0, current: 0, last: 0 };
		this.direction.x = '';
		this.direction.y = '';
	},
	on: function(event,callback){
		if (!this[event]) return;
		Sugar.touch.fnQueue[event].push(callback);
		return this;
	},
	start: function (e) {
		this.time.start = new Date();
		this.speed.history = [];
		this.speed.current = 0;
		this.speed.last = this.time.start;
		this.position.x.initial = e.touches[0].pageX;
		this.position.y.initial = e.touches[0].pageY;
		for (i in this.fnQueue.start) {
			if (typeof this.fnQueue.start[i] == "function") this.fnQueue.start[i](e);
		}
		this.direction.x = null;
		this.direction.y = null;
	},
	move: function (e) {
		this.position.x.last = this.position.x.current;
		this.position.y.last = this.position.y.current;
		this.position.x.current = e.touches[0].pageX;
		this.position.y.current = e.touches[0].pageY;
		this.distance.x = this.direction.x == 'R' ? this.position.x.last - this.position.x.initial : this.position.x.initial - this.position.x.last;
		this.distance.y = this.direction.y == 'D' ? this.position.y.last - this.position.y.initial : this.position.y.initial - this.position.y.last;
		this.movement = this.distance.x > this.distance.y ? 'H' : 'V';

		this.speed.current = this.time.last - this.time.start;
		this.speed.history.push(this.speed.current);
		this.time.last = (new Date());

		if (this.direction.x == null) this.direction.x = (this.position.x.current > this.position.x.initial) ? 'R' : 'L';
		if (this.direction.y == null) this.direction.y = (this.position.y.current > this.position.y.initial) ? 'D' : 'U';

		for (i in this.fnQueue.move) {
			if (typeof this.fnQueue.move[i] == "function") this.fnQueue.move[i](e);
		}
	},
	end: function (e) {
		this.time.end = new Date();
		this.time.total = this.time.end - this.time.start;
		var total = 0;
		for (var i = 0; i < this.speed.history.length; i++) {
			total += this.speed.history[i];
		}
		this.speed.average = total / this.speed.history.length;

		var distance = 120;
		if (!Sugar.sidebar.isdragging) {
			if (this.movement == 'H' && this.direction.x == 'R' && this.distance.x >= distance && this.time.total < 400) {
				this.swipeRight(e);
			}
			if (this.movement == 'H' && this.direction.x == 'L' && this.distance.x >= distance && this.time.total < 400) {
				this.swipeLeft(e);
			}
			if (this.movement == 'V' && this.direction.y == 'U' && this.distance.y >= distance && this.time.total < 400) {
				this.swipeUp(e);
			}
			if (this.movement == 'V' && this.direction.y == 'D' && this.distance.y >= distance && this.time.total < 400) {
				this.swipeDown(e);
			}
			if ((this.distance.y >= distance || this.distance.x >= distance) && this.time.total < 400) {
				this.swipe(e);
			}
		}

		for (i in this.fnQueue.end) {
			if (typeof this.fnQueue.end[i] == "function") this.fnQueue.end[i](e);
		}
		this.position = {
			x: { initial: 0, current: 0, last: 0 },
			y: { initial: 0, current: 0, last: 0 }
		};
		this.distance = {
			x: 0,
			y: 0
		}
	},
	leave: function (e) {
		for (i in this.fnQueue.leave) {
			if (typeof this.fnQueue.leave[i] == "function") this.fnQueue.leave[i](e);
		}
	},
	cancel: function (e) {
		for (i in this.fnQueue.cancel) {
			if (typeof this.fnQueue.cancel[i] == "function") this.fnQueue.cancel[i](e);
		}
	},
	swipeRight: function (e) {
		for (i in this.fnQueue.swipeRight) {
			if (typeof this.fnQueue.swipeRight[i] == "function") this.fnQueue.swipeRight[i](e);
		}
	},
	swipeLeft: function (e) {
		for (i in this.fnQueue.swipeLeft) {
			if (typeof this.fnQueue.swipeLeft[i] == "function") this.fnQueue.swipeLeft[i](e);
		}
	},
	swipeDown: function (e) {
		for (i in this.fnQueue.swipeDown) {
			if (typeof this.fnQueue.swipeDown[i] == "function") this.fnQueue.swipeDown[i](e);
		}
	},
	swipeUp: function (e) {
		for (i in this.fnQueue.swipeUp) {
			if (typeof this.fnQueue.swipeUp[i] == "function") this.fnQueue.swipeUp[i](e);
		}
	},
	swipe: function (e) {
		for (i in this.fnQueue.swipe) {
			if (typeof this.fnQueue.swipe[i] == "function") this.fnQueue.swipe[i](e);
		}
	}
};
Sugar.touch.init();
Sugar.sidebar = {
	all: null,
	element: null,
	blocker: null,
	isopening: false,
	isodragging: false,
	settings: {
		width: 300,
	},
	init: function (sidebar_element) {
		Sugar.sidebar.reset();
		//This event is responsable for the drag and drop left menu functionality
		angular.element(document).on(Sugar.events.touchmove, function (e) {
			if (document.querySelectorAll(".modal.fade-in").length > 0) return;
			Sugar.sidebar.isdragging = false;
			//Somente irá abrir o menu quando pegar bem do cantinho esquerdo da tela
			if (Sugar.touch.direction.x == "R" && Sugar.touch.position.x.initial <= 15) {
				e.preventDefault();
				Sugar.sidebar.translate(Sugar.touch.position.x.current);
				Sugar.sidebar.isopening = true;
				Sugar.sidebar.isdragging = true;
				return;
			}
			if (Sugar.touch.direction.x == "L" /*&& Sugar.touch.position.x.initial >= Sugar.sidebar.settings.width*/ && !Sugar.sidebar.get().hasClass("closed")) {
				e.preventDefault();
				if (Sugar.touch.position.x.current > Sugar.touch.position.x.initial) return;
				var translate = Sugar.sidebar.settings.width - Math.abs(Sugar.touch.position.x.initial - Sugar.touch.position.x.current);
				Sugar.sidebar.translate(translate);
				Sugar.sidebar.isopening = false;
				Sugar.sidebar.isdragging = true;
				return;
			}
		});

		//this event will decide if sidebar will close or open
		angular.element(document).on(Sugar.events.touchend, function (e) {
			if (!Sugar.sidebar.isdragging) return;
			Sugar.sidebar.isdragging = false;
			if (Sugar.sidebar.isopening) {
				Sugar.sidebar.show();
			}
			else {
				Sugar.sidebar.hide();
			}
		});
		//this event will hide sidebar
		//everytime that a link, inside of it, is clicked
		angular.element(document).on(Sugar.events.click, "#sidebar a", function (e) {
			Sugar.sidebar.hide();
		});

		//When drawer-blocker is clicked it hides sidebar
		angular.element(document.querySelector(".nav-drawer-blocker")).on(Sugar.events.click, Sugar.sidebar.hide);

		//ng-child-active means that this element has submenus
		//it will open this submenus
		angular.element(document).on(Sugar.events.click, ".contains-child", function (e) {
			var navsub = angular.element(this).parent().find(".nav-sub");
			Sugar.sidebar.openSub(navsub);
		});

		//When the arrow up submenu in sidebar is clicked it closes submenu
		angular.element(document).on(Sugar.events.click, ".ng-sidebar-back", function () {
			Sugar.sidebar.closeSub(angular.element(this).parent().parent())
		});
		angular.element(document).ready(function () {
			if (Sugar.sidebar.isPinned()) {
				Sugar.sidebar.pin();
			}
		});
	},
	reset: function () {
		Sugar.sidebar.all = angular.element(document.querySelectorAll(".toggle-sidebar,#sidebar,.nav-drawer-blocker"));
		Sugar.sidebar.element = angular.element(document.querySelectorAll("#sidebar"));
		Sugar.sidebar.blocker = angular.element(document.querySelectorAll(".nav-drawer-blocker"));
	},
	get: function (all) {
		if (Sugar.sidebar.all == null) Sugar.sidebar.all = angular.element(document.querySelectorAll(".toggle-sidebar,#sidebar,.nav-drawer-blocker"));
		if (Sugar.sidebar.element == null) Sugar.sidebar.element = angular.element(document.querySelectorAll("#sidebar"));
		if (Sugar.sidebar.blocker == null) Sugar.sidebar.blocker = angular.element(document.querySelectorAll(".nav-drawer-blocker"));
		return all ? Sugar.sidebar.all : Sugar.sidebar.element;
	},
	getToggler: function () {
		return angular.element(document.querySelectorAll(".toggle-sidebar"));
	},
	isOpen: function () {
		return Sugar.sidebar.get().hasClass("open");
	},
	translate: function (translate, element,iTopTranslateToKeep) {
		var sidebar = element == undefined ? Sugar.sidebar.get() : element, translate = translate > Sugar.sidebar.settings.width ? Sugar.sidebar.settings.width : translate;
		sidebar.removeClass("open").removeClass("closed").addClass("dragging-sidebar");

		Sugar.transform.translate(sidebar, translate, iTopTranslateToKeep);

		if (element) return;
		var opacity = (translate / Sugar.sidebar.settings.width) / 2;
		opacity = opacity > 0.2 ? opacity : 0.2;
		var visibility = opacity == 0 ? 'hidden' : 'visible';
		Sugar.sidebar.blocker.removeClass("open").removeClass("closed").removeClass("dragging-sidebar").addClass("menu-sliding").css("opacity", opacity).css("visibility", visibility).addClass("dragging-sidebar");
	},
	show: function () {
		Sugar.sidebar.get().removeAttr("style");
		Sugar.sidebar.get(true).addClass("open").removeClass("closed").removeClass("dragging-sidebar");
		Sugar.sidebar.get().find(".ng-item-name").addClass("ng-child-active");
	},
	hide: function () {
		Sugar.sidebar.get().removeAttr("style");
		Sugar.sidebar.get(true).addClass("closed").removeClass("open").removeClass("dragging-sidebar")[0].scrollTop = 0;
		Sugar.sidebar.closeSub();
		Sugar.sidebar.get().find("ul:first .profile").height("");
	},
	toggle: function () {
		if (Sugar.sidebar.isOpen()) {
			Sugar.sidebar.hide();
			return;
		}
		Sugar.sidebar.show();
	},
	isPinned: function () {
		return localStorage.getItem("sidebar-pinned") == '1';
	},
	pin: function () {
		localStorage.setItem("sidebar-pinned", '1');
		Sugar.sidebar.get(true).addClass("pinned");
		angular.element(document.querySelectorAll("#main, .app-bar, .panel")).addClass("sidebar-pinned");
		var tabs = angular.element(document.querySelectorAll(".tabs li[data-tab-id].active"));
		if (tabs.length > 0) {
			tabs.trigger(Sugar.events.touchstart);
		}
	},
	unpin: function () {
		console.log('here',localStorage.getItem("sidebar-pinned"));
		localStorage.setItem("sidebar-pinned", '0');
		Sugar.sidebar.get(true).removeClass("pinned");
		angular.element(document.querySelectorAll("#main, .app-bar, .panel")).removeClass("sidebar-pinned");
		var tabs = angular.element(document.querySelectorAll(".tabs li[data-tab-id].active"));
		if (tabs.length > 0) {
			tabs.trigger(Sugar.events.touchstart);
		}
	},
	togglePin: function () {
		if (Sugar.sidebar.isPinned()) {
			Sugar.sidebar.unpin();
			return;
		}
		Sugar.sidebar.pin();
	},
	showSubmenu: function () {

	},
	events: function () {
		if (window.innerWidth >= 768) {
			Sugar.sidebar.show();
		}
		else {
			Sugar.sidebar.hide();
		}
	},
	openSub: function (submenu) {
		var navsubs = document.querySelectorAll(".nav-sub"),
			nestingLevel = submenu.attr('data-nesting-level');
		if (navsubs.length == 0) return;
		angular.element(navsubs).not(submenu.parent().parent().parent().get(0)).hide();
		submenu.show();
		Sugar.sidebar.get()[0].scrollTop = 0;
		var oFirst = Sugar.sidebar.get().find("ul:first");
		Sugar.sidebar.translate(Sugar.sidebar.settings.width * -1 * nestingLevel, oFirst);
	},
	closeSub: function (submenu) {
		var navsubs = document.querySelectorAll(".nav-sub");
		if (navsubs.length == 0) return;
		if (!submenu) {
			angular.element(navsubs).hide();
			Sugar.sidebar.translate(0, Sugar.sidebar.get().find("ul:first"));
			return;
		}
		var nestingLevel = submenu.attr('data-nesting-level')
		Sugar.sidebar.get()[0].scrollTop = 0;
		var translateback = Sugar.sidebar.settings.width - (Sugar.sidebar.settings.width * (nestingLevel ));
		Sugar.sidebar.translate(translateback, Sugar.sidebar.get().find("ul:first"));
		setTimeout(function () {
			angular.element(navsubs).not(submenu.parent().parent().parent().get(0)).hide();
		}, 250);
	}
}

Sugar.calendar = {
	month: ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'],
	week: ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
	create: function (year, month, day) {
		day = parseInt(day);
		month = parseInt(month);
		year = parseInt(year);

		var initial_time = strtotime(year + "-" + month + "-" + str_pad(day, 2, '0', 'STR_PAD_LEFT'));
		var prev_time = strtotime("-1 month", initial_time);
		var next_time = strtotime("+1 month", initial_time);
		var month_first_day_time = strtotime(year + "-" + month + "-01");
		var month_total_days = parseInt(date("t", initial_time));
		var prev_month_total_days = parseInt(date("t", prev_time));
		var next_month_total_days = parseInt(date("t", next_time));
		var first_day_week_month = parseInt(date("N", month_first_day_time));
		var total_days_before_first = 7 - first_day_week_month;


		var calendar = document.createElement("div");
		calendar.className = "sugar-calendar";
		var divday = document.createElement("div");
		divday.setAttribute("data-calendar-day", "");
		divday.setAttribute("data-calendar-month", "");
		divday.setAttribute("data-calendar-year", "");
		divday.className = "day";

		for (var i = 0; i < Sugar.calendar.week.length; i++) {
			var d = document.createElement("div");
			d.className = "week-day";
			d.innerHTML = Sugar.calendar.week[i];
			calendar.appendChild(d);
		}

		var a = 1, b = first_day_week_month - 1, c = 1, d = prev_month_total_days - first_day_week_month, e = 1, f, g, h, j;
		for (var i = 1; i <= 42; i++) {
			g = (i <= first_day_week_month && i <= 7);
			a = g ? ++d : c++;
			h = (a > month_total_days && i > 27);
			a = h ? e++ : a;
			j = g ? prev_time : (h ? next_time : initial_time);
			f = divday.cloneNode(true);
			f.innerHTML = a;

			f.className = a == day && (date("Ymd", j) == date("Ymd", strtotime(year + "-" + str_pad(month, 2, "0", 'STR_PAD_LEFT') + "-" + str_pad(day, 2, '0', 'STR_PAD_LEFT')))) ? "day current" : "day";
			f.setAttribute("data-calendar-day", a);
			f.setAttribute("data-calendar-month", date("m", j));
			f.setAttribute("data-calendar-year", date("Y", j));
			calendar.appendChild(f);



		}
		return calendar;
	}
};

Sugar.datepicker = {
	template: '',
	months: ['jan', 'fev', 'mar', 'abr', 'mai', 'jun', 'jul', 'ago', 'set', 'out', 'nov', 'dez'],
	getJsDate: function (input) {
		var parts = input.match(/(\d+)/g);
		return new Date(parts[0], parts[1] - 1, parts[2]);
	},
	date: function (date) {
		var currrent_date = date("d/m/Y");
		console.log(currrent_date);
		return current_date;
	},
	events: function () {

		angular.element(document).on(Sugar.events.touchstart, ".modal .calendar .day", function (e) {
			e.preventDefault();
			var day = str_pad(this.innerText, 2, '0', 'STR_PAD_LEFT');
			var $modal = angular.element(document.querySelectorAll(".modal.datepicker"));
			$modal.find("[data-day]").attr("data-day", day).html(day);
			var $current = angular.element(this);
			var day = str_pad($current.attr("data-calendar-day"), 2, "0", 'STR_PAD_LEFT');
			var month = str_pad($current.attr("data-calendar-month"), 2, "0", 'STR_PAD_LEFT');
			var year = $current.attr("data-calendar-year");
			$modal.find("[data-day]").attr("data-day", day).html(day);
			$modal.find("[data-month]").attr("data-month", month).html(Sugar.datepicker.months[parseInt(month - 1)]);
			$modal.find("[data-year]").attr("data-year", year).html(year);


			angular.element(document.querySelectorAll("#modal-date-picker .day")).removeClass("current");
			angular.element(this).addClass("current");

		});
		angular.element(document).on(Sugar.events.touchstart, ".modal .date-text .prev", function () {
			var $self = angular.element(this), $parent = $self.parent(),
				$day = $parent.find("[data-day]"),
				$month = $parent.find("[data-month]"),
				$year = $parent.find("[data-year]"),
				day = $day.attr("data-day") == '' ? date("d") : $day.attr("data-day"),
				month = $month.attr("data-month") == '' ? date("m") : $month.attr("data-month"),
				year = $year.attr("data-year") == '' ? date("Y") : $year.attr("data-year"),
				time = strtotime(" -1 month", strtotime(year + "-" + month + "-" + day));

			$day.html(date("d", time)).attr("data-day", date("d", time));
			$month.html(Sugar.datepicker.months[parseInt(date("m", time)) - 1]).attr("data-month", date("m", time));
			$year.html(date("Y", time)).attr("data-year", date("Y", time));
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(date("Y", time), date("m", time), date("d", time)));

		});
		angular.element(document).on(Sugar.events.touchstart, ".modal .date-text .next", function () {
			var $self = angular.element(this), $parent = $self.parent(),
				$day = $parent.find("[data-day]"),
				$month = $parent.find("[data-month]"),
				$year = $parent.find("[data-year]"),
				day = $day.attr("data-day") == '' ? date("d") : $day.attr("data-day"),
				month = $month.attr("data-month") == '' ? date("m") : $month.attr("data-month"),
				year = $year.attr("data-year") == '' ? date("Y") : $year.attr("data-year"),
				time = strtotime(" +1 month", strtotime(year + "-" + month + "-" + day));

			$day.html(date("d", time)).attr("data-day", date("d", time));
			$month.html(Sugar.datepicker.months[parseInt(date("m", time)) - 1]).attr("data-month", date("m", time));
			$year.html(date("Y", time)).attr("data-year", date("Y", time));
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(date("Y", time), date("m", time), date("d", time)));

		});
		angular.element(document).on("focus", "[data-date-picker-open]", function (e) {
			e.preventDefault();
			var $self = angular.element(this);
			var day = $self.val().substr(0, 2) == "" ? date("d") : $self.val().substr(0, 2);
			var month = $self.val().substr(3, 2) == "" ? date("m") : $self.val().substr(3, 2);
			var year = $self.val().substr(6, 4) == "" ? date("Y") : $self.val().substr(6, 4);

			Sugar.datepicker.open($self);
			angular.element(document.querySelectorAll(".modal .calendar")).html(Sugar.calendar.create(year, month, day));
			$self.blur();
		})
	},
	init: function () {
		if (angular.element(document.querySelectorAll("#modal-date-picker")).size() == 0) {
			angular.element(document.body).append('<div class="modal datepicker" id="modal-date-picker">' +
								//'	<div class="title">Datepicker</div>' +
								//'	<div class="content">' +
								//'		<div class="date-text">' +
								//'			<div class="prev fa fa-caret-left"></div>' +
								//'			<div data-day class="day">20</div>' +
								//'			<div data-month class="month">AGO</div>' +
								//'			<div data-year class="year">2015</div>' +
								//'			<div class="next fa fa-caret-right"></div>' +
								//'		</div>' +
								//'		<div class="week">' +
								//'		</div>' +
								//'		<div class="calendar">' +
								//'		</div>' +
								//'	</div>' +
								//'	<div class="actions right">' +
								//'		<button data-close-modal="modal-date-picker">Ok</button>' +
								//'	</div>' +
								'</div>');
		}
		Sugar.datepicker.events();
	},
	open: function ($element) {
		var $modal = angular.element(document.body).find(".modal.datepicker");

		var day = $element.val().substr(0, 2);
		var month = $element.val().substr(3, 2);
		var year = $element.val().substr(6, 4);

		$modal.find("button").unbind(Sugar.events.touchstart).on(Sugar.events.touchstart, function () {
			var $current = $modal.find(".day.current");
			var day = str_pad($current.attr("data-calendar-day"), 2, "0", 'STR_PAD_LEFT');
			var month = str_pad($current.attr("data-calendar-month"), 2, "0", 'STR_PAD_LEFT');
			var year = $current.attr("data-calendar-year");
			$modal.find("[data-day]").attr("data-day", day).html(day);
			$modal.find("[data-month]").attr("data-month", month).html(Sugar.datepicker.months[parseInt(month - 1)]);
			$modal.find("[data-year]").attr("data-year", year).html(year);
			$element.val(day + "/" + month + "/" + year);
			$element.trigger("input");
		});

		$modal.find(".calendar .day").removeClass("current").each(function () {
			if (this.innerHTML == day) angular.element(this).addClass("current");

		});
		$modal.show().addClass("fade-in");

	},
	close: function ($element) {

	}
}
Sugar.datepicker.init();
Sugar.modal = {
	init:function(){
		Sugar.modal.bind();
	},
	bind: function () {

	}
}
Sugar.modal.init();
Sugar.switchbox = {
	init:function(){
		Sugar.switchbox.bind();
	},
	bind: function () {
		angular.element(document).on(Sugar.events.touchstart, ".switch-box .switch", function () {
			var $self = $(this);
			if ($self.hasClass("on")) {
				$self.removeClass("on").attr("data-state", "off");
				$self.find("input[type=checkbox]").trigger("click").removeAttr("checked");
				return;
			}
			$self.addClass("on").attr("data-state", "on");
			$self.find("input[type=checkbox]").trigger("click").attr("checked", true);
			//$self.find("i").addClass("fa fa-check");
		});

	}
}
Sugar.switchbox.init();
Sugar.alert = function (message, type) {
	angular.element(document.querySelectorAll(".alert")).remove();
	var div = document.createElement("div");
	div.className = "alert " + type;
	div.innerHTML = "<div class='message animated slideInUp'>" + message + "</div>";
	document.body.appendChild(div);
	//angular.element(document.querySelectorAll(".app-bar,#main, nav.drawer")).addClass("blur");


	angular.element(div).on("click", function () {
		var $alert = $(this);
		$alert.addClass("fade-out");
		setTimeout(function () {
			$alert.remove();
			//angular.element(document.querySelectorAll(".app-bar,#main,nav.drawer")).removeClass("blur");
		}, 300)
	});

}
Sugar.alert.error = function (message) {
	Sugar.alert(message, 'fail');
}
Sugar.alert.success = function (message) {
	Sugar.alert(message, 'success');
}

Sugar.card = {
	init:function(){
		Sugar.card.events.generic();
	},
	select: function (card) {
		card = angular.element(card);
		if (!card.hasClass('selectable')) return;
		if (card.hasClass("selected")) {
			card.removeClass("selected");
			return;
		};
		if (!card.hasClass("multi-select")) {
			$(".card").removeClass("selected");
		}
		card.addClass("selected");
	},
	events: {
		select: function (card, e) {
			if (e.type == 'keydown' || e.type=='keyup') {
				var keycode = e.which || e.keyCode || e.charCode;
				if (keycode != 32) return;
				e.preventDefault();
			}
			Sugar.card.select(card);
		},
		generic: function () {
			angular.element(document).on(Sugar.events.contextmenu, ".card-image img", function (e) {
				e.preventDefault();
			});
		}
	}
}
Sugar.card.init();
Sugar.accordion = {
	init:function(){
		Sugar.accordion.bind();
	},
	bind: function () {
		angular.element(document).on("click keydown", ".accordion-item-title", function (e) {

			if (e.type == 'keydown') {
				var keycode = e.keyCode || e.which || e.charCode;
				if (keycode != 32 && keycode!= 13) return;
			}
			//e.preventDefault();
			e.stopPropagation();

			$target = angular.element(this).parent();
			$target.toggleClass("active");
			//$target.find('.accordion-item-content').toggle();
		})
	}
}
Sugar.accordion.init();
Sugar.effects = {
	init: function () {
		Sugar.effects.bind();
	},
	bind: function () {
		var waveStart = new Date();
		angular.element(document).on(Sugar.events.touchstart, ".action, button, .button, .card-image,.tab", function (e) {
			$target = angular.element(this);
			if ($target.hasClass("no-effect")) return;
			$target.find(".wave-effect").remove();
			var div = document.createElement("div");
			div.className = "wave-effect";
			if (Sugar.IsMobileOrTablet) {
				div.style.top = (e.originalEvent.touches[0].pageY - $target.offset().top - 10) + "px";
				div.style.left = (e.originalEvent.touches[0].pageX - $target.offset().left - 10) + "px";
			}
			else {
				div.style.top = (e.pageY - $target.offset().top - 10) + "px";
				div.style.left = (e.pageX - $target.offset().left - 10) + "px";
			}
			$target.append(div);
			waveStart = new Date();
		});
		angular.element(document).on(Sugar.events.touchend + " " + Sugar.events.touchcancel, function (e) {
			var a = setInterval(function () {
				var date = new Date();
				if (date - waveStart < 300) return;

				$target = angular.element(this);
				$target.find(".wave-effect").addClass("fadeout");
				clearInterval(a);
			}.bind(this), 1);
		});

		$.fn.extend({
			animateCss: function (animationName, animationEndFn) {
				var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				$(this).addClass(animationName).addClass('animated').one(animationEnd, function () {
					var $self = $(this);
					setTimeout(function () {
						$self.removeClass('animated').removeClass(animationName);
					}, 0)
					if (animationEndFn) animationEndFn();
					animationEndFn = null;
				});
			}
		});
	}
}
Sugar.effects.init();
Sugar.list = {
	init:function(){
		Sugar.list.bind();
	},
	bind: function () {

	}
}
Sugar.list.init();
Sugar.form = {
	field: {
		binary: {
			init: function () {
				angular.element(document).on('focus blur', '.form-control', function (e) {
					$parent = $(this).parent();
					if (e.type=='focusin') {
						$parent.find('.input-border-bottom').addClass('state-focus')
					}
					else {
						$parent.find('.input-border-bottom').removeClass('state-focus');
					}
				})
			}
		}
	}
}
Sugar.form.field.binary.init();
Sugar.promise = function (context) {
	var _status = {
		pending: 'pending',
		fulfilled: 'fulfilled',
		rejected: 'rejected',
		settled: 'settled'
	}
	var _private = {
		then: [],
		catch: [],
		status: _status.pending,
		result: null,
		resolve: function (args) {
			_private.status = _status.fulfilled;;
			_private.call(args);
		},
		reject: function (args) {
			_private.status = _status.rejected
			_private.call(args);
		},
		call: function () {
			var oResult;
			var bBreak = false;

			if (_private.status === _status.fulfilled) {

				_private.then.slice().forEach(function (fnThen, i) {
					if (bBreak) return;

					oResult = fnThen.apply(context, [_private.result]);

					_private.then.splice(i, 1);

					bBreak = _private.checkResultExecution(oResult);
				});
			}
			if (_private.status === _status.rejected) {

				_private.catch.slice().forEach(function (fnCatch, i) {
					if (bBreak) return;

					oResult = fnCatch.apply(context, [_private.result]);

					_private.fnCatch.splice(i, 1);

					bBreak = _private.checkResultExecution(oResult);
				});

			}
		},
		checkResultExecution: function (oResult) {
			if (oResult === undefined) return false;

			if (oResult instanceof Sugar.promise) {
				_private.status = _status.pending;

				oResult.then(function (oNewResult) {
					_private.status = _status.fulfilled;
					if (_private.checkResultExecution(oNewResult)) {
						return;
					}
					_private.call();
				}).catch(function (oNewCallback) {
					_private.status = _status.rejected;
					if (_private.checkResultExecution(oNewResult)) {
						return;
					}
					_private.call();
				});

				return true;
			}
			_private.result = oResult;

			return false;
		}
	};

	this.resolve = function (result) {
		if (_private.status == _status.fulfilled) return this;
		_private.status = _status.fulfilled;
		_private.result = result;
		//Call all the multiple callbacks
		_private.call();
		return this;
	};
	this.reject = function (result) {
		_private.status = _status.rejected;
		_private.result = result;
		//Call all the multiple callbacks
		_private.call();
		return this;
	};
	this.then = function (fnCallback) {
		_private.then.push(fnCallback);
		//we have multiple callbacks
		//than we can not use "call"
		//because if this callback was
		//added before the promise resolve
		//than all the callbacks before promise
		//were already called
		_private.call();
		return this;
	};
	this.catch = function (fnCallback) {
		_private.catch.push(fnCallback);
		//we have multiple callbacks
		//than we can not use "call"
		//because if this callback was
		//added before the promise resolve
		//than all the callbacks before promise
		//were already called
		_private.call();
		return this;
	};

	return this;
};
Sugar.transform = {

	translate: function (element, x, y, z) {
		x = x ? x : 0;
		y = y ? y : 0;
		z = z ? z : 0;
		element.css("-webkit-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-moz-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-ms-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("-o-transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
		element.css("transform", "translate3d(" + x + "px," + y + "px," + z + "px)");
	},
	rotate: function (element, x, y, z, degrees) {
		x = x ? x : 0;
		y = y ? y : 0;
		z = z ? z : 0;
		degrees = degrees ? degrees : 0;
		element.css("-webkit-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-moz-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-ms-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("-o-transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
		element.css("transform", "rotate3d(" + x + "," + y + "," + z + "," + degrees + "deg)");
	}
}
