settings.devmode = true;
settings.debug = false;

settings.app = {
	baseurl: 'http://app.nexatlas.com/',
	theme: 'ng-theme-light',
	language: 'pt-BR',
	header: {
		file: 'app/partials/header.html'
	},
	sidebar: {
		position: 'fixed',
		background: 'ng-background-5',
		file: 'app/partials/sidebar.html',
		width: 250
	},
	services: {
		api: 'http://api.nexatlas.com/',
		facebook: 'http://facebook.com/',
		google: 'http://google.ccom/'
	},
	is_extension: window.location.protocol == 'chrome-extension:'
};
settings.router.signupform = '/app/authentication/form';
settings.router.signupgoogle = '/app/authentication/form';
settings.router.signupfacebook = '/app/authentication/form';
settings.router.forbidden = '/app/static/errors/forbidden';
settings.components.default_components = [
	'app-drawer',
	'panel/tab/list',
	'selectable-list',
];

switch (window.location.hostname) {
	case 'app.nexatlas':
		settings.app.baseurl = 'http://app.nexatlas/';
		settings.app.services.api = 'http://api.nexatlas/';
		break;
	case 'alfa.nexatlas.com':
		settings.app.baseurl = 'http://alfa.nexatlas.com/app';
		settings.app.services.api = 'http://alfa.nexatlas.com/api';
		break;
	case 'beta.nexatlas.com':
		settings.app.baseurl = 'http://beta.nexatlas.com/app';
		settings.app.services.api = 'http://beta.nexatlas.com/api';
		break;
	default:
		settings.app.services.api = 'http://api.nexatlas.com/';
		settings.app.baseurl = 'http://' + window.location.hostname + '/';
		break;

}

angular.module('System-Authorization',[]);

angular.module("System-Authorization").factory("Session", ['$rootScope', function ($rootScope) {
	var $current_user_store_key = '#current$user@';
	var $current_company_store_key = '#current$company@';
	var $current_subsidiary_store_key = '#current$subsidiary@';

	var $companies_store_key = '#list$companies@';
	var $subsidiaries_store_key = '#list$subsidiaries@';
	var $routes_store_key = '#list$routes@';
	
	var session = {
		clear: function () {
			Storage.local.set($current_user_store_key, {});
			Storage.local.set($current_subsidiary_store_key, {});
			Storage.local.set($current_company_store_key, {});

			Storage.local.set($routes_store_key, null);
			Storage.local.set($subsidiaries_store_key, null);
			Storage.local.set($companies_store_key, null);

			$rootScope.logged = false;
		},
		setCurrent:{
			user: function (user) {
				session.current.user = user;
				return Storage.local.set($current_user_store_key, user);
			},
			company: function (company) {				
				for (var i = 0; i < session.list.subsidiaries.length; i++) {
					if (company.id == session.list.subsidiaries[i].id_empresa) {
						session.setCurrent.subsidiary(session.list.subsidiaries[i]);
						break;
					}
				}
				session.current.company = company;
				return Storage.local.set($current_company_store_key, company);;
			},
			subsidiary: function (subsidiary) {
				session.current.subsidiary = subsidiary;
				return Storage.local.set($current_subsidiary_store_key, subsidiary);
			}
		},
		setList:{
			companies: function (companies) {
				session.list.companies = companies;
				return Storage.local.set($companies_store_key, companies);
			},
			subsidiaries: function (subsidiaries) {
				session.list.subsidiaries = subsidiaries;
				return Storage.local.set($subsidiaries_store_key, subsidiaries);
			},
			routes: function (routes) {
				session.list.routes = routes;
				return Storage.local.set($routes_store_key, routes);
			}
		},
		current: {
			user: Storage.local.get($current_user_store_key, {}),
			company: Storage.local.get($current_company_store_key, {}),
			subsidiary: Storage.local.get($current_subsidiary_store_key, {})
		},
		list: {
			companies: Storage.local.get($companies_store_key, null),
			subsidiaries: Storage.local.get($subsidiaries_store_key, null),
			routes: Storage.local.get($routes_store_key, null)
		},
		start: function (info) {
			session.setList.routes(info.routes);
			session.setList.companies(info.companies);
			session.setList.subsidiaries(info.subsidiaries);

			session.setCurrent.user(info.user);
			session.setCurrent.company(info.companies[0]);
			session.setCurrent.subsidiary(info.subsidiaries[0]);
		},
		checkRoutePermission: function (route) {
			var routeAllowed = false;
			for (var i = 0; i < session.list.routes.length; i++) {
				//TODO check if parameters are allowed
				if (session.current.subsidiary.id == session.list.routes[i].id_empresa_filial 
					&& session.list.routes[i].rota.toLowerCase() == route.toLowerCase()) {
					routeAllowed = true;
				}
			}
			return routeAllowed;
		}
	}

	return session;
}]);

angular.module("System-Authorization").run(['$rootScope', '$location', '$http', 'Session', function ($rootScope, $location, $http, Session) {
	$rootScope.$on('$locationChangeStart', function (event, next, current) {
		//Set last route
		if ($location.$$url != settings.router.forbidden
				&& $location.$$url != settings.router.signupform
				&& $location.$$url != settings.router.authentication
				&& $location.$$url != settings.router.signupgoogle
				&& $location.$$url != settings.router.signupfacebook) {
			$rootScope.lastRoute = $location.$$url;
		}
		//Check Session
		if (!Session.current.user.id) {
			$rootScope.logged = false;
			if ($location.$$url != settings.router.forbidden
				&& $location.$$url != settings.router.signupform
				&& $location.$$url != settings.router.authentication
				&& $location.$$url != settings.router.signupgoogle
				&& $location.$$url != settings.router.signupfacebook) {
				$location.path(settings.router.authentication);
			}
		}
		else {
			$rootScope.logged = true;
		}
		if (settings.devmode) return;

		var route = angular.module('Axis-Router').Router.getRoute($location.$$url);
		
		//Check Route permission
		var $routeAllowed = Session.checkRoutePermission(route.path);

		if (!$routeAllowed
			&& $location.$$url != settings.router.forbidden
			&& $location.$$url != settings.router.signupform
			&& $location.$$url != settings.router.authentication
			&& $location.$$url != settings.router.signupgoogle
			&& $location.$$url != settings.router.signupfacebook) {
			$location.path(settings.router.forbidden);
		}
	});
}]);

angular.module("System-Authorization").factory('HeadersDefault-httpInterceptor', ['$q', '$rootScope', '$log', 'Session', function ($q, $rootScope, $log, Session) {
	return {
		request: function (config) {
		    
		    config.headers['X-Requested-With'] = 'XMLHttpRequest';
			config.headers['Rest'] = 'REST';
			config.headers['CCID'] = Session.current.company.id;
			config.headers['CSID'] = Session.current.subsidiary.id;

			return config || $q.when(config);
		},
		response: function (response) {
			return response || $q.when(response);
		},
		responseError: function (response) {
			return $q.reject(response);
		}
	};
}])
.config(['$httpProvider', function ($httpProvider) {
	$httpProvider.interceptors.push('HeadersDefault-httpInterceptor');
}])
angular.module('System', ['Sugar', 'System-Authorization', 'Axis', 'Axis-Router', 'ngTouch', 'ui.utils.masks']);

angular.module('System').config(['$compileProvider', function ($compileProvider) {
	$compileProvider.debugInfoEnabled(false);
}]);


angular.module('System').factory("i18n", [(function () {
	var service = {
		language: {},
		setLanguage: function (lang) {			
			service.language = lang;
		},
		getTranslated: function (key) {
			var value = '';
			try {
				value = eval("service.language." + key);
			}
			catch (e) { }
			return value;
		}
	}
	      
	return function () {
		return service;
	}
})()]);

angular.module('System').filter("translate", ['i18n', function (i18n) {
	return function (input) {
		return i18n.getTranslated(input);
	}
}]);

angular.module('System').controller("ApplicationController", ['$scope', '$location', '$rootScope', '$injector', '$route', '$touch', 'Session','i18n',
function ($scope, $location, $rootScope, $injector, $route, $touch, Session, i18n) {
	$scope.settings = settings.app;
	$scope.sidebar = {}
	$scope.language = {
		instance: null,
		selected: null,
		set: function (new_language) {
			//Load language file
			Loader.load.withnocache().js('app/i18n/' + new_language + '.js', function () {
				$scope.language.instance = Language;
				$scope.language.selected = new_language;
				$rootScope.$broadcast("language-loaded");
				Storage.local.set('chosen-language', new_language);
				i18n.setLanguage($scope.language.instance);
			}, false, 'language-script-repository', true);
		},
		apply: function () {
			var s = arguments[0];
			for (i = 1; i < arguments.length; i++) {
				s = s.replace('@?', arguments[i]);
			}
			return s;
		}
	}
	$scope.mobile = Sugar.IsMobileOrTablet;
	$scope.settings.language = Storage.local.get('chosen-language', settings.app.language);
	$scope.language.set($scope.settings.language);

	$rootScope.language = $scope.language;

	$touch.ngClickOverrideEnabled(true);

	$rootScope.logged = Session.current.user.id != null;
	$scope.navigation = {
		reloading: true,
		active: function (path) {
			return path == "#" + $location.path().toLowerCase();
		},
		go: function (path) {
			$location.path(path);
		},
		back: function () {
			window.history.back();
		},
		withoutReloading: function () {
			$scope.navigation.reloading = false;
			return this;
		}
	}
	var lastRoute = $route.current;
	$scope.$on('$routeChangeStart', function (e, next, last) {
		if (!$scope.navigation.reloading) {
			e.preventDefault();
			$route.current = last.$$route;
		}
	});
	$scope.$on("$locationChangeStart", function (e) {

	});

	$scope.title = 'RedCherry app';
	$scope.setTitle = function (title) {
		$scope.title = title;
	}
	$scope.header = {
		setTitle: function (title) {
			if ($scope.language.instance.labels[title]) {
				$scope.title = $scope.language.instance.labels[title];
			}
			else {
				$scope.title = title;
			}
		},
		show: function () {
			$scope.header.hidden = false;
		},
		hide: function () {
			$scope.header.hidden = true;
		},
		customText: null,
		setCustomText: function (text) {
			$scope.header.customText = text;
		},
		setMainIcon: function (icon, fnAction) {
			$scope.header.mainIcon = icon;
			$scope.header.mainIconAction = fnAction;
		},
		setBackgrond: function (background, color) {
			if (typeof StatusBar !== "undefined") {
				StatusBar.hide();
				StatusBar.backgroundColorByHexString(hexc(background));
			}
			$(".app-bar").css('background', background).css('color', color);
		},
		icon: {
			showBack: function (action) {
				$scope.header.setMainIcon('arrow_back', action ? action : $scope.navigation.back);
			}
		},
		actions: {
			open: false,
			list: [],
			clear: function (actions) {
				$scope.header.actions.list = [];
			},
			set: function (actions) {
				$scope.header.actions.clear();

				for (i in actions) {
					$scope.header.actions.add(actions[i].name, actions[i].icon, actions[i].click, actions[i].items, actions[i].attributes)
				}
			},
			add: function (name, icon, click, items, attributes) {
				if (typeof icon == 'function') {
					attributes = items;
					items = click;
					click = icon;
					icon = undefined;
				}
				$scope.header.actions.list.push($scope.header.actions.create(name, icon, click, items, attributes));
			},
			remove: function (id) {
				for (i in $scope.header.actions.list) {
					if ($scope.header.actions.list[i].id == id) {
						$scope.header.actions.list.splice(i, 1);
						break;
					}
				}
			},
			create: function (name, icon, click, items, attributes) {
				return { name: name, icon: icon, click: click, items: items, attributes: attributes };
			}
		}
	}
	$scope.tabs = {
		visible: true,
		list: [],
		add: function (label, icon, active, hideLabel) {
			this.list.push({
				label: hideLabel ? '' : $scope.language.instance.labels[label],
				icon: icon,
				contentId: label,
				active: active
			});
		},
		remove: function (label) {
			for (var i = 0; i < this.list.length; i++) {
				if (this.list[i].contentId == label) {
					this.list.shift(i, 1);
					return true;
				}
			}
			return false;
		},
		clear: function () {
			$scope.tabs.list = [];
		},
		hide: function () {
			$scope.tabs.visible = false;
		},
		show: function () {
			$scope.tabs.visible = true;
		}
	}

	$scope.alert = {
		info: function (key) {
			Sugar.alert($scope.language.instance.phrases[key]);
		},
		success: function (key, code) {
			Sugar.alert.success($scope.language.instance.success[key + '-' + code]);
		},
		error: function (key, code) {
			Sugar.alert.error($scope.language.instance.errors[key + '-' + code]);
		}
	}
	$rootScope.header = $scope.header;
	$rootScope.tabs = $scope.tabs;
	$rootScope.alert = $scope.alert;

	//EVENTS
	//controller loading starts 
	//- Clear all header buttons
	$rootScope.$on("$locationChangeStart", function () {

	})
	$rootScope.$on("axis:resolve:controller_loading:start", function () {
		$scope.header.setCustomText('');
		$scope.header.actions.clear();
		$scope.header.show();
		$scope.header.setMainIcon(null, null);
		$scope.header.setBackgrond($(".app-bar").css("backgroundColor"), $(".app-bar").css("color"));
		$scope.tabs.clear();
		document.body.scrollTop = 0;
		$(window).unbind("wheel");
		$("body").unbind("touchstart touchmove touchend");
	});
	//language loading finishes
	//load all things deppending on language support
	//Must be wirtten in here
	$rootScope.$on("language-loaded", function () {
		Sugar.calendar.week = $scope.language.instance['defaults']['calendar-week']['short'];

		$scope.$apply();
	});




	$scope.logout = function () {
		Storage.local.clear();
		$location.path(settings.router.authentication)
	}




}]);


document.addEventListener('backbutton', function () {
	Sugar.getBackButtonActions().forEach(function (fnBackButton) {
		fnBackButton();
	})
	if (Sugar.preventBackButton) {
		return false;
	}
	
	if (Sugar.sidebar.isOpen()) {
		Sugar.sidebar.hide();
		return false;
	}
	if ($(".modal.open").size() > 0) {
		$(".modal.open").find(".modal-close").trigger('click');
		return false;
	}



	//navigator.app.exitApp();
	window.history.back();

});

window.Math.isPositive = function (i) {
	return i >= 0;
};
angular.module('System').directive("loader", ['$rootScope', '$route', function ($rootScope, $route) {
	return function ($scope, element, attrs) {
		$rootScope.$on("$locationChangeStart", function () {
			$("#main").hide();
			$("#application-view").html(" ");
			element.show().css('opacity', 1).removeClass("manual").addClass("resolving");
		});
		$rootScope.$on("axis:resolve:default_components:load", function () {
			element.show().css('opacity', 1).removeClass("manual").addClass("resolving");
		});
		$rootScope.$on("$viewContentLoaded", function () {
			$("#main").fadeIn();
			element.removeClass("resolving");
			element.hide().removeClass("manual");
		});
		$scope.$on("axis:request:start", function () {
			//$("#main").hide();
			element.show();
		});
		$scope.$on("axis:request:all:finished", function () {
			if (element.hasClass("resolving")) return;
			element.hide().removeClass("manual");
			//$("#main").fadeIn();
		});

		$scope.reloadPage = false;
		$scope.showTimeout;


		Sugar.touch.on("move", function (e) {
			if (element.hasClass("resolving")) return;
			var distance = 30;


			if (document.body.scrollTop > 0 || Sugar.touch.movement == 'H' /*|| Sugar.touch.distance.y < distance*/ || Sugar.touch.position.y.initial > 60 || Sugar.touch.direction.y == 'U') {
				element.hide();
				return;
			}
			e.preventDefault();
			element.addClass("manual");



			var $icon = element.find(".icon");
			//var height = angular.element(document).height();
			var height = angular.element(window).outerHeight();
			var positionStart = height / 2 + ($icon.outerHeight()/2);
			var rotation = 180 + Sugar.touch.distance.y - distance;
			var top = -positionStart + Sugar.touch.distance.y /*- distance*/;
			
			Sugar.transform.rotate($icon.find("i"), 0, 0, 1, rotation);

			
			var cutPoint = 150;
			$scope.showTimeout = setTimeout(function () {
				element.show();
				$scope.reloadPage = top >= -cutPoint;
			}, 10);

			//element.removeClass("cut-point");
			if (top >= -cutPoint) {
				element.addClass("cut-point");
				return;
			} else {
				element.removeClass("cut-point");
			}

			Sugar.transform.translate($icon, 0, (top >= -14 ? -14 : top));
		});
		Sugar.touch.on("end", function () {
			if ($scope.showTimeout) {
				clearTimeout($scope.showTimeout);
				$scope.showTimeout = null;
			}
			if (element.hasClass("resolving")) return;
			if (document.body.scrollTop > 0 || !$scope.reloadPage || Sugar.touch.movement == 'H' || Sugar.touch.distance.y < 100 || Sugar.touch.direction.y == 'U') {
				if (element.hasClass("manual")) {
					var height = angular.element(window).outerHeight();
					element.addClass("returning");
					Sugar.transform.translate(element.find(".icon"), 0, -(height / 2 + 200));
					setTimeout(function () {
						element.css("transform", "").find(".icon").css("transform", "");
						element.removeClass("returning").removeClass("manual");
						element.hide();
					}, 100);
				}

				element.fadeOut(100, function () {
					element.css("transform", "").find(".icon").css("transform", "");
				});
				return;
			}
			element.fadeIn().css("transform", "").removeClass("manual").find(".icon").css("transform", "");
			$scope.reloadPage = false;
			$route.reload();
		});
	};
}])